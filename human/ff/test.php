#!/usr/bin/php
<?php namespace woozalia\futil;
/*
  PURPOSE: FileFind -- a CLI which understands that bash's globbing is broken
  USAGE:
    ff [options] text-to-find "[file mask]"
    - if no file-mask specified, assumes *.*
    - if only one argument is specified, it is assumed to be the file-mask.
    - if two arguments are specified, the first will be treated as search-text and the second as the file-mask.
    - search is always recursive; you can just use grep for non-recursive... most of the time
  RULES:
    - masks in the path *will* be applied to folders; masks in the filename will only be applied to files
      Specifically: we'll use glob() for searching but only use its file results; we'll recursively apply
        the search to *all* folders at any given level, not just the ones glob() finds.
    - Options need to be before search terms. (This is a restriction imposed by getopt().)
    - Only options in the list are recognized as such; otherwise they're assumed to be search terms.
  TODO:
    2022-07-27 If no text specified, don't run grep for each file. Just list the files.
  HISTORY:
    2022-06-17 started
    2022-06-22 mostly working; ironing out some possible bugs
    2022-07-27 partly fixed the just-find-files option; a bit of tidying around options
    2022-07-29 major rewrite of... everything
      * completely redid how options are parsed; no longer using getopt()
      * search classes to handle what happens when a match is found
      * more sensible encapsulation of stuff
*/

require_once('conf.php');   // default config
#require_once('../lib/fileutil.php');  // general classes
#require_once('../lib/folder.php');    // folder classes
require_once('lib/0.php');  // class autoloader
require_once('lib.php');    // FF-specific class

$tt = new cScreenANSI;

$tt->EraseScreenAll();

echo "Testing:\n";
$tt->MoveCursor(1,1);
echo 'one';
$tt->MoveCursor(2,2);
echo 'two';
$tt->MoveCursor(1,20);
echo "Done testing.\n";
/*
cApp::SetVersionString('Test Thing v0.0 2022-10-28');
cApp::SetDocsURL('https://wooz.dev/');
new cApp($argv);
*/
