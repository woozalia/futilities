<?php namespace woozalia\futil;
/*
  PURPOSE: classes specific to the FF utility
  CONFIG:
    * names of CLI options are specified in cOpts constructor
  HISTORY:
    2022-07-31 reversed order of search terms, because that's what I found myself typing in:
      1: filepath/mask, 2: text mask (optional)
*/

/*::::
  HISTORY:
    2022-09-09 added to match refactoring in FTI
*/
class cApp extends caAppBase {
    // ++ ACTION ++ //
    
    // CEMENT caAppBase
    protected function Go() {
        $this->ProcessInput();
    }
    protected function ProcessInput() {
        $oOpts = $this->Options();
        
        if ($oOpts->HasSearchArgs()) {
            $of = new cFolderator;
            
            $of->DoSearch($oOpts->Matcher());
            csTTY::EraseLine(); // erase last status line
        }
    }
}

/*::::
  PURPOSE: cOpts which knows the specific options used by FF.
  HISTORY:
    2022-07-29 renamed from cOpts to cOptsFF; moved general stuff to new cOpts
    2022-08-05 renamed from cOptsFF to cOptsApp for easier templating of new utils
    2022-09-09 remamed from cOptsApp to cOpts because of library refactoring
*/
class cOpts extends caOptsSpider {

    // ++ CONFIG ++ //
  
    const ksOPT_CASE_SENSITIVE  = 'case.sens';  // file-mask case sensitive?
    const ksOPT_FILE_DATE       = 'file.date';  // match file save/edit date
    const ksOPT_LIST_MATCHES    = 'list.match'; // ooptions for showing matches
    #const ksOPT_LOG_FOLDERS     = 'log.fldrs';  // log all folders, even if no matches?
    const ksOPT_SHOW_COMMANDS   = 'show.cmd';   // show each command used (grep)

    public function DescribeSelf() : string {
        if ($this->HasSearchArgs()) {
            $s = $this->Matcher()->DescribeSelf();
        } else {
        throw new \exception('2022-09-13 How would we ever get here?');
            $s = "You need to specify at least one search argument.\n"
              .$oOpts->ListOptions()
              ;
        }

        $s .= "\nShow entries checked: ";
        $oSubCheck = $this->ListingOptions();
        if ($oSubCheck->HasAny()) {
            $s .= $oSubCheck->DescribeSelf();
        } else {
            $s .= 'NONE';
        }

        $s .= "\nShow matches found: ";
        $oSubMatch = $this->ListMatches();
        if ($oSubMatch->HasAny()) {
            $s .= $oSubMatch->DescribeSelf();
        } else {
            $s .= 'NONE';
        }
        $s .= "\nOptions set:\n".$this->ListCmdOptions();
        return $s;
    }
    // CEMENT caOptsBase
    protected function MinQtyTerms() : int { return 1; }
    // CEMENT caOptsBase
    protected function RenderTooFewTerms(int $qTerms) : string {
        return "You must at least enter a folder to search.";
    }
    // CEMENT caOptsBase
    protected function RenderAppUsage() : string {
        $sThis = $this->GetSelfName();
        return "$sThis [<options>] <folder path>?<name mask> [<content regex>]";
    }
    
    // -- CONFIG -- //
    // ++ SETUP ++ //

    protected function SetupAppOptions() {
        $this->AddAppOptions([
          self::ksOPT_CASE_SENSITIVE  => ['-cs','--case-sensitive'],
          self::ksOPT_FILE_DATE       => ['-fd','--file-date'],
          #self::ksOPT_LOG_FOLDERS     => ['-lf','--log-folders'],
          self::ksOPT_LIST_MATCHES     => ['-lm','--log-matches'],
          self::ksOPT_SHOW_COMMANDS   => ['-sc','--show-commands'],
          ]);
    }

    // CEMENT caOptsBase
    protected function HandleTerm($idx,$sTerm) {
        switch ($idx) {
          case 0: $this->SetFileSpecMask($sTerm); break;
          #case 0: $this->SetSourcePath($sTerm); break;
          case 1: $this->SetTextRegex($sTerm); break;
          default: $this->HandleUnknownTerm($idx,$sTerm);
        }
    }
    
    protected function HandleDateErrors(\DateTime $od) {
        $arMsg = $od->getLastErrors();
        if (is_array($arMsg)) {
            $qMsg = $arMsg['error_count'];
            if ($qMsg > 0) {
                echo $qMsg
                  . ' error'
                  . (($qMsg == 1) ? '' : 's')
                  . " found:\n"
                  ;
                foreach ($arMsg['errors'] as $idMsg => $sMsg) {
                    echo " - $idMsg: $sMsg\n";
                }
                die();  // for now, we just quit on errors
            }
            $qMsg = $arMsg['warning_count'];
            if ($qMsg > 0) {
                echo $qMsg
                  . ' warning'
                  . (($qMsg == 1) ? '' : 's')
                  . " found:\n"
                  ;
                foreach ($arMsg['warnings'] as $idMsg => $sMsg) {
                    echo " - $idMsg: $sMsg\n";
                }
                // report the warning but continue processing
            }
        }
    }
      
    // -- SETUP -- //
    // ++ OPTIONS ++ //
    
    // simple option values

    public function CaseSensitive() : bool { return $this->HasCmdOption(self::ksOPT_CASE_SENSITIVE); }
    public function ShowCommands()  : bool  { return $this->HasCmdOption(self::ksOPT_SHOW_COMMANDS); }
    // if a folder contains matching files, include it in the output (this could proabably be named more clearly)
    #public function ListMatchedFolders() : bool { return TRUE; }
    #public function ListAllFolders() : bool { return $this->HasCmdOption(self::ksOPT_LOG_FOLDERS); }
    public function ListMatches() : cListingOptions { return $this->GetSubOpts(self::ksOPT_LIST_MATCHES, cListingOptions::class); }
    
    // calculated option values
    
    private $oDate;
    public function HasFileDate() : bool { return $this->HasCmdOption(self::ksOPT_FILE_DATE); }
    protected function SetFileDate(\DateTime $od) { $this->oDate = $od; }
    public function GetFileDate() : \DateTime { return $this->oDate; }
    
    public function LogMatchesTo()  : string {
        $ooListing = $this->ListMatches();
        if ($ooListing->UseLogFile()) {
            return $ooListing->LogFileSpec();
        } else {
            return '';
        }
    }

    // -- OPTIONS -- //
    // ++ OBJECTS ++ //

    private $oMatch = NULL;
    // ASSUMES: sufficient search arguments have been found
    public function Matcher() : caMatchFF {
        if (is_null($this->oMatch)) {
            if ($this->HasTextRegex()) {
                // searching for given text in files matching the mask
                $om = new cMatchGrep($this);
            } elseif ($this->HasFileDate()) {
                // searching for matching files saved on given date
                $om = new cMatchDate($this);
            } else {
                // just searching for files matching the mask
                $om = new cMatchShow($this);
            }
            $this->oMatch = $om;
        }
        return $this->oMatch;
    }
    
    // -- OBJECTS -- //
    // ++ INTERNAL SERVICES ++ //

    /*----
      INPUT:
        $fs = URL with mask
            [<proto>://<domain>[:port]/]<path>?<mask>
      HISTORY:
        2022-10-24 revised to handle full URLs, with mask in query
    */
    protected function SetFileSpecMask(string $fs) {
        echo "File spec/mask: $fs\n";

        // SPLIT PATH AND MASK

        $oURL = cnURLInfo::FromSpec($fs);
        $fp = $oURL->BaseURL();
        $vMask = $oURL->Query();
        $sMask = is_null($vMask) ? '(all files)' : $vMask;
        echo
           " -> path: $fp\n"
          ." -> mask: $sMask\n"
          ;

        $this->SetSourcePath($fp);
        echo "SetSourcePath($fp)\n";
        if (is_string($vMask)) {
            $this->SetFileMask($vMask);
        }
        $omSrce = $this->SourceMount();
        echo 'MOUNT class: '.get_class($omSrce)."\n";
        $fpReal = $omSrce->GetLocalSpec();

        // make sure the path exists
        if (!file_exists($fpReal)) {
            echo
              "INPUT ERROR: path [$fpReal] cannot be accessed (may not exist).\n"
              ." - original spec: $fs\n"
              ." - translated to: $fp\n"
              ;
            die();
        }
        
        #$this->SetSourcePath($fpReal);
        #$this->SetFileMask($opi->FileNameFull());
    }
    
    // search text
    private $sRegex = NULL;
    protected function SetTextRegex(string $s) { $this->sRegex = $s; }
    public function GetTextRegex() : string { return $this->sRegex; }
    public function HasTextRegex() : bool { return !is_null($this->sRegex); }
    
    // -- INTERNAL SERVICES -- //

}

abstract class caMatchFF extends caMatchStandard {
    public function __construct(cOpts $o) {
        $this->SetOptions($o);
        $fsLog = $o->LogMatchesTo();
        if ($fsLog != '') {
            $oLog = new cLog($fsLog);
            $this->SetMatchLog($oLog);
            echo "Logging matches to [$fsLog].\n";
            $sHeader =
              'FileFind: '
              .$this->DescribeSelf()
              ."\nBase path: "
              .$o->GetSourcePath()
              ."\n=========="
              ;
            $oLog->WriteLn($sHeader);
        }
    }

    // ++ OBJECTS ++ //
      
    private $omLog = NULL;
    protected function SetMatchLog(cLog $o) { $this->omLog = $o; }
    protected function GetMatchLog() : cLog { return $this->omLog; }
    protected function HasMatchLog() : bool { return !is_null($this->omLog); }
    protected function LogMatch(string $fs) {
        if ($this->HasMatchLog()) {
            $this->GetMatchLog()->WriteLn($fs);
        }
    }
        
    // -- OBJECTS -- //
}
    
// PURPOSE: executes a grep on matching files
class cMatchGrep extends caMatchFF {
  
    public function DescribeSelf() : string {
        $oOpts = $this->GetOptions();
        $sText = $oOpts->GetTextRegex();
        $sMask = $oOpts->GetFileMask();
        return "Searching for '$sText' in files named '$sMask'";
    }
    
    // ++ ACTIONS ++ //
    
    // RULE: no special actions for folders
    public function HandleFolder(caPathPiece $of) {}
    public function HandleFile(caPathNamed $of) {
        // 2022-09-11 this has not been tested
        $fs = $of->GetSpec();
        $oOpts = $this->GetOptions();
        $sText = $oOpts->GetTextRegex();
        
        $sCmd = 'grep "'.$sText.'" "$fs"';
        if ($oOpts->ShowCommands()) {
            echo "  CMD: $sCmd\n";
        }
        $ok = exec($sCmd,$arOut,$nResult);
        if ($ok === FALSE) {
            echo "  -- ERROR $nResult\n";
        } else {
            if (count($arOut) > 0) {
                if (!$doShowAllFiles) {
                    echo "  FILE: $fs\n";
                }
                foreach ($arOut as $sLine) {
                    echo "  -- [$sLine]\n";
                }
            }
        }
    }
    
    // -- ACTIONS -- //
}

// PURPOSE: just displays matching files
class cMatchShow extends caMatchFF {
    public function DescribeSelf() : string {
        $oOpts = $this->GetOptions();
        $fpBase = $oOpts->GetSourcePath();
        $fnMask = $oOpts->GetFileMask();
               
        return "Searching in [$fpBase] for files named '$fnMask'";
    }
        
    // ++ ACTIONS ++ //
    
    /*----
      HISTORY:
        2022-09-25 $oMatch was not being set; now it is. (Not tested yet.)
        2022-10-02 corrected labels used in logging; eliminated redundant dir listings
    */
    private $sDir = NULL; // for detecting change, for display purposes
    public function HandleFile(caPathNamed $of) {
        $oOpts = $this->GetOptions();
        $ooListFound = $oOpts->ListingOptions();
          $doFoundFiles = $ooListFound->DoFiles();
          $doFoundFldrs = $ooListFound->DoFolders();
          $doFoundScrn = $ooListFound->UseScreen();
          $doFoundLog = $ooListFound->UseLogFile();
        $ooListMatch = $oOpts->ListMatches();
          $doMatchFiles = $ooListMatch->DoFiles();
          $doMatchFldrs = $ooListMatch->DoFolders();
          $doMatchScrn = $ooListMatch->UseScreen();
          $doMatchLog = $ooListMatch->UseLogFile();

        // 2022-09-11 missing code to get $fs from $of
        // just show the matching file
        $oMatch = $this->GetTextMatch();
        $sMatch = $oMatch->GetString();
        $isMatch = $oMatch->GetIsMatch();

        $doScrn = (($doFoundScrn and $doFoundFiles) or ($doMatchScrn and $doMatchFiles and $isMatch));
        $doLog = (($doFoundLog and $doFoundFiles) or ($doMatchLog and $doMatchFiles and $isMatch));

        #if ($isMatch) { echo "MATCH DOSCRN=[$doScrn] MSCRN=[$doMatchScrn] MFI=[$doMatchFiles]\n"; }
        $fs = $of->GetBasePath();
        if ($this->sDir != $fs) {
            $this->sDir = $fs;

            if ($doScrn) {
                csTTY::KeepLine("IN DIR [$fs]:");
            }
            if ($doLog) {
                $this->LogMatch("DIR $fs");
            }
        }
        if ($doScrn) {
            csTTY::KeepLine(' - [FILE MATCH] '.$sMatch);
            // (KeepLine() prevents it from clashing with the single-line-status display) (in theory)
        }
        if ($doLog) {
            $this->LogMatch(" - FILE: $sMatch");
        }
    }
    /*----
      NOTE: same as HandleFile, except for display
        2022-09-27 This should only happen after we know the folder is *wanted*
      TODO: figure out if we want relative, absolute, or what
        in display and logs
      HISTORY:
        2022-09-25 It kind of doesn't make sense to be looking for a text match in a folder.
          I think GetTextMatch() (renamed from GetMatchRes()) was maybe supposed to refer to
          $this, back when this code was in some other class -- but then what is GetString()?
    */
    public function HandleFolder(caPathPiece $of) {
        $fpr = $of->GetPath();

        $oMatch = $this->NameMatch($of);
        $isMatch = $oMatch->GetIsMatch();
        $sStat = "[DIR" . ($isMatch?' MATCH':'') . ']';

        $oOpts = $this->GetOptions();

        // first check to see if we're showing all folders
        $oListOpts = $oOpts->ListingOptions();
        $doList = $oListOpts->DoFolders();  // are we listing this item (folders)?
        if ($doList) {
            // we're listing all folders -- so the "list all items" output options apply:
            $doScrn = $oListOpts->UseScreen();
            $doLog = $oListOpts->UseLogFile();
        } elseif ($isMatch) {
            // not showing everything, but this is a match; are we listing matches?:
            $oMatchOpts = $oOpts->ListMatches();
            $doList = $oMatchOpts->DoFolders();
            if ($doList) {
                // yes we are -- so the "list matches" output options apply
                $doScrn = $oMatchOpts->UseScreen();
                $doLog = $oMatchOpts->UseLogFile();
            }
        }

        if ($doList) {
            if ($doScrn) {
                csTTY::KeepLine($sStat.' '.$fpr);
            }
            if ($doLog) {
                $fpa = $of->GetSpec();
                $this->LogMatch($sStat.': '.$fpa);
            }
        } else {
            // not listing the current item, so just update the status onscreen
            csTTY::ShowLine($sStat.' '.$fpr);
        }

        // same logic, but screen
    }
}
// PURPOSE: matcher which looks at file-creation dates
abstract class caMatchDates extends caMatchFF {
    const ksFileDateFmt = 'Y/m/d H:i';  // format to show file timestamp
    const ksReqDateFmt = 'Y/m/d';       // format to show requested date
    
    abstract protected function IsDateInRange(int $nStamp) : bool;
    // OVERRIDE caMatch
}
// PURPOSE: matcher for files edited during a single given date
class cMatchDate extends caMatchDates {
    
    public function DescribeSelf() : string {
        $oOpts = $this->GetOptions();
        $fpBase = $oOpts->GetSourcePath();
        $fnMask = $oOpts->GetFileMask();
        $oDate = $oOpts->GetFileDate();
        $sDate = $oDate->format(self::ksReqDateFmt);
        
        // Only for debugging, for now.
        #echo $oOpts->DescribeRequest();
        
        return "Searching in '$fpBase' for files named '$fnMask' and saved on $sDate";
    }

    // ++ ACTIONS ++ //
    
    // RULE: no special actions for folders
    public function HandleFolder(caPathPiece $of) {}
    public function HandleFile(caPathNamed $of) {
        // 2022-09-11 This will need updating, and PathSeg may need a GetInfo() function too.
        // TODO: give $of a stat() function
        //  -- does ffmove maybe already implement a return-class?
        #$fs = $of->CurrentAbsolute();
        #$arInfo = @stat($fs);
        $oInfo = $of->GetInfo();
        if ($oInfo->HasInfo()) {
            $arMatch = [];
            $nDate = $oInfo->ChgTime();
            if ($this->IsDateInRange($nDate)) {
                $sWhen = date(self::ksFileDateFmt,$nDate);
                $arMatch[] = 'created '.$sWhen;
            }
            $nDate = $oInfo->ModTime();
            if ($this->IsDateInRange($nDate)) {
                $sWhen = date(self::ksFileDateFmt,$nDate);
                $arMatch[] = 'edited '.$sWhen;
            }
            if (count($arMatch) > 0) {
                $om = $this->GetTextMatch();
                $fs = $of->GetSpec();
                csTTY::KeepLine('[FILE MATCH] '.$om->GetString());
                csTTY::KeepLine(" IN $fs");
                $this->LogMatch('DIR: ['.$fs.']');
                foreach ($arMatch as $sText) {
                    $sLine = ' - '.$sText;
                    csTTY::KeepLine($sLine);
                    $this->LogMatch($sLine);
                }
            }
        } else {
            $fs = $of->GetSpec();
            csTTY::KeepLine("WARNING - could not stat file: $fs\n");
        }
    }
    
    // -- ACTIONS -- //
    
    protected function IsDateInRange(int $nStamp) : bool {
        $odReq = $this->GetOptions()->GetFileDate();
        return self::IsSameDate($nStamp,$odReq);
    }
    
    // RETURNS: TRUE iff $ndFile is on the same day as $odReq
    static protected function IsSameDate(int $ndFile, \DateTime $odReq) : bool {
        $ndReqSt = $odReq->getTimestamp();
        $ok = FALSE;
        if ($ndReqSt < $ndFile) {
            // file time is greater than beginning-of-day
            $ndReqFi = $ndReqSt + (24 * 60 * 60);
            if ($ndReqFi > $ndFile) {
                // file time is less than end-of-day
                $ok = TRUE;
            }
        }
        
        /*
        $sReqSt = date(self::ksFileDateFmt,$ndReqSt);
        $sReqFi = date(self::ksFileDateFmt,$ndReqFi);
        $sFile =  date(self::ksFileDateFmt,$ndFile);
        echo "START=[$sReqSt] FILE=[$sFile] END=[$sReqFi] - " . ($ok ? 'YEP!' : 'nope') . "\n";
        */
        return $ok;
    }
}
