#!/usr/bin/php
<?php namespace woozalia\futil;
/*
  PURPOSE: FTS - File Tree Sync
    Compares one file tree with same-named files in another location.
    Where discrepancies are found, uses a variety of resolution techniques.
  HISTORY:
    2022-09-16 started
*/

require_once('lib/0.php');  // general classes
require_once('lib.php');    // app-specific class
require_once('conf.php');   // configuration

cApp::SetVersionString('File Tree Sync v0.00 / 2022-09-20');
cApp::SetDocsURL('https://wooz.dev/Futilities/human/fts');

new cApp($argv);
echo "== DONE ==\n";
