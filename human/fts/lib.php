<?php namespace woozalia\futil;

use woozalia\futil\IO\Store\Aux\Path\iPiece as PathPieceIface;

/**
  PURPOSE: classes specific to the FTS utility
  HISTORY:
    2022-09-01 started for FCC
*/
class cApp extends caAppBase {
    
    // ++ ACTION ++ //
    
    // CEMENT caAppBase
    protected function Go() {
        #csTTY::Open();
        $this->ProcessInput();
        #csTTY::Shut();
    }
    protected function ProcessInput() {
        $oOpts = $this->Options();
                
        $f = new cInputTextDataFile($oOpts->GetListSpec());
        if (!$oOpts->HasTargetPath() or $oOpts->WantPreCount()) {
            $this->CountLines($f);
        }
        if ($oOpts->HasTargetPath()) {
            $this->CopyFiles($f);
        }
    }
    protected function CountLines(cInputTextDataFile $f) {
        $oOpts = $this->Options();
        $nLines = 0;
        
        $of = new cFolderator;
        $om = new cSyncher($oOpts);
        $of->DoSearch($om);
        
        die("2022-09-20 CountLines() needs rewriting.\n");
        
        while (($or = $f->ReadDataLine())->IsOkay()) {
            #$fsr = $or->Text();
            $nLines++;
        }
        
        // The rest of this should work still. Maybe a folder-count, too?
        
        $sCount = "$nLines line".(($nLines == 1)?'':'s')."\n";
        if ($oOpts->HasTargetPath()) {
            echo "Processing $sCount";
        } else {
            echo "Counted $sCount";
        }
        $this->SetLineCount($nLines);
        $f->Restart();  // set file pointer back to beginning
    }
    /*----
      NOTE:
        2022-09-09 Do NOT set any part of $of to options->GetSourcePath(),
          as this may include protocol stuff; only use mount->GetLocalSpec().
    */
    protected function CopyFiles(cInputTextDataFile $f) {
        $oOpts = $this->Options();
        $nlCurr = 0;
        $of = new cFileSpec;

        die("2022-09-20 CopyFiles() needs rewriting.\n");
        
        $hasCount = $this->HasLineCount();
        if ($hasCount) {
            $nlCount = $this->GetLineCount();
        }
        
        $hasSource = $oOpts->HasSourcePath();
        if ($hasSource) {
            $omSrce = $this->SourceMount();
            $of->SetPartA($omSrce->GetLocalSpec());
        }

        $fpSource = $oOpts->GetSourcePath();
        #echo "\nSOURCE (1): ".$of->DumpLine();
        #echo "\nfpSource = [$fpSource]";
        #echo "\n";
        while (($or = $f->ReadDataLine())->IsOkay()) {
            $fsr = $or->Text();
            $nlCurr++;
            if ($hasCount) {
                $sPct = sprintf('%d',$nlCurr/$nlCount);
                $sStatus = "$sPct% ($nlCurr/$nlCount)";
            } else {
                $sStatus = "($nlCurr)";
            }
            csTTY::ShowLine($sStatus.' '.$fsr);
            $of->SetPartC($fsr);
        #echo "\nSOURCE (2): ".$of->DumpLine()."\n"; die();
            $this->CopyFile($of);
        }
    }
    /*----
      INPUT:
        $of is the filespec for the source file to copy.
          By default it is absolute -- but if there is a SourcePath,
          that is prepended. SourceFileSpec($of) calculates this.
    */
    protected function CopyFile(cFileSpec $of) {
        $oOpts = $this->Options();
        
        // filespec for source
        $fsaSrce = $of->PartCAbsolute();
        
        // calculate destination filespec
        $fpDest = $oOpts->GetTargetPath();
        
        // copy() does not support folder-creation, but mkdir() does
        // so we need to know the folder path that needs to exist
        $fpSrceBase = dirname($fsaSrce);
        $ofDest = clone $of;
        $ofDest->SetPartA($fpDest);
        $fsaDest = $ofDest->PartCAbsolute();
        #echo "\nofDest: ".$ofDest->DumpLine()."\n";
        
        if (file_exists($fsaSrce)) {
            $fpDestBase = dirname($fsaDest);
            // TODO: check that fpDestBase is a folder; delete first if not
            if (!file_exists($fpDestBase)) {
                $ok = @mkdir($fpDestBase,0777,TRUE);
                if ($ok) {
                    $ok = copy($fsaSrce,$fsaDest);
                    if ($ok) {
                        // copy over timestamp too
                        
                    }
                    csTTY::Append(' ok');
                } else {
                    csTTY::KeepLine(' - could not mkdir');
                    csTTY::Keepline("MKDIR failed for [$fpDestBase]");
                    echo "\n";
                    die();  // maybe handle more elegantly later?
                }
            }
        } else {
            csTTY::Append(' NOT FOUND');
            csTTY::NewLine();
        }
    }
    
    // -- ACTION -- //
    // ++ STATE ++ //
    
    private $nLines = NULL;
    protected function HasLineCount() : bool { return !is_null($this->nLines); }
    protected function SetLineCount(int $n) { $this->nLines = $n; }
    protected function GetLineCount() : int { return $this->nLines; }
    
    // -- STATE -- //
}

class cOpts extends caOptsSpider {
    use tOpts_HasTargetPath;

    // ++ CONFIG ++ //
    
    // CEMENT caOptsBase
    protected function RenderAppUsage() : string {
        $sThis = $this->GetSelfName();
        return "$sThis [<options>] <source folder> [<target folder>]";
   }
    // CEMENT caOptsBase
    protected function MinQtyTerms() : int { return 1; }
    // CEMENT caOptsBase
    protected function RenderTooFewTerms(int $qTerms) : string {
        if ($qTerms < 1) {
            $out = 'You must at least specify a source folder to spider.';
        } else {
            // TODO: let's have another term qty limit where only a warning is issued
            $out = 'You might want to specify a target folder for synchronizing.';
        }
        return $out;
    }
    // CEMENT cOptsBase
    public function DescribeSelf() : string {
        $fpSrce = $this->GetSourceSpec();
        $sSubj = "files found in $fpSrce";
        if ($this->HasTargetPath()) {
            $sVerb = 'Synching';
            $sTarg = $this->GetTargetPath();
            $sObj = 'to '.$fpTarg;
        } else {
            $sVerb = 'Counting';
            $sObj = '';
        }
        
        $s = "$sVerb $sSubj $sObj";
        
        return $s;
    }
    
    // -- CONFIG -- //
    // ++ SETUP ++ //
    
    const ksOPT_WANT_PRECOUNT   = 'want.count'; // name of config file to use
    const ksOPT_USE_SUDO        = 'use.sudo'  ; // where possible/applicable, use sudo
    
    public function __construct(array $arCmdArgs) {
        $this->AddAppOptions([
          self::ksOPT_WANT_PRECOUNT => ['-pc','--precount'],
          self::ksOPT_USE_SUDO      => ['-su','--sudo'],
          ]);
        parent::__construct($arCmdArgs);
    }
    protected function HandleTerm($idx,$sTerm) {
        switch ($idx) {
            case 0: $this->SetListSpec($sTerm); break;
            case 1: $this->SetTargetPath($sTerm); break;
            case 2: $this->SetSourcePath($sTerm); break;
            default: $this->HandleUnknownTerm($idx,$sTerm);
        }
    }
    
    // -- SETUP -- ///
    // ++ SETTINGS ++ //
    
    // simple option values
    public function WantPreCount()  : bool { return $this->HasCmdOption(self::ksOPT_WANT_PRECOUNT); }
    public function UseSudo()       : bool { return $this->HasCmdOption(self::ksOPT_USE_SUDO); }

    // input list-file
    private $fsList;
    protected function SetListSpec(string $fs) { $this->fsList = $fs; }
    public function GetListSpec() : string { return $this->fsList; }
    
    // -- SETTINGS -- //
}
class cSyncher extends caMatchStandard {
    // CEMENT
    public function DescribeSelf() : string { return $this->Options()->DescribeSelf(); }
    /*----
      CEMENT
      RULE: No special actions for folders.
      TODO: might actually want to check for nonexistence of either folder.
        This gets tricky, because how do you determine that a folder removal
        was a *later* event?
    */
    public function HandleFolder(PathPieceIface $of) {}
    /*----
      CEMENT
      RULE: This is the Syncher match-handler, so we need to compare the
        source file with its corresponding target filespec.
    */
    public function HandleFile(PathPieceIface $of) {
      // TO BE WRITTEN
        $oOpts = $this->Options();
        $ofTarg = $this->GetTargetPath();
    }
}
