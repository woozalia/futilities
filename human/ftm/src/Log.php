<?php namespace woozalia\futil\ftm;

use woozalia\futil\IO\Store\Aux\Path\iPiece as PieceIface;
use woozalia\futil\IO\Store\File\cLog as BaseClass;
use woozalia\futil\IO\Store\File\iLog as BaseIface;
use woozalia\futil\IO\Store\File\iPair as FilePairIface;

interface iLog extends BaseIface {
    function LogSourceFolder(PieceIface $ofSrce);
    function LogSourceFile(PieceIface $ofSrce);
    function RecordConflict(FilePairIface $oPair, string $sIssue);
    function RecordMissing(FilePairIface $oPair);
    function RecordBlacklisted(string $fp);
    #function RecordTimeMismatch(string $fs,int $nTimeA,int $nTimeB,string $sType);
    function RecordTimeMismatch(FilePairIface $oPair,string $sType);
    function RecordCopyAttempt();
    function RecordCopyFailure(string $fsA,string $fsB,int $nPos);
    function RecordDeletion(string $fs);
}

/**
  PURPOSE: specialized logfile class for moving file-trees
  HISTORY:
    2022-10-19 started (adapting from ffmove.php)
    2024-04-29 now using file-per-class library organization
      Renamed .\cLogMove -> .\ftm\cLog
*/
class cLog extends BaseClass implements iLog {

    // ++ PROGRESS ++ //

    public function LogSourceFolder(PieceIface $ofSrce) {
        $osSrce = $ofSrce->GetPathStatus();
        $fp = $osSrce->GetRelPath();
        $this->WriteLn("[ $fp ]:");
    }
    public function LogSourceFile(PieceIface $ofSrce) {
        $fn = $ofSrce->GetName();
        $this->WriteLn(" - $fn");
    }

    // ++ RESULTS DATA ++ //

    private $arDiff=[];
    public function RecordConflict(FilePairIface $oPair, string $sIssue) {
        $this->arDiff[] = $oPair; // TODO: save $sIssue here too
        $this->WriteLn(" -- $sIssue");
    }
    protected function GetConflictArray() : array { return $this->arDiff; }
    protected function GetConflictCount() : int { return count($this->GetConflictArray()); }
    protected function RenderConflictList() : string {
        $ar = $this->GetConflictArray();
        if (count($ar) == 0) {
            $out = " = NONE FOUND =\n";
        } else {
            $out = '';
            foreach ($ar as $oPair) {
                $out .= " - ".$oPair->GetPathA()."\n";
            }
        }
        return $out;
    }

    private $arMiss=[];
    private $nBytes=0;
    public function RecordMissing(FilePairIface $oPair) {
        $this->arMiss[] = $oPair;
        $this->nBytes += ($oPair->GetPathA()->Info()->GetSize());
    }
    protected function GetMissingArray() : array { return $this->arMiss; }
    protected function GetMissingCount() : int { return count($this->GetMissingArray()); }
    protected function RenderMissingList() : string {
        $ar = $this->GetMissingArray();
        if (count($ar) == 0) {
            $out = " = NONE FOUND =\n";
        } else {
            $out = '';
            foreach ($ar as $oPair) {
                $oFileA = $oPair->GetFileA();
                $out .= " - ".$oFileA->GetSpec()."\n";
            }
        }
        return $out;
    }

    private $arBList=[];
    public function RecordBlacklisted(string $fp) { $this->arBList[] = $fp; }
    protected function GetBlacklisted() : array { return $this->arBList; }
    protected function GetBlacklistedCount() : int { return count($this->GetBlacklisted()); }
    protected function RenderBlacklisted() : string {
        $ar = $this->GetBlacklisted();
        if (count($ar) == 0) {
            $out = " = NONE FOUND =\n";
        } else {
            $out = '';
            foreach ($ar as $fp) {
            $out .= " - $fp\n";
            }
        }
        return $out;
    }

    // ASSUMES: times are different
    private $arTimeDiff=[];
    #public function RecordTimeMismatch(string $fs,int $nTimeA,int $nTimeB,string $sType) {
    public function RecordTimeMismatch(FilePairIface $oPair,string $sType) {
        $ofA = $oPair->GetFileA();
        $ofB = $oPair->GetFileB();
        $opA = $oPair->GetPathA();
        $opB = $oPair->GetPathB();
        $onfA = $opA->Info();
        $onfB = $opB->Info();

        $nSizeA = $onfA->GetSize();
        $nSizeB = $onfB->GetSize();
        $urlA = $ofA->GetSpec();
        $urlB = $ofB->GetSpec();
        $nTimeA = $ofA->GetTimeInt();
        $nTimeB = $ofB->GetTimeInt();
        $sTimeA = $ofA->GetTimeStr();
        $sTimeB = $ofB->GetTimeStr();

        if ($nTimeA > $nTimeB) {
            $sDetail = "A is later ($sType): $sTimeA > $sTimeB";
        } else {
            $sDetail = "B is later ($sType): $sTimeA < $sTimeB";
        }
        $arDetail = array(
          'fs'	=> $urlA,
          'det'	=> $sDetail
          );
        $this->arTimeDiff[] = $arDetail;

        $this->WriteLn(" -- $sDetail");
    }
    protected function GetTimeDiffs() : array { return $this->arTimeDiff; }
    protected function GetTimeDiffCount() : int { return count($this->GetTimeDiffs()); }
    protected function RenderTimeDiffs() : string {
        $ar = $this->GetTimeDiffs();
        if (count($ar) == 0) {
            $out = " = NONE FOUND =\n";
        } else {
            $out = '';
            foreach ($ar as $arDetail) {
                $fs = $arDetail['fs'];
                $sDet = $arDetail['det'];
                $out .= " - $fs\n -- $sDet\n";
            }
        }
        return $out;
    }
    private $nCopies=0;
    public function RecordCopyAttempt() { $this->nCopies++; }
    protected function GetCopyCount() { return $this->nCopies; }

    private $arErrors=[];
    public function RecordCopyFailure(string $fsA,string $fsB,int $nPos)
      { $this->arErrors[] = " - Error at ~$nPos bytes left:\n -- A: $fsA\n -> B: $fsB\n"; }
    protected function GetFailureCount() : int { return count($this->arErrors); }
    protected function GetFailureList() : array { return $this->arErrors; }
    protected function RenderFailures() : string {
        $ar = $this->GetFailureList();
        if (count($ar) == 0) {
            $out = " = NO ERRORS =\n";
        } else {
            $out = '';
            foreach ($ar as $s) {
                $out .= $s;
            }
        }
        return $out;
    }

    private $arDel=[];
    public function RecordDeletion(string $fs) { $this->arDel[] = $fs; }
    protected function GetDeletionList() : array { return $this->arDel; }
    protected function GetDeletionCount() : int { return count($this->GetDeletionList()); }
    protected function RenderDeletions() : string {
        $ar = $this->GetDeletionList();
        if (count($ar) == 0) {
            $out = " = NOTHING DELETED =\n";
        } else {
            $out = '';
            foreach ($ar as $s) {
            $out .= $s."\n";
            }
        }
        return $out;
    }

    // -- RESULTS DATA -- //
    // ++ OUTPUT: RESULTS ++ //

	// simple for now
    protected function SummarizeReults() : void { echo $this->ShowMessage($this->RenderResultSummary()); }
    protected function RenderResultSummary() : string {
        $out = 'SUMMARY:'
          ."\n - # content conflicts = "		. $this->GetConflictCount()
          ."\n - # timestamp conflicts = "		. $this->GetTimeDiffCount()
          ."\n - # files missing from B = "		. $this->GetMissingCount()
            . " (".number_format($this->nBytes)." bytes)"
          ."\n - # folders blacklisted = "		. $this->GetBlacklistedCount()
          ."\n"
          ;
        if (KF_DO_COPY) {
            $out .=
            " - # files copied = "              . $this->GetCopyCount()
            .' (failures: '.$this->GetFailureCount().')'
            ."\n"
            ;
        }
        if (KF_DO_DEL) {
            $out .=
              " - # deletions = "          . $this->GetDeletionCount()
              ."\n"
              ;
        }
        return $out;
    }
    protected function SaveDetail($sStartTime) {

        $out =
          'Started: '.$sStartTime."\n"
          .'Finished: '.$this->GetTimeStamp() . "\n"
          .'URL A: '.$this->GetFolderA()."\n"
          .'URL B: '.$this->GetFolderB()."\n"
          .$this->RenderResultSummary()
          ."\n== CONFLICTS ==\n"
          ."\n".$this->RenderConflictList()
          ."\n== TIME DIFFS ==\n"
          ."\n".$this->RenderTimeDiffs()
          ."\n== MISSING ==\n"
          ."\n".$this->RenderMissingList()
          ."\n== BLACKLISTED ==\n"
          ."\n".$this->RenderBlacklisted()
          ;
        if (KF_DO_COPY) {
            $out .=
              "\n== COPY FAILURE ==\n"
              ."\n".$this->RenderFailures()
              ;
        }
        if (KF_DO_DEL) {
            $out .=
              "\n== FILES DELETED ==\n"
              ."\n".$this->RenderDeletions()
              ;
        }
        $this->LogWrite($out);
    }

}
