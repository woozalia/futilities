
* PURPOSE: classes specific to the FF utility
* CONFIG:
  * names of CLI options are specified in cOpts constructor
* HISTORY:
  * 2022-07-31 reversed order of search terms, because that's what I found myself typing in:
    * 1: filepath/mask, 2: text mask (optional)
