<?php namespace woozalia\futil\ftm;
/*
  TODO:
    2022-10-27 rename cConfigApp -> csConfigApp (and probably cConfig -> csConfig)
  HISTORY:
    2022-08-31 created as a stub just to bring in the base cConfig
*/

use woozalia\futil\cConf as BaseClass;

class cConfig extends BaseClass {
    public function BytesToReadPerBlock() : int { return 1048576; }
    public function LogFileSpec() : string { return 'ftm.log'; }
}
