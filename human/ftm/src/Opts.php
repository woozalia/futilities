<?php namespace woozalia\futil\ftm;

use woozalia\futil\Opt\caSpider as BaseClass;
use woozalia\futil\Opt\Aux\Has\Target\tMountable as TargetTrait;
use woozalia\futil\ftm\cLog as LogClass;
use woozalia\futil\ftm\iLog as LogIface;

/*::::
  PURPOSE: cOpts which knows the specific options used by FF.
  HISTORY:
    2022-07-29 renamed from cOpts to cOptsFF; moved general stuff to new cOpts
    2022-08-05 renamed from cOptsFF to cOptsApp for easier templating of new utils
    2022-09-09 remamed from cOptsApp to cOpts because of library refactoring
    2024-04-29 now using file-per-class library organization
*/
class cOpts extends BaseClass {
    use TargetTrait;

    // ++ CONFIG ++ //

    const ksOPT_DO_DELETE     = 'do.del';   // delete after B copy is confirmed to match A
    const ksOPT_DO_INFO_ONLY  = 'do.info';  // don't actually copy, just show information
    const ksOPT_DO_COMP_FND   = 'do.comp';  // if target file already found, compare before assuming?
    const ksOPT_IGN_DATE_CHG  = 'ign.date'; // ignore date mismatch
    const ksOPT_FINISH_PART   = 'finish';   // if B smaller than A, see if B is a partial copy of A. Finish copy if so.

    protected function SetupAppOptions() {
        $this->AddAppOptions([
          self::ksOPT_DO_DELETE     => ['-dd','--do-delete'],
          self::ksOPT_DO_INFO_ONLY  => ['-io','--info-only'],
          self::ksOPT_DO_COMP_FND   => ['-cf','--comp-found'],
          self::ksOPT_IGN_DATE_CHG  => ['-id','--ign-date-chg'],
          self::ksOPT_FINISH_PART   => ['--finish-part'],
          ]);
    }

    // OVERRIDE - this app always generates a log
    public function UseLogFile() : bool { return TRUE; }

    public function DescribeSelf() : string {
        $doDel = $this->HasCmdOption(self::ksOPT_DO_DELETE);
        if ($doDel) {
            $sVerb = 'Move';
        } else {
            $sVerb = 'Copy';
        }
        $fpSrce = $this->GetSourcePath();
        $fpTarg = $this->GetTargetPath();
        $s = "$sVerb all files in $fpSrce to $fpTarg.\n"
          ."In other words, ensure that everything in A also exists at B:\n"
          ." - A: $fpSrce\n"
          ." - B: $fpTarg"
          ;
        if ($doDel) {
            $s .= "\n ...and delete each file from A after it is confirmed at B.";
        }
        return $s;
    }
    // CEMENT caOptsBase
    protected function MinQtyTerms() : int { return 2; }
    // CEMENT caOptsBase
    protected function RenderTooFewTerms(int $qTerms) : string {
        switch($qTerms) {
          case 0: return "You must enter folders for both source and target.";
          case 1: return "You must also enter a target folder.";
        }
    }
    // CEMENT caOptsBase
    protected function RenderAppUsage() : string {
        $sThis = $this->GetSelfName();
        return "$sThis [<options>] <source folder> <target folder>";
   }

    // -- CONFIG -- //
    // ++ SETUP ++ //

    // CEMENT caOptsBase
    protected function HandleTerm($idx,$sTerm) {
        switch ($idx) {
            case 0:
                echo "Source URL: $sTerm\n";
                $this->SetSourcePath($sTerm);
                break;
            case 1:
                echo "Target URL: $sTerm\n";
                $this->SetTargetPath($sTerm);
                break;
            default:
                $this->HandleUnknownTerm($idx,$sTerm);
        }
    }

    // -- SETUP -- //
    // ++ OBJECTS ++ //

    private $oMatch = NULL;
    // ASSUMES: sufficient search arguments have been found
    public function Matcher() : cMover {
        if (is_null($this->oMatch)) {
            $this->oMatch = new cMover($this);
        }
        return $this->oMatch;
    }

    private $oLog = NULL;
    // NOTE: This app *always* emits a log
    public function LogFile() : LogIface {
        if (is_null($this->oLog)) {
            $oListOpts = $this->ListingOptions();
            if ($oListOpts->UseLogFile()) {
                $fsLog = $oListOpts->LogFileSpec();
            } else {
                $fsLog = cConfig::Me()->LogFileSpec();
            }
            $oLog = new LogClass($fsLog);
            $this->oLog = $oLog;
        }
        return $this->oLog;
    }

    // -- OBJECTS -- //
    // ++ VALUES ++ //

    public function DoDelete() : bool { return $this->HasCmdOption(self::ksOPT_DO_DELETE); }
    public function DoInfoOnly() : bool { return $this->HasCmdOption(self::ksOPT_DO_INFO_ONLY); }
    public function DoCompFound() : bool { return $this->HasCmdOption(self::ksOPT_DO_COMP_FND); }
    public function IgnoreDateDiff() : bool { return $this->HasCmdOption(self::ksOPT_IGN_DATE_CHG); }
    public function DoFinishPartial() : bool { return $this->HasCmdOption(self::ksOPT_FINISH_PART); }

    // -- VALUES -- //
}
