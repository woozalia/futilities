<?php namespace woozalia\futil\ftm;

use woozalia\futil\IO\Screen\Full\cANSI as BaseClass;

/**
  PURPOSE: specialized screen output class
  HISTORY:
    2022-11-02 started (adapting from ffmove.php)
    2024-04-29 renamed from .\cMoveTTY -> .\ftm\cScrn
*/
class cScrn extends BaseClass {

    public function __construct() { $this->Setup(); }

    // ACTION: clears out space for the status display
    public function Setup() { $this->AllocateLines(7); }

    public function ShowPathStatus(string $s) { $this->ShowOnRelLine(1,'PATH STATUS: '.$s); }
    public function ShowPathSpec(string $s)   { $this->ShowOnRelLine(2,'   REL PATH: '.$s); }
    public function ShowFileName(string $s)   { $this->ShowOnRelLine(3,'  FILE NAME: '.$s); }
    public function ShowAction(string $s)     { $this->ShowOnRelLine(4,'     ACTION: '.$s); }
    public function ShowFileStatus(string $s) { $this->ShowOnRelLine(5,'FILE STATUS: '.$s); }
    public function ShowCreate(string $s)     { $this->ShowOnRelLine(6,'     CREATE: '.$s); }
    public function ShowIssue(string $s)      { $this->ShowOnRelLine(7,' LAST ISSUE: '.$s); }
}
