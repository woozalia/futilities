<?php namespace woozalia\futil\ftm;

use woozalia\futil\App\caScreen as BaseClass;
use woozalia\futil\IO\Store\cSpider as SpiderClass;

/**::::
  HISTORY:
    2022-09-09 added to match refactoring in FTI
    2024-04-29 now using file-per-class library organization
*/
class cApp extends BaseClass {

    // ++ CONFIG ++ //

    static protected function ConfigClass() : string { return cConfig::class; }
    static protected function OptionsClass() : string { return cOpts::class; }
    static protected function ScreenClass() : string { return cScrn::class; }

    // -- CONFIG -- //

    // -- CONFIG -- //
    // ++ ACTION ++ //

    // CEMENT caAppBase
    protected function Go() { $this->ProcessInput(); }
    protected function ProcessInput() {
        $oOpts = $this->Options();

        if ($oOpts->HasSearchArgs()) {
            $ofAct = new SpiderClass;

            $ofLog = $oOpts->LogFile();
            $ofLog->WriteHeader();

            $ofAct->DoSearch($oOpts->Matcher());
        }
    }
}
