<?php namespace woozalia\futil\ftm;

use woozalia\futil\caApp as BaseAppClass;
use woozalia\futil\IO\Store\Spider\Aux\Match\caStandard as BaseClass;
use woozalia\futil\IO\Store\Aux\Path\caPiece as PathPieceClass;
use woozalia\futil\IO\Store\Aux\Path\iPiece as PathPieceIface;
use woozalia\futil\IO\Store\Aux\Path\Piece\cRoot as PathRootClass;
use woozalia\futil\IO\Store\Aux\Compare\cPath as PathCompareStatusClass;
use woozalia\futil\IO\Store\Aux\Compare\iPath as PathCompareStatusIface;
use woozalia\futil\IO\Store\File\cPair as FilePairClass;

class cMover extends BaseClass {
    public function __construct(cOpts $o) { $this->SetOptions($o);}

   // TODO 2022-10-24 rename to FigureDest()
    protected function FigureDestPath(PathPieceIface $ofSrce) : PathPieceIface {
        $fpaSrce = $ofSrce->GetSpec();      // just for debug

        $oOpts = $this->GetOptions();
        $fpbTarg = $oOpts->GetTargetPath(); // base target folder

        $fprSrce = $ofSrce->GetPath();      // path relative to source base

        $ofDestRoot = new PathRootClass($fpbTarg);
        $ofDest = $ofDestRoot->ParsePath($fprSrce);

        return $ofDest;
    }
    
    /*----
      ACTION: Copies the folder to its corresponding location
        in the target foldertree. If DoDelete is set,
        will delete the source folder if it is empty afterwards.
      NOTE: This is only called if there are folders to copy/move.
    */
    public function HandleFolder(PathPieceIface $ofSrce) {
        $ofDest = $this->FigureDestPath($ofSrce);
        $fpaDest = $ofDest->GetSpec();

        $oOpts = $this->GetOptions();
        $ofLog = $oOpts->LogFile();
        #$ofLog->LogSourceFolder($ofSrce);  // 2024-05-03 This seems to be a duplicate.

        if (file_exists($fpaDest)) {
            // DEBUG
            #echo "Local folder [$fpaDest] already exists.\n";
        } else {
            $oApp = BaseAppClass::Me();
            $oScrn = $oApp->Screen();

            $ofLog->WriteLn("Creating [$fpaDest]");
            $osSrce = $ofSrce->GetPathStatus();

            $oScrn->ShowPathStatus($osSrce->GetInfo());
            $oScrn->ShowPathSpec($osSrce->GetRelPath());
            $sTargDesc = "target folder [$fpaDest]";
            $oScrn->ShowCreate($sTargDesc);
            mkdir($fpaDest,0777,TRUE);	// create recursively if needed
            $oScrn->ShowCreate("$sTargDesc [DONE]");
        }
    }

    // INPUT: $of = object for source spec
    public function HandleFile(PathPieceIface $ofSrce) {
        $fpaSrce = $ofSrce->GetSpec();
        $ofDest = $this->FigureDestPath($ofSrce);
        $fpaDest = $ofDest->GetSpec();

        $fprSrce = $ofSrce->GetPath();
        $fprDest = $ofDest->GetPath();

        // DISPLAY

        $oScrn = BaseAppClass::Me()->Screen();

        $osSrce = $ofSrce->GetPathStatus();
        $oScrn->ShowPathStatus($osSrce->GetInfo());

        $oScrn->ShowPathStatus($osSrce->GetInfo());
        // 2022-11-04 apparently the updates in HandleFolder() aren't showing up?
        // 2024-04-28 I think that is maybe only updated when a folder is *created*.
        $oScrn->ShowPathSpec($osSrce->GetRelPath());
        $oScrn->ShowFileName($ofSrce->GetName());
        $oScrn->ShowAction('COPYING from A to B');

        // LOGGING

        $oOpts = $this->GetOptions();
        $ofLog = $oOpts->LogFile();
        $ofLog->LogSourceFile($ofSrce);

        // ACTION

        $oPair = new FilePairClass($ofSrce,$ofDest,0,0,$oOpts);
        $oPair->Ensure(new PathCompareStatusClass($ofSrce,0,0));
    }
}
    
