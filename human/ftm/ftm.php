#!/usr/bin/php
<?php namespace woozalia\futil\ftm;
/*
  PURPOSE: FTM - file tree mover
  HISTORY:
    2022-09-28 started, but will be leaning heavily on ffmove.php from "FileFerret" repo
    2023-12-23 fixed some bugs... but ultimately this needs to be integrated with Ferreteria
      and made more easily deployable.
*/

use woozalia\futil\init\csClassLoader as ClassLoaderClass;

#require_once('lib.php');          // app-specific class
$fpFutils = dirname(__DIR__,2);
$fsLoader = $fpFutils.'/init/ClassLoader.php';
if (!file_exists($fsLoader)) {
    echo "ERROR: Class Loader file [$fsLoader] not found!\n";
    die();
}
require_once($fsLoader);
ClassLoaderClass::Init();
ClassLoaderClass::SetNSpacePath(__NAMESPACE__,__DIR__.'/src');
// ++TODO: These need to go in a general config file
ClassLoaderClass::SetNSpacePath(\woozalia\futil::class,$fpFutils.'/src');
ClassLoaderClass::SetNSpacePath(\woozalia\ferret::class,$fpFutils.'/lib/ferret');
define('CRLF',"\n");
// --TODO

define('KS_FMT_TIMESTAMP','Y/m/d H:i:s');

cApp::SetVersionString('File Tree Mover v0.14 / 2024-05-16');
cApp::SetDocsURL('https://wooz.dev/Futilities/human/ftm');
new cApp($argv);

echo "\n== DONE ==\n";
