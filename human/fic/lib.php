<?php namespace woozalia\futil;
/*
  PURPOSE: classes specific to the FIC utility
  HISTORY:
    2022-08-07 started
    2022-08-09 adding output file
    2022-09-14 refactoring endgame
*/

/*::::
  HISTORY:
    2022-09-14 added to match refactoring in FTI
*/
class cApp extends caAppBase {
    // ++ ACTION ++ //
    
    // CEMENT caAppBase
    protected function Go() {
        $this->ProcessInput();
    }
    protected function ProcessInput() {
        $this->Options()->DoCompare();
    }
}


class cOpts extends caOptsBase {

    // ++ CONFIG ++ //
    
    public function DescribeSelf() : string {
        $arFiles = $this->GetIndexFiles();
        $nFiles = count($arFiles);
        $out = "Comparing $nFiles index file".($nFiles==1?'':'s').": \n";
        foreach ($arFiles as $idx => $fs) {
            $out .= " - $fs\n";
        }
        return $out;
    }
    // CEMENT caOptsBase
    protected function MinQtyTerms() : int { return 2; }
    // CEMENT caOptsBase
    protected function RenderTooFewTerms(int $qTerms) : string {
        switch ($qTerms) {
            case 0: return "You must enter at least two index-files to compare.";
            case 1: return "You must also enter another index-file to compare with.";
        }
    }
    // CEMENT caOptsBase
    protected function RenderAppUsage() : string {
        $sThis = $this->GetSelfName();
        return "$sThis [<options>] <model file> <comparison file> [<comparison file> [...]]";
   }
    // CEMENT caOptsBase
    protected function HandleTerm($idx,$sTerm) {
        $this->SetIndexFile($idx,$sTerm);
    }
        
    // ++ CONFIG ++ //
    // ++ SETUP ++ //

    const ksOPT_RECORD_FOUND    = 'rec.fnd';    // file in which to list matches found
    // 2022-09-15 to be implemented later:
    const ksOPT_RECORD_ABSENT   = 'rec.abs';    // file in which to list absent matches
    
    // CEMENT caOptsBase

    protected function SetupAppOptions() {}

    // -- SETUP -- //
    // ++ SETTINGS ++ //

    private $arFS = [];
    protected function SetIndexFile(int $idx,string $fs) { $this->arFS[$idx] = $fs; }
    protected function GetIndexFiles() : array { return $this->arFS; }

    /*
    protected function GetFileA() : string { return $this->arFS[0]; }
    protected function NeedFileA() : bool { return !array_key_exists(0,$this->ar); }
    protected function GetFileB() : string { return $this->arFS[1]; }
    protected function NeedFileB() : bool { return !array_key_exists(1,$this->ar); }
    */

    private $arData = [];
    protected function GetIndexData(string $sKey, string $fs) : cFileTreeIndex {
        if (!array_key_exists($sKey,$this->arData)) {
            $od = new cFileTreeIndex;
            $od->ReadFrom($fs);
            #$arFile = $this->ReadFileData($fs);
            #$arData[$fs] = $arFile;
            $arData[$sKey] = $od;
        } else {
            $od = $arData[$sKey];
        }
        return $od;
    }

    // -- SETTINGS -- //
    // ++ ACTION ++ //
    
    /*----
      ACTION: For each index in the input list --
        * assign each one a letter-name (just for conceptual clarity, hope this doesn't create issues)
        * collect all the hash data (hash + files, including duplicates)
        * add the index's name to the list of indexes where that hash was found
    */
    public function DoCompare() {
        $arIdxs = $this->GetIndexFiles();
        $odOut = new cFileIndexComp;
        foreach ($arIdxs as $idx => $fsIdx) {
            $sSet = chr(ord('A') + $idx);   // 0=A. 1=B, etc.
            $odIdx = $this->GetIndexData($sSet,$fsIdx);
            $fpTree = $odIdx->GetPath();
            $odOut->AddSet($sSet,$fpTree,$fsIdx);

            // first, add duplicates
            $arHashes = $odIdx->GetDups();
            $odOut->AddHashes($arHashes,$sSet);
            // next, add singletons
            $arHashes = $odIdx->GetIndex();
            $odOut->AddHashes($arHashes,$sSet);
        }

        $fnOut = cConfigApp::OutputFilename();
        $odOut->WriteTo($fnOut);
    }
    protected function ReadIndex(string $sKey, string $fs) : ?array {
        $oData = $this->GetIndexData($sKey,$fs);
        if ($oData->HasData()) {
            $arFiles = $oData->GetIndex();
            return $arFiles;
        } else {
            echo "ERROR: could not read set '$sKey' data in [$fs].\n";
            return NULL;
        }
    }
}
