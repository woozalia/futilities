#!/usr/bin/php
<?php namespace woozalia\futil;
/*
  PURPOSE: FIC - File Index Comparison utility
    This is designed to work with the output of fti.php.
  INPUT: two fti output files
  OUTPUT: list of files present in the first input (model) but not in the second (compare)
    2022-08-20 Actually, why not output two lists -- one for each direction?
        Then a third utility could act on those lists as appropriate, possibly doing a
        full two-way synch. TODO
  USAGE: php fic.php <fti output file for model folder> <fti output file for compare folder>
  HISTORY:
    2022-08-07 started
*/

require_once('lib/0.php');  // general classes
require_once('lib.php');          // app-specific class

cApp::SetVersionString('File Index Compare v0.03 / 2022-10-07');
cApp::SetDocsURL('https://wooz.dev/Futilities/human/fic');
new cApp($argv);

echo "== DONE ==\n";
