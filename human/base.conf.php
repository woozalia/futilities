<?php namespace woozalia\futil;
/*
  PURPOSE: common configuration stuff for FIC and FTI
    This should never include any sensitive information.
*/
class cConfig {
    // TODO: somehow get this via ANSI?
    static public function ScreenWidth() : int { return 180; }
    // NOTE: Be careful not to accidentally type '.' instead of ',' for the list-item separator.
    // TODO: 2022-10-21 this should probably use regex instead of glob
    static public function MasksToIgnore() : array {
        return [
          'cache*',
          '*.cache*',
          'files_trashbin',
          '*files_versions*',
          '.sync_*',
          '._sync_*',
          'temp',
          '*.tmp',
          '*thumbnails*',
          ];
    }
    // TODO: change this to a userspace folder
    static public function RemoteMountBase() : string { return '/mnt/futil'; }
}
