<?php namespace woozalia\futil;
# 2022-08-21 Not sure this is actually needed, but setting it up so it's there.

include '../base.conf.php';
/*::::
  PARAMETERS:
    HashAlgorithm(): the name of file-hashing algorithm to use, as understood by hash_file()
      Fewer bits are probably fine if 512 is slowing things down,
      but of course all indexes to be compared will need to use the same algorithm.
*/
class cConfigApp extends cConfig {
    static public function OutputFilename() : string { return 'fti.out.json'; }
    static public function HashAlgorithm() : string { return 'sha3-512'; }
}
