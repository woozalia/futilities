#!/usr/bin/php
<?php namespace woozalia\futil;
/*
  PURPOSE: FTI - file tree indexing & comparison utility
  USAGE:
    EXAMPLES:
        php fti.php '<path to folder>'
  HISTORY:
    2022-08-04 started
    2022-08-06 basic functionality working
*/

require_once('conf.php');       // config (class)
require_once('lib/0.php');      // preload class library
require_once('lib.php');        // app-specific classes

cApp::SetVersionString('File Tree Index v0.04 / 2022-10-16');
cApp::SetDocsURL('https://wooz.dev/Futilities/human/fti');
new cApp($argv);
