<?php namespace woozalia\futil;
/*
  PURPOSE: classes specific to the FTI utility
  HISTORY:
    2022-08-05 split off cOptsApp and cFileIndexer from fti.php
    2022-08-22 Trim ending '/' from input path
      This isn't critical, but '//' in some output files might result in mismatches
      during processing by other utilities.
    2022-08-25 
      - made hash algorithm a config option
      - base path needs to be removed from filespecs as given in list
    2022-09-12 reworking a bit to match refactoring elsewhere
      - cApp class
*/
/*::::
  HISTORY:
    2022-09-12 added to match refactoring in FF
*/
class cApp extends caAppBase {
    
    // ++ ACTION ++ //
    
    // CEMENT caAppBase
    protected function Go() {
        $oOpts = $this->Options();

        if ($oOpts->HasSearchArgs()) {
            $om = new cFileIndexer($oOpts);
            #echo $om->DescribeSelf() ."\n";
            $of = new cFolderator;
            
            #csTTY::Open();
            $of->DoSearch($om);
            csTTY::EraseLine(); // erase last status line
            #csTTY::Shut();
        } else {
            echo "You need to specify at least one search argument.\n";
        }
    }

    // -- ACTION -- //
}

class cOpts extends caOptsSpider {
  
    // ++ CONFIG ++ //
  
    // RETURNS: the minimum number of terms (non-option arguments) needed for usefulness
    protected function MinQtyTerms() : int { return 1; }
    // RETURNS: string showing basic command structure for the app
    protected function RenderAppUsage() : string { return $this->GetSelfName()." <folder spec>"; }
    // RETURNS: string describing what is missing
    protected function RenderTooFewTerms(int $qTerms) : string { return 'No input file specified.'; }
    

    // -- CONFIG -- //
    // ++ SETUP ++ //

    protected function SetupAppOptions() {
        // 2022-10-27 This currently just uses the Spider options.
        #$this->AddAppOptions([
    }

    // CEMENT caOptsBase
    protected function HandleTerm($idx,$sTerm) {
        switch ($idx) {
            case 0:
                $fp = rtrim($sTerm,'/');
                $this->SetSourcePath($fp);
                break;
            default:
                $this->HandleUnknownTerm($idx,$sTerm);
        }
    }
    public function DescribeSelf() : string {
        $fp = $this->GetSourcePath();
        return "indexing all files inside the folder '$fp'";
    }
        
    // -- SETUP -- //
}

class cFileIndexer extends caMatchStandard {
  
    // ++ SETUP ++ //

    public function __construct(cOpts $o) { $this->SetOptions($o); }
    
    // -- SETUP -- //
    // ++ ACTIONS: API ++ //
    
    // OVERRIDE: save results
    public function DoFinish() { $this->SaveResults(); }
    
    // RULE: no special actions for folders
    public function HandleFolder(caPathPiece $of) {}
    public function HandleFile(caPathNamed $of) {
        $oMatch = $this->NameMatch($of);
        if ($oMatch->GetIsMatch()) {
            $fsa = $of->GetSpec();
            // TODO: $fsa apparently needs to be escaped, somehow, if filename has weird characters
            $sHash = hash_file(cConfigApp::HashAlgorithm(),$fsa);
            $fsr = $of->GetPath();
            $this->AddFile($sHash,$fsr);
        }
    }
    
    // ++ ACTIONS: CUSTOM ++ //
    
    protected function SaveResults() {
        $oj = new cFileTreeIndex;
        $oj->SetAlgo(cConfigApp::HashAlgorithm());
        $oj->SetPath($this->GetOptions()->GetSourcePath());
        $oj->SetDups($this->FileDups());
        $oj->SetIndex($this->FileIndex());

        $oj->WriteTo(cConfigApp::OutputFilename());
    }
    
    // -- EVENTS -- //
    // ++ ACTION ++ //
    
    private $arIndex = [];
    private $arDups = [];
    protected function AddFile(string $sHash, string $fs) {
        $isDup = array_key_exists($sHash,$this->arIndex);
        $this->arIndex[$sHash][] = $fs;
        if ($isDup) {
            $this->arDups[$sHash] = $this->arIndex[$sHash];
        }
    }
    protected function FileIndex() : array { return $this->arIndex; }
    protected function FileCount() : int { return count($this->arIndex); }
    protected function FileDups() : array { return $this->arDups; }
    
    // -- ACTION -- //
    
}
