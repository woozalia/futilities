<?php namespace woozalia\futil;
/*
  PURPOSE: classes specific to the FLC utility
  HISTORY:
    2022-09-01 started
*/
class cApp extends caAppBase {
    
    // ++ ACTION ++ //
    
    // CEMENT caAppBase
    protected function Go() {
        #csTTY::Open();
        $this->ProcessInput();
        #csTTY::Shut();
    }
    protected function ProcessInput() {
        $oOpts = $this->Options();
                
        $f = new cInputTextDataFile($oOpts->GetListSpec());
        if (!$oOpts->HasTargetPath() or $oOpts->WantPreCount()) {
            $this->CountLines($f);
        }
        if ($oOpts->HasTargetPath()) {
            $this->CopyFiles($f);
        }
    }
    protected function CountLines(cInputTextDataFile $f) {
        $oOpts = $this->Options();
        $nLines = 0;
        
        while (($or = $f->ReadDataLine())->IsOkay()) {
            #$fsr = $or->Text();
            $nLines++;
        }
        $sCount = "$nLines line".(($nLines == 1)?'':'s')."\n";
        if ($oOpts->HasTargetPath()) {
            echo "Processing $sCount";
        } else {
            echo "Counted $sCount";
        }
        $this->SetLineCount($nLines);
        $f->Restart();  // set file pointer back to beginning
    }
    /*----
      NOTE:
        2022-09-09 Do NOT set any part of $of to options->GetSourcePath(),
          as this may include protocol stuff; only use mount->GetLocalSpec().
    */
    protected function CopyFiles(cInputTextDataFile $f) {
        $oOpts = $this->Options();
        $nlCurr = 0;
        $of = new cFileSpec;
        
        $hasCount = $this->HasLineCount();
        if ($hasCount) {
            $nlCount = $this->GetLineCount();
        }
        
        $hasSource = $oOpts->HasSourcePath();
        if ($hasSource) {
            $omSrce = $this->SourceMount();
            $of->SetPartA($omSrce->GetLocalSpec());
        }

        $fpSource = $oOpts->GetSourcePath();
        while (($or = $f->ReadDataLine())->IsOkay()) {
            $fsr = $or->Text();
            $nlCurr++;
            if ($hasCount) {
                $sPct = sprintf('%d',$nlCurr/$nlCount);
                $sStatus = "$sPct% ($nlCurr/$nlCount)";
            } else {
                $sStatus = "($nlCurr)";
            }
            csTTY::ShowLine($sStatus.' '.$fsr);
            $of->SetPartC($fsr);
        #echo "\nSOURCE (2): ".$of->DumpLine()."\n"; die();
            $this->CopyFile($of);
        }
    }
    /*----
      INPUT:
        $of is the filespec for the source file to copy.
          By default it is absolute -- but if there is a SourcePath,
          that is prepended. SourceFileSpec($of) calculates this.
    */
    protected function CopyFile(cFileSpec $of) {
        $oOpts = $this->Options();
        
        // filespec for source
        $fsaSrce = $of->PartCAbsolute();
        
        // calculate destination filespec
        $fpDest = $oOpts->GetTargetPath();
        
        // copy() does not support folder-creation, but mkdir() does
        // so we need to know the folder path that needs to exist
        $fpSrceBase = dirname($fsaSrce);
        $ofDest = clone $of;
        $ofDest->SetPartA($fpDest);
        $fsaDest = $ofDest->PartCAbsolute();
        #echo "\nofDest: ".$ofDest->DumpLine()."\n";
        
        if (file_exists($fsaSrce)) {
            $fpDestBase = dirname($fsaDest);
            // TODO: check that fpDestBase is a folder; delete first if not
            if (!file_exists($fpDestBase)) {
                $ok = @mkdir($fpDestBase,0777,TRUE);
                if ($ok) {
                    $ok = copy($fsaSrce,$fsaDest);
                    if ($ok) {
                        // copy over timestamp too
                        
                    }
                    csTTY::Append(' ok');
                } else {
                    csTTY::KeepLine(' - could not mkdir');
                    csTTY::Keepline("MKDIR failed for [$fpDestBase]");
                    echo "\n";
                    die();  // maybe handle more elegantly later?
                }
            }
        } else {
            csTTY::Append(' NOT FOUND');
            csTTY::NewLine();
        }
    }
    
    // -- ACTION -- //
    // ++ STATE ++ //
    
    private $nLines = NULL;
    protected function HasLineCount() : bool { return !is_null($this->nLines); }
    protected function SetLineCount(int $n) { $this->nLines = $n; }
    protected function GetLineCount() : int { return $this->nLines; }
    
    // -- STATE -- //
}

class cOpts extends caOptsBase {
    use tOpts_HasSourcePath;
    use tOpts_HasTargetPath;

    // ++ CONFIG ++ //

    // CEMENT caOptsBase
    protected function RenderAppUsage() : string {
        $sThis = $this->GetSelfName();
        return "$sThis [<options>] <index file> <source folder> [<target folder>]";
   }
    // CEMENT caOptsBase
    protected function MinQtyTerms() : int { return 2; }
    // CEMENT caOptsBase
    protected function RenderTooFewTerms(int $qTerms) : string {
        if ($qTerms < 1) {
            $out = 'You must at least specify a file index to use.';
        } else {
            // TODO: let's have another term qty limit where only a warning is issued
            $out = 'You also need to specify a source folder to merge.';
        }
        return $out;
    }
    // CEMENT cOptsBase
    public function DescribeSelf() : string {
        $fsList = $this->GetListSpec();
        if ($this->HasTargetPath()) {
            $sVerb = 'Copying';
        } else {
            $sVerb = 'Counting';
        }
        $s = "$sVerb files listed in $fsList";
        if ($this->HasSourcePath()) {
            $s .= "\n from : ".$this->GetSourcePath();
        }
        if ($this->HasTargetPath()) {
            $s .= "\n to   : ".$this->GetTargetPath();
        }
        return $s;
    }
    
    // -- CONFIG -- //
    // ++ SETUP ++ //
    
    const ksOPT_WANT_PRECOUNT   = 'want.count'; // name of config file to use

    protected function SetupAppOptions() {
        $this->AddAppOptions([
          self::ksOPT_WANT_PRECOUNT => ['-pc','--precount'],
          ]);
    }

    protected function HandleTerm($idx,$sTerm) {
        switch ($idx) {
            case 0:
                $this->SetListSpec($sTerm);
                break;
            case 1:
                $this->SetTargetPath($sTerm);
                break;
            case 2:
                $this->SetSourcePath($aTerm);
                break;
            default:
                $this->HandleUnknownTerm($idx,$sTerm);
        }
    }
    
    // -- SETUP -- ///
    // ++ SETTINGS ++ //
    
    // simple option values
    public function WantPreCount()  : bool { return $this->HasCmdOption(self::ksOPT_WANT_PRECOUNT); }

    // input list-file
    private $fsList;
    protected function SetListSpec(string $fs) { $this->fsList = $fs; }
    public function GetListSpec() : string { return $this->fsList; }
    
    // -- SETTINGS -- //
}
