#!/usr/bin/php
<?php namespace woozalia\futil;
/*
  PURPOSE: FCM - File Collection Merge
    Copies a list of files to a relative destination
  HISTORY:
    2022-08-25 started
    2022-10-07 rethought a bit; renamed FCC -> FCM
*/

require_once('lib/0.php');  // general classes
require_once('lib.php');    // app-specific class
require_once('conf.php');   // configuration

cApp::SetVersionString('File Collection Merge v0.00 / 2022-09-13');
cApp::SetDocsURL('https://wooz.dev/Futilities/human/fcm');

new cApp($argv);
echo "== DONE ==\n";
