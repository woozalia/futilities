<?php namespace woozalia\futil\fsys\file;

interface iNative {
    // ++ SETUP ++ //

    static function FromArray(array $ar) : self;
    static function FromFSpec(string $fs) : self;

    // ++ SETUP: readout ++ //

    function HasInfo() : bool;

    // -- SETUP -- //
    // ++ FIELDS ++ //

    function ModTime() : int;
    function ChgTime() : int;

    // -- FIELDS -- //
}

/**::::
  PURPOSE: wrapper for stat()/lstat() results
  HISTORY:
  * 2024-04-29
    * renamed .\cnFileInfo -> .\fsys\file\cNative
    * rewrote to use only static pseudoconstructors
*/
class cNative implements iNative {

    // ++ SETUP ++ //

    protected function __construct(){}  // disallow direct construction

    static public function FromArray(array $ar) : self {
        $oThis = new static;
        $oThis->WithArray($ar);
        return $oThis;
    }

    static public function FromFSpec(string $fs) : self {
        $oThis = new static;
        $ar = @stat($this->fs);
        if (is_array($ar)) {
            $this->WithArray($ar);
        } else {
            $this->WithNull();
        }
        return $oThis;
    }

    // ++ SETUP: dynamic ++ //

    private $ar;
    protected function WithArray(?array $ar)  { $this->ar = $ar; }
    protected function WithNull()             { $this->ar = NULL; }

    // ++ SETUP: readout ++ //

    public function HasInfo() : bool { return is_array($this->ar); }

    // -- SETUP -- //
    // ++ FIELDS ++ //

    public function ModTime() : int { return $this->ar['mtime']; }
    public function ChgTime() : int { return $this->ar['ctime']; }

    // -- FIELDS -- //
}
