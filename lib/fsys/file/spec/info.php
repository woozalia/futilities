<?php namespace woozalia\futil\fsys\file\spec;

use Directory;
##
use woozalia\futil\fsys\file\cNative as NativeClass;
use woozalia\futil\fsys\file\iNative as NativeIface;

/**::::
 * HISTORY:
 * * 2024-04-29 renamed .\cFSpecInfo -> .\fsys\file\spec\cInfo
 */
class cInfo {
    public function __construct(string $fs) { $this->fs = $fs; }

    public function Raw() : string { return $this->fs; }

    public function Exists()      : bool  { return file_exists($this->fs); }
    public function IsLink()      : bool  { return is_link($this->fs); }
    public function IsRegular()   : bool  {
        $fs = $this->fs;
        return is_readable($fs) and (is_file($fs) or is_dir($fs));
    }
    protected function GetPerms()         { return fileperms($this->fs); }
    public function GetTimestamp() : int  { return filemtime($this->fs); }
    public function SetTimestamp(int $dt) { touch($this->fs,$dt); }
    public function IsFile()      : bool  { return is_file($this->fs); }
    public function IsFolder()    : bool  { return is_dir($this->fs); }
    public function IsCopyable()  : bool  { return is_readable($this->fs); }
    public function IsPipe()      : bool  { return (($this->GetPerms() & 010000) != 0); }
    public function GetSize()     : int   { return filesize($this->fs); }

    public function GetDir() : Directory {
        $fp = $this->fs;
        $od = dir($fp);
        if (FALSE === $od) {
            echo "INTERNAL ERROR: dir('$fp') failed.\n";
            throw new \exception('how did we get here?');
            die();
        }
        return $od;
    }

    public function GetInfo() : NativeIface { return NativeClass::FromFSpec($this->fs); }

}
