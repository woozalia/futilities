<?php namespace woozalia\futil\fsys\file;
/*
  PURPOSE: classes for handling file-paths in a structured way
  HISTORY:
    2019-03-31 started, so we can show paths as trees
      Originally written for ffmove, I think; was pathSeg.php.
    2022-09-05 copied to The Futilities and adapted/updated
      ...though it turns out maybe I don't need it [yet].
    2022-09-11 need it now! Some class changes:
        caPathPiece: new base class
        cPathSeg renamed to cPathCap
        cPathBase renamed to cPathRoot, descends from cPathHandler instead of cPath[Seg]
        merged in work from 9/10 accidentally done on separate file
    2022-09-12 added cnPathInfo
*/

class cnURLInfo {
    static public function FromSpec(string $fs) : self { return new self(parse_url($fs)); }

    private $ar;

    public function __construct(array $ar) { $this->ar = $ar; }

    protected function GetItNz(string $sName, $def=NULL, string $sPfx=NULL, string $sSfx=NULL) {
        if (array_key_exists($sName,$this->ar)) {
            return $sPfx . $this->ar[$sName] . $sSfx;
        } else {
            return $def;
        }
    }
    // 2022-10-25 Just using this where immediately needed; will probably need to use in more of the below methods.

    public function Scheme()    : ?string { return $this->ar['scheme']; } // proto:
    public function Host()      : string { return $this->ar['host']; }    // domain/address
    public function Port()      : int    { return $this->ar['port']; }    // port number (if specified)
    public function User()      : string { return $this->ar['user']; }
    public function Pass()      : string { return $this->ar['pass']; }
    public function Path()      : string { return $this->ar['path']; }
    public function PathInfo()  : cnPathInfo { return cnPathInfo::FromSpec($this->Path()); }
    public function Query()     : ?string { return $this->GetItNz('query'); }  // after the question mark ?
    public function Fragment()  : string { return $this->ar['fragment']; }    // after the "#"

    // ++ FIGURED ++ //

    // *Mk() functions include the necessary URL markup *if* the part is set

    public function SchemeMk()    : string    { return $this->GetItNz('scheme','','','://'); }
    public function PortMk()      : string    { return $this->GetItNz('port','',':'); }
    public function PassMk()      : string    { return $this->GetItNz('pass','',':'); }
    public function UserMk()      : string    { return $this->GetItNz('user','','','@'); }

    // 2022-12-25 maybe we need a version of some of these which includes demarcation (e.g. "://") *only if* the piece isn't blank.
    public function BaseURL()   : string {
        return
          $this->SchemeMk()
          .$this->UserMk()
          .$this->Host()
          .$this->PortMk()
          .$this->PassMk()
          .$this->Path()
          ;
    }

    // -- FIGURED -- //
}


// PURPOSE: generic base class for path-handling classes
abstract class caPathPiece implements iPathNamed {

    // ++ ACTION ++ //
    
    public function AddSegment(string $fn) : cPathCap { return new cPathCap($fn,$this); }
    // ACTION: Parse the given relative path and return a new PathPiece for the last segment
    public function ParsePath(string $fpr) : cPathCap {
        $fpClean = trim($fpr,'/');  // make sure there are no extra slashes
        $arPath = explode('/',$fpClean);
        $oSeg = $this;
        foreach ($arPath as $fnSeg) {
            $oSeg = $oSeg->AddSegment($fnSeg);
        }
        return $oSeg;
    }
    
    // -- ACTION -- //
    // ++ FIGURING ++ //

    public function GetName() : string { return $this->Info()->GetInfo()->something; }  // TO BE WRITTEN

    // RETURNS entire filespec, including base
    public function GetSpec() : string { return $this->GetPathStatus()->GetAbsPath(); }
    /*----
      RETURNS relative filespec from base
      PUBLIC because sometimes we need that information
    */
    public function GetPath() : string { return $this->GetPathStatus()->GetRelPath(); }
    
    // -- FIGURING -- //
    // ++ FILESYS: OBJECTS ++ //
    
    abstract public function GetPathStatus() : cPathStatus;
    
    public function GetFileNode() : cFile {
        $fs = $this->GetSpec();
        return new cFile($fs);
    }
    public function GetFolderNode() : cFolder {
        $fs = $this->GetSpec();
        return new cFolder($fs);
    }

    // ++ FILESYS: I/O ++ //
    
    public function Info() : cFSpecInfo { return new cFSpecInfo($this->GetSpec()); }

    // -- FILESYS -- //
    
}
interface iPathNamed {
    function GetName() : string;
}
abstract class caPathNamed extends caPathPiece implements iPathNamed {
    
    // ++ VALUE ++ //

    private $fn;
    public function SetName(string $fn) { $this->fn = $fn; }
    // PUBLIC so we can display just the latest addition to the path
    public function GetName() : string { return $this->fn; }
    
    // -- VALUE -- //
    // ++ PATH ++ //
    
    private $oBase;
    protected function SetBase(caPathPiece $oBase=NULL) { $this->oBase = $oBase; }
    #public function GetBase() : caPathPiece { return $this->oBase; }
    public function GetBasePath() : string {
        if (is_null($this->oBase)) {
            return '';
        } else {
            return $this->oBase->GetPath();
        }
    }

    /*----
      CEMENT caPathHandler
      INSTAR: non-base piece, no numbering
      RETURNS: PathStatus
      HISTORY:
        2022-09-29 revised how cPathStatus works
    */
    public function GetPathStatus() : cPathStatus {
        $oStat = $this->oBase->GetPathStatus();
        #$oStat->nSegs++;
        $sName = $this->GetName();
        $oStat->AddRelPath($sName);
        return $oStat;
    }
        
    // -- PATH -- //
}
/*::::
  PURPOSE: a Path-segment where we don't yet know the name, possibly because we're iterating
    through the parent-folder. (That's the need-case here, anyway,
*/
class cPathBlank extends caPathNamed {
    public function __construct(caPathPiece $oBase) {
        $this->SetName('');
        $this->SetBase($oBase);
    }
}
/*::::
  PURPOSE: a Path-segment for iterating through a directory
    I *think* this will essentially replace cPathBlank.
  HISTORY:
    2022-09-29 started
*/
class cPathFileList extends caPathNamed {
    public function __construct(caPathPiece $oBase, array $arFiles) {
        $this->SetName('');
        $this->SetBase($oBase);
        $this->SetFiles($arFiles);
        if (count($arFiles) == 0) { throw new \exception('Something is wrong here.'); }
    }
    
    private $arFiles;
    protected function SetFiles(array $ar) {
        $this->arFiles = $ar;
        $this->SetCount(count($ar));
    }
    protected function GetFiles() : array { return  $this->arFiles; }
    
    private $nFiles;
    protected function SetCount(int $n) { $this->nFiles = $n; }
    protected function GetCount() : int { return $this->nFiles; }
    
    private $idxFile;
    public function DoReset() { $this->idxFile = 0; }
    public function GoNext() : bool {
        $ar = $this->GetFiles();
        $idx = $this->idxFile;
        $isMore = $idx < $this->GetCount();
        if ($isMore) {
            $this->SetName($ar[$idx]);
            $this->idxFile++;
        }
        return $isMore;
    }
    protected function SelfNumStatus() : string {
        $nThis = $this->idxFile;
        $nAll = $this->GetCount();
        return "$nThis:$nAll";
    }
    public function GetPathStatus() : cPathStatus {
        $oStat = parent::GetPathStatus();
        $oStat->AddInfo('/'.$this->SelfNumStatus());
        return $oStat;
    }
}
/*::::
  PURPOSE: segment of a Path, but also provides an API to the entire parent-path
    from which it dangles; it's the "cap" to a path
  NOTE: "base" in this context means "parent folder", not the root-base of the whole path.
*/
class cPathCap extends caPathNamed {
    public function __construct(string $sName, caPathPiece $oBase) {
        $this->SetName($sName);
        $this->SetBase($oBase);
    }
}

/*::::
  PURPOSE: the root segment of a path-chain; has no parent-folder
*/
class cPathRoot extends caPathPiece {
    public function __construct(string $fp) { $this->SetSpec($fp); }
    
    private $fpBase;
    protected function SetSpec(string $fp) { $this->fpBase = $fp; }
    /*----
      CEMENT caPathHandler
      HISTORY:
        2022-09-29 revised how cPathStatus works
    */
    public function GetPathStatus() : cPathStatus {
        $oStat = new cPathStatus;
        $oStat->SetRootPath($this->fpBase);
        return $oStat;
    }
}
