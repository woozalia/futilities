<?php namespace woozalia\futil\fsys\path;

/**::::
  PURPOSE: wrapper for pathinfo() results
  NOTE: for paths that (might) start with a protocol, use cnURLInfo
  HISTORY:
  * 2024-04-29 renamed .\cnPathInfo -> .\fsys\path\cnInfo
*/
class cnInfo {
    static public function FromSpec(string $fs) : self { return new self(pathinfo($fs)); }

    private $ar;

    public function __construct(array $ar) { $this->ar = $ar; }

    public function FolderPath()    : string { return $this->ar['dirname']; }   // /path/to
    public function FileNameFull()  : string { return $this->ar['basename']; }  // name.ext
    public function FileNameMain()  : string { return $this->ar['filename']; }  // name
    public function FileExtOnly()   : string { return $this->ar['extension']; } // ext

    public function FullSpec() : string {
        return
          $this->FolderPath()
          .$this->FileNameFull()
          ;
    }
}
