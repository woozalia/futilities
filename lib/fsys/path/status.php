<?php namespace woozalia\futil\fsys\path;

/**::::
  PURPOSE: return-object with interrrelated calculated values,
    to eliminate unnecessary recalculation
  HISTORY:
  * 2022-09-29 revamped with access methods instead of public values
      and to be more sensible, generally
  * 2024-04-29 renamed .\cPathStatus -> .\fsys\path\cStatus

*/
class cStatus {

    // ++ VALUES ++ //

    private $fpRoot;	// path of root piece-object
    public function SetRootPath(string $fp) { $this->fpRoot = $fp; }
    protected function GetRootPath() : string { return $this->fpRoot; }

    // NOTE: this will begin with '/'
    private $fpRel = '';	  // path relative to root
    public function SetRelPath(string $fp) { $this->fpRel = $fp; }
    public function AddRelPath(string $fp) { $this->fpRel .= '/'.$fp; }
    public function GetRelPath() : string { return $this->fpRel; }


    // The idea here is that this can be *any* info, in theory, but it was actually written for PathFileList.
    private $sInfo = '';
    public function AddInfo(string $s) { $this->sInfo .= $s; }
    public function GetInfo() : string { return $this->sInfo; }

    // -- VALUES -- //
    // ++ FIGURING ++ //

    public function GetAbsPath() : string {
        return $this->GetRootPath().$this->GetRelPath();
    }

    // -- FIGURING -- //
    // ++ USER OUTPUT ++ //

    #public function

    // -- USER OUTPUT -- //
}
