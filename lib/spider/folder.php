<?php namespace woozalia\futil;
/*
  PURPOSE: classes for managing folders
  HISTORY:
    2022-08-04 Created; moved cFolderator here from ff-lib.php and caMatch here from fileutil.php
      TODO: possibly caMatch should be renamed something like caFound
    2022-08-26 reworking a bunch of stuff having to do with relative paths
    2022-09-28 working again; attempting to sort files found in folders
*/

/*::::
  DETAILS:
    TODO: Main loop is:
      OnPath()
        if it's not a link or a fake folder --
          if it's a folder: OnFolder()
            for each item: OnPath()
          if it's a file: OnFile()
*/
class cFolderator {

    // ++ ACTIONS ++ //

    public function StartSpider(caMatch $om) {
        $this->SetMatcher($om);
        $oOpts = $om->GetOptions();
        $fpBase = $oOpts->GetSourcePath();

        // if we're logging what we find, open an output file
        $oOptsList = $oOpts->ListingOptions();  // get Listing sub-options
        if ($oOptsList->UseLogFile()) {
            $fs = $oOptsList->LogFileSpec();
            $sDescr = $oOpts->DescribeSelf();
            $sTime = date('Y/m/d H:i:s');

            $oLog = new cLog($fs);
            $oLog->WriteLn(cApp::GetVersionString());
            $oLog->Write($sTime.' '.$sDescr);         // $sDescr ends with a CRLF
            $this->SetLogFile($oLog);
        }

        $op = new cPathRoot($fpBase);
        $this->ResetCount();
        $this->OnWantedFolder($op);  // start recursive search with base folder
        echo "\n".$this->SummarizeCount();
        $om->DoFinish();
    }
    /*----
      HISTORY:
        2022-09-20 (former todo) This should probably be renamed Start(), OSLT.
        2022-09-27 added StartSpider() and made this an alias. DEPRECATED; replace with
            call to StartSpider().
    */
    public function DoSearch(caMatch $om) { $this->StartSpider($om);  }
    
    // -- ACTIONS -- //
    // ++ EVENTS ++ //
    
    /*----
      PURPOSE:
        MAIN: filter out links, check whether file/folder, dispatch to appropriate event-handler
        AUX: optionally show items as they are found, where requested
      RULE: We have a valid path to a file or folder that we do want to consider.
        For now, we assume no '.', '..', or links. We might enable link-inclusion
        as an option, later.
      HISTORY:
        2022-08-31 oops, forgot to include the link-check. Fixed.
        2022-10-16 updating to handle more complete logging options
    */
    protected function OnPath(caPathPiece $op) {
        $opInfo = $op->Info();
        if ($opInfo->IsRegular()) {

            // calculations for output list
            $om = $this->GetMatcher();
            $oOpts = $om->GetOptions();

            $oOptsList = $oOpts->ListingOptions();  // get Listing sub-options

            $doLFi = $oOptsList->DoFiles();
            $doLFo = $oOptsList->DoFolders();
            $useScreen = $oOptsList->UseScreen();
            $useLog = $this->HasLogFile();
            if ($useLog) { $oLog = $this->GetLogFile(); }

            if ($doLFi or $doLFo) {
                $fs = $opInfo->Raw();
            }
            
            if ($opInfo->IsFolder()) {
                if ($doLFo) {
                    $snFo = $this->FoCount();
                    $s = "[$snFo FO] $fs";
                    if ($useScreen) { echo $s."\n"; }
                    if ($useLog) { $oLog->WriteLn($s); }
                }
                $this->OnFolder($op);
            } else {
                if ($doLFi) {
                    $snFi = $this->FiCount();
                    $s = "[$snFi FI] $fs\n";
                    if ($useScreen) { echo $s; }
                    if ($useLog) { $oLog->WriteLn($s); }
                }
                $this->OnFile($op);
            }
        }
    }
    /*----
        RULE: we have a valid path to a folder. We need to ask the Matcher if
          it should be spidered into.
    */
    protected function OnFolder(caPathNamed $op) {
        $om = $this->GetMatcher();
        #$of = $this->GetSpec();
        
        if ($om->IsFolderWanted($op)) {
            // YES - so process it
            // Tell the Matcher to do any purpose-specific stuff.
            $om->HandleFolder($op);
            $this->OnWantedFolder($op);
            // track this for summary
            $this->IncFolders();
            
        } else {
            #echo "SKIPPING FOLDER ".$op->GetPath()."\n";
        }
    }
    /**
     * TODO: 2024-04-28 There's a conflict between the intent of ShowProgress() -- which logs to the screen --
     *  and the way some apps keep a fixed-position readout-block on the screen.
     */
    protected function OnWantedFolder(caPathPiece $op) {
        $om = $this->GetMatcher();
        $oOpts = $om->GetOptions();
        $doSP = $oOpts->ShowProgress();
        $ofLog = $oOpts->LogFile();

        // log entering the folder
        $ofLog->LogSourceFolder($op);

        // visual update, if ShowProgress()
        $fp = $op->GetPath();  // spec relative to base
        if ($doSP) {
            $sCount = $this->ShortCount();
            csTTY::ShowLine("$sCount | Fo: $fp");
        }

        
        // read the active directory
        $od = $op->Info()->GetDir();
        // iterate through directory of this folder and copy to array
        $arList = [];
        while (FALSE !== ($fn = $od->read())) {
            // exclude utility entries
            if (($fn != '.') and ($fn != '..')) {
                $arList[] = $fn;
            }
        }
        $od->close();
        if (count($arList) > 0) {

            // sorting the files makes progress easier to detect in large folders
            sort($arList);

            $opDir = new cPathFileList($op,$arList);
            $opDir->DoReset();
            while ($opDir->GoNext()) {
                if (!$opDir->Info()->IsLink()) {    // never go into linked folders
                    if ($doSP) {
                        $sCount = $this->ShortCount();
                        $osCurr = $opDir->GetPathStatus();
                        $fs = $osCurr->GetRelPath();
                        $sIndex = $osCurr->GetInfo();
                        #echo "FS=[$fs]\n";

                        csTTY::ShowLine("$sCount | $sIndex | Fi: $fs");
                    }
                    // process it recursively
                    $this->OnPath($opDir);
                }
            }
        }
    }
    protected function OnFile(caPathNamed $op) {
        $om = $this->GetMatcher();
        if ($om->IsFileWanted($op)) {
            $this->IncFiles();
            // Tell the Matcher to do any purpose-specific stuff.
            $om->HandleFile($op);
        }
    }

    // -- EVENTS -- //
    // ++ STATE ++ //
    
    private $nFi,$nFo;
    protected function ResetCount() { $this->nFi = 0; $this->nFo = 0; }
    protected function IncFiles() { $this->nFi++; }
    protected function IncFolders() { $this->nFo++; }
    protected function SummarizeCount() : string {
        $nFi = $this->nFi;
        $nFo = $this->nFo;
        $sSumm = "Checked $nFi file"
          .(($nFi == 1) ? '' : 's')
          ." in $nFo folder"
          .(($nFo == 1) ? '' : 's')
          ;
        return $sSumm;
    }
    protected function ShortCount() : string {
        $nFi = $this->nFi;
        $nFo = $this->nFo;
        return "fo:$nFo/fi:$nFi";
    }
    protected function FiCount() : string { return $this->nFi; }
    protected function FoCount() : string { return $this->nFo; }
    
    // -- STATE -- //
    // ++ OBJECTS ++ //
    
    private $om;
    protected function SetMatcher(caMatch $om) { $this->om = $om; }
    protected function GetMatcher() : caMatch { return $this->om; }

    private $ofLog = NULL;
    protected function SetLogFile(cLog $of) { $this->ofLog = $of; }
    protected function HasLogFile() : bool { return is_object($this->ofLog); }
    protected function GetLogFile() : cLog { return $this->ofLog; }
    
    /*
    private $of;
    protected function SetSpec(cFileSpec $of) { $this->of = $of; }
    public function GetSpec() : cFileSpec { return $this->of; }
    */
    
    // -- OBJECT -- //
}
