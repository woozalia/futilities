<?php namespace woozalia\futil;
/*::::
  PURPOSE: wraps the result of a string-matching operation
*/
class cMatchResult {
    // sWhat = the string examined - might be just part of the original inquiry
    public function __construct(bool $isMatch, string $sWhat) {
        $this->SetIsMatch($isMatch);
        $this->SetString($sWhat);
    }

    private $isMatch;
    protected function SetIsMatch(bool $b) { $this->isMatch = $b; }
    public function GetIsMatch() : bool { return $this->isMatch; }

    private $sWhat;
    protected function SetString(string $s) { $this->sWhat = $s; }
    public function GetString() : string { return $this->sWhat; }
}
/*::::
  PURPOSE: abstract wrapper for determining what constitutes a "match" and what do do when one happens
  HISTORY:
    2022-07-29 created
    2022-08-05 Moved abstract HandleFile() here from caMatchFF,
      because every Match class should be able to describe what it is doing.
*/
abstract class caMatch {

    // ++ ACTIONS ++ //

    public function DoStart() {}   // STUB
    public function DoFinish() {}  // STUB

    abstract public function IsFolderWanted(caPathNamed $of) : bool;
    abstract public function IsFileWanted(caPathNamed $of) : bool;
     // handle a folder known to be wanted
    public function HandleFolder(caPathPiece $of) {
        $sClass = get_called_class();
        $sMethod = __METHOD__;
        die("CODING PROMPT: Class $sClass needs to implement $sMethod.\n");
    }
    abstract public function HandleFile(caPathNamed $of);   // handle a file known to be wanted

    // -- ACTIONS -- //
    // ++ SETTINGS ++ //

    private $oOpts;
    protected function SetOptions(cOpts $o) { $this->oOpts = $o; }
    public function GetOptions() : cOpts { return $this->oOpts; }

    // -- SETTINGS -- //
}
/*::::
  PURPOSE: Matcher class with some common defaults
  HISTORY:
    2022-09-30 moved to here from FTM: the version of DescribeSelf()
      which just passes the buck to Options. This seems like a good default.
      Maybe it should even go in caMatch.
    2022-11-04 Removed DescribeSelf() -- doesn't seem to be used.
      If we do need it, let's call it DescribeMatch(), to avoid confusion.
*/
abstract class caMatchStandard extends caMatch {

    // ++ CONDITIONS: API ++ //

    // RULE: all folders searched unless in ignore list
    public function IsFolderWanted(caPathNamed $of) : bool {
        $fnThis = $of->GetName();
        $doUse = !$this->IsIgnorable($fnThis);
        #echo " || NAME=[$fnThis] - ".($doUse ? 'ok' : 'IGNORE')."\n";
        #if (!$doUse) { echo " IGNORING FOLDER $fnThis\n"; }
        return $doUse;
    }
    // RULE: files searched if mask matches
    public function IsFileWanted(caPathNamed $of) : bool {
        $fnThis = $of->GetName();
        if ($this->IsIgnorable($fnThis)) {
            $bWant = FALSE;
            #echo " IGNORING FILE $fnThis\n";
        } else {
            $omRes = $this->NameMatch($of);
            $this->SetTextMatch($omRes);
            $bWant = $omRes->GetIsMatch();
        }
        return $bWant;
    }

    // ++ CONDITIONS: SUB ++ //

    /*----
      HISTORY:
        2022-09-28 renamed from FileMatch() to NameMatch(), because that's more accurately descriptive
    */
    protected function NameMatch(caPathNamed $of) : cMatchResult {
        $fnThis = $of->GetName();

        $oOpts = $this->GetOptions();
        if ($oOpts->HasFileMask()) {
            $fnMask = $oOpts->GetFileMask();
            $nFlags = ($oOpts->CaseSensitive() ? 0 : FNM_CASEFOLD);
            $bOk = fnmatch($fnMask,$fnThis,$nFlags);
        } else {
            $bOk = TRUE;
        }

        $oRes = new cMatchResult($bOk,$fnThis);
        return $oRes;
    }
    protected function IsIgnorable(string $fn) : bool {
        // check against ignore list
        $arIgnore = cConfig::MasksToIgnore();
        $doIgnore = FALSE;
        #echo "\nCHECKING [$fn]...\n";
        foreach ($arIgnore as $fnMask) {

            /*
            // DEBUGGING
            if (strpos($fnMask,'cache') !== FALSE) {
                $okGlob = fnmatch($fnMask,$fn);
                $dxCache = strpos($fn,'cache');
                if ($dxCache !== FALSE) {
                    echo "\n - [$fn] matches [$fnMask]: [$okGlob] @[$dxCache]\n";
                }
            }
            */

            if (fnmatch($fnMask,$fn)) {
                $doIgnore = TRUE;
                break;  // no need to check further
            }
        }
        #echo ($doIgnore ? 'IGNORE' : 'ok')."\n";
        return $doIgnore;
    }

    // -- CONDITIONS -- //

    private $omRes;
    protected function SetTextMatch(cMatchResult $o) { $this->omRes = $o; }
    protected function GetTextMatch() : cMatchResult { return $this->omRes; }
}
