<?php namespace woozalia\futil;

class cListingOptions extends cSubOpts {
    const ksARG_LIST_INPUT        = 'i';          // what to include in listings
      const ksVAL_LIST_FOUND_FILES   = 'fi';     // list found files
      const ksVAL_LIST_FOUND_FOLDERS = 'fo';     // list found folders
    const ksARG_LIST_OUTPUT_FILE  = 'f';      // filename for listing output
    const ksARG_LIST_OUTPUT_SCRN  = 's';      // show listing on screen

    public function UseScreen() : bool { return $this->HasOption(self::ksARG_LIST_OUTPUT_SCRN); }
    public function UseLogFile() : bool { return $this->HasOption(self::ksARG_LIST_OUTPUT_FILE); }
    public function LogFileSpec() : string { return $this->GetOption(self::ksARG_LIST_OUTPUT_FILE)->GetVal(); }

    protected function InputTypes() : string { return $this->GetOptionNz(self::ksARG_LIST_INPUT); }
    public function DoFiles() : bool { return strpos($this->InputTypes(),self::ksVAL_LIST_FOUND_FILES) !== FALSE; }
    public function DoFolders() : bool { return strpos($this->InputTypes(),self::ksVAL_LIST_FOUND_FOLDERS) !== FALSE; }

    // ++ UI ++ //

    public function DescribeSelf() : string {
        $bFi = $this->DoFiles();
        $bFo = $this->DoFolders();
        $bFB = ($bFi and $bFo);   // both files and folders
        $bScr = $this->UseScreen();
        $bLog = $this->UseLogFile();
        $bOB = ($bScr and $bLog); // both screen and logfile
        if ($bLog) { $fsLog = $this->LogFileSpec(); }

        $s = 'list '
          .($bFi?'files':'')
          .($bFB?' and ':'')
          .($bFo?'folders':'')
          .' '
          .($bScr?'onscreen':'')
          .($bOB?' and ':'')
          .($bLog?"logfile ($fsLog)":'')
          ;
        return $s;
    }

    // -- UI -- //
}

/*::::
  PURPOSE: Options for folder-tree crawlers
  HISTORY:
    2022-08-31 created
    2022-09-13 moved from opt.php to fspider.php
*/
abstract class caOptsSpider extends caOptsBase {
    use tOpts_HasSource_Mountable;

    const ksOPT_LIST_FOUND      = 'list.chk';   // show found items, one line each

    /*
    const ksOPT_SHOW_COMMANDS   = 'show.cmd';   // show each command used (grep)
    const ksOPT_USE_SUDO        = 'use.sudo'  ; // where possible/applicable, use sudo
    */

    protected function SetupOptions() {
        parent::SetupOptions();
        $this->SetupSpiderOptions();
    }
    protected function SetupSpiderOptions() {
        $this->AddAppOptions([
          self::ksOPT_LIST_FOUND    => ['-lf','--list-found'],
        ]);
    }

    // ++ NEW OPTIONS ++ //

    // TODO: change name to something that indicates we're listing all items found (ListFound()? ListSearched()?)
    public function ListingOptions() : cListingOptions { return $this->GetSubOpts(self::ksOPT_LIST_FOUND,cListingOptions::class); }

    // mask for files to match
    private $fnMask = NULL;
    protected function SetFileMask(string $fn) { $this->fnMask = $fn; }
    public function GetFileMask() : string { return $this->fnMask; }
    public function HasFileMask() : bool { return is_string($this->fnMask); }

    // -- NEW OPTIONS -- //

}

