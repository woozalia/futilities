<?php namespace woozalia\futil;
/*::::
  HISTORY:
    2017-12-21 created to make it easier to sum up file sizes without trying to stat un-stat-able files
      need to encapsulate detection of whether something is a copyable file, at least
    2018-01-19 added GetIsCopyable()
    2022-09-10 copied from old FileFerret repo; tidying and updating
    2022-09-11 moving all functions which just use GetSpec() to the PathSeg class
*/
class cFile extends cFSNode {

    // ++ STATUS ++ //

    private $fh=NULL;	// file handle
    protected function SetNative($fh) {	$this->fh = $fh; }
    protected function GetNative() { return $this->fh; }
    protected function IsOpen() : bool { return (!is_null($this->GetNative())); }
    
    // -- STATUS -- //
    // ++ CALCULATIONS ++ //

    /* 2022-10-25 old code expects this, but it's actually in caPathPiece
    private $canCopy = NULL;
    public function GetIsCopyable() {
        if (is_null($this->canCopy)) {
            $this->canCopy = is_readable($this->GetSpec());	// best guess: if it can be read, it can be copied
        }
        return $this->canCopy;
    }
    */

    // -- CALCULATIONS -- //
    // ++ OPERATIONS ++ //
    
    public function OpenForRead() { return fopen($this->GetSpec(),'r'); }
    // ACTION: opens a file for writing; creates if not found; creates parent folder if not found
    public function OpenForWrite() {
        $fs = $this->GetSpec();
        $fp = dirname($fs);
        if (!file_exists($fp)) {
            $ok = mkdir($fp,0777,TRUE);
            if (!$ok) {
          die("COULD NOT CREATE [$fp].\n");
            }
        }
        return fopen($fs,'w');
    }
    public function Shut() {
        if ($this->IsOpen()) {
            fclose($this->GetNative());
            $this->SetNative(NULL);
        }
    }

    // -- OPERATIONS -- //
    // ++ ACTIONS ++ //
    
    // CEMENT
    public function Delete() {
        $fs = $this->GetSpec();
        $ok = unlink($fs);
        return $ok;
    }

    // -- ACTIONS -- //

}
