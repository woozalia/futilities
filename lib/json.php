<?php namespace woozalia\futil;
/*
  PURPOSE: class for managing JSON-data stored in files
  TODO:
    2022-10-28 This should probably be split into two class-families: one for handling data storage
      (JSON being what we use now, but it could be XML or a database) and one for R/W of the loaded data.
      Ideally, the R/W handler should be switchable at runtime.
  HISTORY:
    2022-10-07 started, because it seemed like it was needed
*/

class cJSON {
    private $arData = [];
    protected function SetData(array $ar) { $this->arData = $ar; }  // used by ReadFrom()
    public function &GetData() : array { return $this->arData; }
    public function SetItem(string $sName, $val) { $this->arData[$sName] = $val; }
    public function HasData() : bool { return count($this->arData) > 0; }

    public function ReadFrom(string $fs) {
        $fsNorm = realpath($fs);
        if (is_string($fsNorm)) {
            $sData = @file_get_contents($fsNorm);
            if ($sData === FALSE) {
                csTTY::KeepLine("WARNING: Could not read from [$fs].");
                $arData = NULL;
            } else {
                $arData = @json_decode($sData,TRUE);
                if (is_null($arData)) {
                    csTTY::KeepLine("WARNING: Could not decode data in [$fs].");
                }
            }
        } else {
            csTTY::KeepLine("FILE ERROR: [$fs] appears to be inaccessible.");
            $arData = NULL;
        }
        $this->SetData($arData);
    }
    public function WriteTo(string $fsOut) {
        $arOut = $this->GetData();
        $jsOut = json_encode($arOut,JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        if ($jsOut === FALSE) {
            csTTY::KeepLine("ERROR encoding results.");
        } else {
            $ok = file_put_contents($fsOut,$jsOut);
            if ($ok) {
                csTTY::KeepLine("Results ($ok bytes) written to [$fsOut].\n");
            } else {
                csTTY::KeepLine("ERROR writing results to [$fsOut].");
            }
            csTTY::NewLine();
        }
    }
}
/*::::
  PURPOSE: data organized in the way that FTI organizes it, so others can use that data
  STRUCTURE:
    "tree.path": "<path to root of tree>",
    "hash.algo": "<name of hash algorithm, currently sha3-512 by default>",
    "dups": [],
    "index":
        <hash>
            <filespec>
        (repeated)
  HISTORY:
    2022-10-09 created
    2022-10-10 working for output
*/
class cFileTreeIndex extends cJSON {
    public function SetAlgo(string $s) { $this->SetItem('hash.algo',$s); }
    public function GetAlgo() : string { return $this->GetData()['hash.algo']; }

    public function SetPath(string $fp) { $this->SetItem('tree.path',$fp); }
    public function GetPath() : string { return $this->GetData()['tree.path']; }

    public function SetIndex(array $ar) { $this->SetItem('index',$ar); }
    public function GetIndex() : array { return $this->GetData()['index']; }

    public function SetDups(array $ar) { $this->SetItem('dups',$ar); }
    public function GetDups() : array { return $this->GetData()['dups']; }
}
/*::::
  PURPOSE: data organized in the way that FIC organizes it
  STRUCTURE:
    sets: (array)
      <set name>:
        fs.index: <filespec to set's index file>
        fp.tree: <path that was indexed>
    index: (array)
      <hash>:
        sets: array of
          <set name>: array of <filespec>

  HISTORY:
    2022-10-10 started
    2022-10-28 tree and index were reversed in the JSON output; fixed
*/
class cFileIndexComp extends cJSON {

    public function AddSet(string $sSet, string $fpTree, string $fsIndex) {
        $arSets =& $this->GetData()['sets'];
        $arSets[$sSet] = [
          'fs.index' => $fsIndex,
          'fp.tree' => $fpTree,
          ];
    }
    protected function AddHash(string $sHash, array $arFiles, string $sSet) {
        $arHashes =& $this->GetData()['index'];
        $arHash =& $arHashes[$sHash];
        $arHash['sets'][$sSet] = $arFiles;
    }
    public function AddHashes(array $ar, string $sSet) {
        foreach($ar as $sHash => $arFiles) {
            $this->AddHash($sHash,$arFiles,$sSet);
        }
    }
}
