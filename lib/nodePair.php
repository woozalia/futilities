<?php namespace woozalia\futil;
/*
  HISTORY:
    2017-12-21 extracting general-purpose classes from ensure_subset.php
    2019-04-05 added cPathCompareStatus
    2022-10-19 updating for FUtils
*/
class cPathCompareStatus {
    private $n,$dx,$oPath;

    public function __construct(caPathPiece $oPath, $n,$dx) {
        $this->oPath = $oPath;
        $this->n = $n;
        $this->dx = $dx;
    }
    public function GetProgressText() { return $this->dx.'/'.$this->n;  }
    public function GetPathObject() { return $this->oPath; }
}

class cFSNodePair {

    // ++ SETUP ++ //

    public function __construct(caPathPiece $oPathA,caPathPiece $oPathB) { $this->SetPaths($oPathA,$oPathB); }

    private $oPathA,$oPathB;
    protected function SetPaths(caPathPiece $oPathA,caPathPiece $oPathB) {
        $this->oPathA = $oPathA;
        $this->oPathB = $oPathB;
    }
    // PUBLIC so results can be displayed
    public function GetPathA() : caPathPiece { return $this->oPathA; }
    protected function GetPathB() : caPathPiece { return $this->oPathB; }

    // -- SETUP -- //
}
