<?php namespace woozalia\futil;
/*
  HISTORY:
    2018-01-20 created cFSNode
    2022-09-10 copied from old FileFerret repo; tidying and updating
*/

/*::::
  HISTORY:
    2018-01-20 created as a common parent for cFile and cFolder
    2022-09-11 moving all functions which just use GetSpec() to the PathSeg class
*/
abstract class cFSNode {

    // ++ SETUP ++ //

    public function __construct(string $fs) { $this->SetSpec($fs); }
    
    private $fs;
    protected function SetSpec(string $fs) { $this->fs = $fs; }
    public function GetSpec() : string { return $this->fs; }

    // -- SETUP -- //
    // ++ CALCULATIONS ++ //

    
    // -- CALCULATIONS -- //
    // ++ ACTIONS ++ //
    
    abstract public function Delete();
    public function DeleteVisibly() : bool {
        $oApp = cApp::Me();
        $oScrn = $oApp->Screen();

        $ok = $this->Delete();
        $fs = $this->GetSpec();
        if ($ok) {
            $oScrn->ShowAction( "DELETED [$fs]" );
        } else {
            $oScrn->ShowAction( "Could not delete [$fs]." );	// just leave it there, no need to die()
        }
        $oApp->Options()->LogFile()->RecordDeletion($fs);
        return $ok;
    }

    // -- ACTIONS -- //

}
