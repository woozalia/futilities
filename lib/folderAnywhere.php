<?php namespace woozalia\futil;

/*
  HISTORY:
    2017-12-08 (or thereabouts): created to manage mounting of remote file systems for ensure_subset.php
      (which will probably get renamed at some point)
    2021-07-15 tidying and updating
    2022-09-07 copied from old FileFerret repo; tidying and updating
*/

// PURPOSE: translates URLs into file paths, mounting remote folders as needed
abstract class caFolderAnywhere {

    // ++ SETUP ++ //

    /*----
      PURPOSE: Check to see if the location has already been mounted. Mount it if not.
      ACTION:
        * if URL includes a protocol, parse it: proto://[user@]host/path
        * invoke the appropriate class to handle the URL
    */
    static public function MakeObject($url) {
        // see if there is a ':' before the first '/'
        $idxColon = strpos($url,':');
        $idxSlash = strpos($url,'/');
        //echo "COLON=[$idxColon] SLASH=[$idxSlash]\n";
        if ($idxColon < $idxSlash) {
            // ...then the URL starts with a protocol.
            $sProto = substr($url,0,$idxColon); // get the protocol string
            echo "PROTOCOL=[$sProto]\n";
            $sRem = substr($url,$idxColon+1);   // "://" etc.
            #echo "SREM=[$sRem]\n";
            $idxSlash = strpos($sRem,'//');
            $uri = substr($sRem,$idxSlash+2);  // everything after "://"
            #$oFolder = self::MakeMount($sProto,$sUser,$sHost,$sPath);
            $oFolder = self::MakeMount($sProto,$uri);
        } else {
            $sPath = $url;
            //echo "PATH=[$sPath]\n";
            $oFolder = new cFolderLocal($sPath);
        }
        return $oFolder;
    }
    static private $arMounts = array();
    static protected function MakeMount(string $sProto,string $uri) {
        switch ($sProto) {
          case 'dav':   $sClass = cFolderDAV::class; break;
          case 'file':  $sClass = cFolderFile::class; break;
          case 'sftp':  $sClass = cFolderSFTP::class; break;
          default:
            echo "ERROR: protocol [$sProto] is not currently supported.\n";
            die();
        }
        $oMount = new $sClass($uri);
        return $oMount;
    }

    // -- SETUP -- //
    // ++ OPTIONS ++ //
    
    // NOTE 2022-09-23 This is currently only used by caFolderRemote; documentation needed.
    private $useSudo = FALSE;
    public function SetUseSudo(bool $b) { $this->useSudo = $b; }
    protected function GetUseSudo() : bool { return $this->useSudo; }

    // -- OPTIONS -- //
    // ++ FIGURING ++ //

    abstract public function GetLocalSpec() : string ;
    
    // -- FIGURING -- //
    // ++ LIFECYCLE ++ //
    
    abstract public function Open();
    abstract public function Shut();

    // -- LIFECYCLE -- //
}
/*::::
  RULE:
    URI is just the folder spec
*/
class cFolderLocal extends caFolderAnywhere {
    public function __construct(string $uri) { $this->SetLocalSpec($uri); }
    
    // ++ LIFECYCLE ++ //
    
    public function Open()	{}	// no action needed
    public function Shut()	{}	// no action needed

    // -- LIFECYCLE -- //
    // ++ VALUE ACCESS ++ //
    
    private $fp;
    protected function SetLocalSpec(string $fp) { $this->fp = $fp; }
    // CEMENT
    public function GetLocalSpec() : string { return $this->fp; }
    
    // ++ VALUE ACCESS ++ //
}
/*::::
  PURPOSE: Folder on a remote server which gets mounted somewhere locally
  RULE:
    URI is the part after the "<protocol>://"
        Format: [<user>@]<host>/<path>
  HISTORY:
    2022-11-04 Added destructor that closes any open connection, in hopes of reducing hangs
      after interrupted sessions.
*/
abstract class caFolderRemote extends caFolderAnywhere {

    // ++ SETUP ++ //

    public function __construct(string $uri) {
        $idxSlash = strpos($uri,'/');      // this might include a path
        if ($idxSlash == FALSE) {
            // This is just a connection spec.
            $sConn = $uri;
            $sPath = '';
        } else {
            // This spec includes a path
            $sConn = substr($uri,0,$idxSlash); // connection ([user]@host)
            $sPath = substr($uri,$idxSlash);	  // don't include first '/'
        }
        #echo "SCONN=[$sConn]\n";

        $idxAt = strpos($sConn,'@');            // check for optional user@
        if ($idxAt !== FALSE) {
            $sUser = substr($sConn,0,$idxAt);
            $sHost = substr($sConn,$idxAt+1);
        } else {
            $sUser = NULL;  // for diagnostic
            $sHost = $sConn;
        }
        $this->SetHost($sHost);
        $this->SetPath($sPath);
        $this->SetUser($sUser);
    }
    public function __destruct() { $this->Shut(); }

    // -- SETUP -- //
    // ++ VALUE ACCESS ++ //

    private $sUser=NULL;
    protected function SetUser(?string $s) { $this->sUser = $s; }
    protected function GetUser() : ?string { return $this->sUser; }
    
    private $sHost=NULL;
    protected function SetHost(string $s) { $this->sHost = $s; }
    protected function GetHost() : string { return $this->sHost; }
    
    private $sPath=NULL;
    protected function SetPath(string $s) { $this->sPath = $s; }
    protected function GetPath() : string { return $this->sPath; }
    
    // for making unique mountpoints
    private $sStamp = NULL;
    protected function GetStamp() : string {
        if (is_null($this->sStamp)) {
            $this->sStamp = date('Y-m-d.His');
        }
        return $this->sStamp;
    }
    
    // -- VALUE ACCESS -- //
    // ++ VALUE CALCULATIONS ++ //
    
    protected function GetMountFileSpec() : string {
        $sHost = $this->GetHost();
        $sStamp = $this->GetStamp();
        $fpMounts = cConfig::RemoteMountBase();
        $fsMount = "$fpMounts/$sHost-$sStamp";
        return $fsMount;
    }
    public function GetLocalSpec() : string { return $this->GetMountFileSpec(); } // ALIAS
    /*----
      RETURNS: spec for connection
        * [user@]host/path
        * no protocol
    */
    protected function GetConnSpec() : string {
        $sHost = $this->GetHost();
        $sUser = $this->GetUser();
        
        return (is_null($sUser)?'':"$sUser@") . $sHost;
    }
    protected function GetPath_esc() : string {
        return addslashes($this->GetPath());
    }
    abstract protected function GetMountCommand() : string;
    protected function GetUnmountCommand() {
        $fpLocal = $this->GetMountFileSpec();
        $fpqLocal = addslashes($fpLocal); // escape the spec string for CLI usage
        $sCmd = 'umount '.$fpqLocal;
        return $sCmd;
    }
    
    // -- VALUE CALCULATIONS -- //
    // ++ OPERATIONS ++ //

    protected function CheckMountpoint() : bool {
        $fpLocal = $this->GetMountFileSpec();	// where the remote will appear locally
        if (file_exists($fpLocal)) {
            // make sure nothing is mounted (2021-07-15 I *think* this is to prevent collision with incomplete processes?)
            echo " - Error: Duplicate mount point [$fpLocal] found; making sure it is unmounted...";
            $sCmd = $this->GetUnmountCommand();
            exec($sCmd,$arOut,$nStatus);
            echo "done (exit code $nStatus)\n";
            $ok = FALSE;
        } else {
            echo " - Creating mount folder [$fpLocal]...";
            $doSudo = TRUE;
            if (!$this->GetUseSudo()) {
                // If not explicitly using sudo, try nonsudo first:
                $ok = @mkdir($fpLocal,0777,TRUE);	// create the root mount folder (recursively if needed)
                if ($ok) {
                    $doSudo = FALSE;
                } else {
                    echo "\nCouldn't create mount folder; trying sudo...\n";
                }
            }
            if ($doSudo) {
                $fpeLocal = escapeshellarg($fpLocal);
                $sCmd = "sudo mkdir -p -v --mode=777 $fpeLocal";
                $ok = passthru($sCmd);
            }
            if ($ok === FALSE) {
                echo "\nMount Failed: $sCmd\n";
                #die();
            } else {
                $ok = TRUE;
            }
            $sStatus = $ok?'ok':'ERROR';
            echo " $sStatus\n";
        }
        return $ok;
    }
    
    public function Open() {
        $sHost = $this->GetHost();
        $fpRemote = $this->GetPath();
        
        echo "Creating mountpoint:\n";
        
        if ($this->CheckMountpoint()) {
            $sCmd = $this->GetMountCommand();
            if ($this->GetUseSudo()) {
                $sCmd = 'sudo '.$sCmd;
            }
            echo " - Mounting $fpRemote (on $sHost):"
              ."\n -- command: [$sCmd]"
              ."\n -- "
              ;
            #exec($sCmd,$arOut,$nStatus);
            exec($sCmd,$arOut,$nStatus);
            if ($nStatus == 0) {
                echo "ok.\n";
            } else {
                echo "ERROR $nStatus; unmounting...";
                $sCmd = $this->GetUnmountCommand();
                exec($sCmd,$arOut,$nStatus);
                echo " unmount complete (exit code $nStatus)\n";
                die();
            }
        } else {
            die();
        }
    }
    public function Shut() {
        $fpLocal = $this->GetMountFileSpec();
        echo " - Closing (unmounting) [$fpLocal]...";
        $sCmd = $this->GetUnmountCommand();
        exec($sCmd,$arOut,$nStatus);
        echo "done (exit code $nStatus)\n";
    }
    
    // -- OPERATIONS -- //
}
/*
  NOTE:
    2021-07-15 not sure this was ever tested
*/
class cFolderDAV extends caFolderRemote {
	// mount -t davfs office.sageandswift.com/owncloud/remote.php/webdav /mnt/test
    protected function GetMountCommand() : string {
        $sConn = $this->GetConnSpec();
        $fpqPath = $this->GetPath_esc();
        $fpqMount = '"'.addslashes($this->GetMountFileSpec()).'"';
        return "mount -t davfs $sConn$fpqPath $fpqMount";
    }
}
/*::::
  PURPOSE: This is just so I can paste URLs directly from Caja without having to strip off the "file:///".
  HISTORY:
    2022-10-06 created as stub, and (obviously) not tested
    2022-11-04 seems to have been working ok for a week or so now; I should maybe have made a note.
*/
class cFolderFile extends cFolderLocal {}
/*::::
  NOTE:
    sshfs [user@]host:[dir] mountpoint [options]
  HISTORY:
    2022-09-08 now defaults $fpqRemote to "/" so that a blank string
      takes us to "/", not "/<user>".
      We may want to change this behavior in future, because permissions,
      but wait for a need-case so we can figure out how it needs to work.
*/
class cFolderSFTP extends caFolderRemote {
    protected function GetMountCommand() : string {
        $sConn = $this->GetConnSpec();
        $fpqRemote = $this->GetPath_esc();
        $fpMount = $this->GetMountFileSpec();
        if ($fpqRemote == '') { $fpqRemote = '/'; }
        $fpqMount = addslashes($fpMount);
        #echo "SCONN=[$sConn]\n";
        return "sshfs '$sConn:$fpqRemote' '$fpqMount'";
    }
}
