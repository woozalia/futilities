<?php namespace woozalia\futil;
/*
  PURPOSE: simple array-based autoloader for The Futilities
  HISTORY:
    2022-09-03 started
*/
$arFiles = [
    'app.php'  => [
      caAppBase::class,
      caAppScreen::class,
      ],
    'cio.php'  => [
      csTTY::class,
      ],
    'file.php'  => [
      cFile::class,
      ],
    'filePair.php'  => [
      cFilePair::class,
      ],
    'fio.php'  => [
      cInputFile::class,
      cInputTextDataFile::class,
      cLog::class,
      ],
    'folderAnywhere.php' => [
      caFolderAnywhere::class,
      ],
    'fpath.php'  => [
      cPathCap::class,
      cnPathInfo::class,
      cnURLInfo::class,
      caPathNamed::class,
      caPathPiece::class,
      cPathRoot::class,
      ],
    'fsnode.php'  => [
      cFSNode::class,
      ],
    'json.php'  => [
      cJSON::class,
      cFileTreeIndex::class,
      cFileIndexComp::class,
      ],
    'nodePair.php'  => [
      cFSNodePair::class,
      ],
    'opt.php'  => [
      cSubOpts::class,
      caOptsBase::class,
      ],
    'opt-std.php'  => [
      tOpts_HasSource_Mountable::class,
      ],
    'screen.php'  => [
      cScreenANSI::class,
      ],

    // SPIDER

    'spider/folder.php'    => [
      cFolderator::class,
      ],
    'spider/match.php'    => [
      caMatchStandard::class,
      ],
    'spider/opt.php'    => [
      cListingOptions::class,
      caOptsSpider::class,
      ],
  ];

class csClassLoader {

    static private $arCls;
    static private $arFi;

    static public function Setup(array $arFiles) {
        self::$arFi = $arFiles;
        
        // Populate class-name index:
        $arClasses = [];
        foreach ($arFiles as $fn => $arInFile) {
            foreach ($arInFile as $sClass) {
                #echo "Adding class $sClass\n";
                $arClasses[$sClass] = $fn;
            }
        }
        self::$arCls = $arClasses;
    }
    static public function AutoCallback($sc) {
        if (array_key_exists($sc,self::$arCls)) {
            $fn = self::$arCls[$sc];
            include $fn;
        } else {
            echo "FUTILITY PANIC: The class '$sc' has not been registered.\n";
        }
    }
    
}
csClassLoader::Setup($arFiles);
spl_autoload_register(__NAMESPACE__.'\csClassLoader::AutoCallback');
