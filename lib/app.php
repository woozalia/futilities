<?php namespace woozalia\futil;
/*
  PURPOSE: class for managing overall app structure
  HISTORY:
    2022-09-08 started
    2023-12-23 made Options() PUBLIC. If it is supposed to be accessed via some other means,
      that needs to be better documented.
*/
abstract class caAppBase {
  
    // ++ SETUP ++ //

    static private $oApp = NULL;
    static public function Me() : self { return self::$oApp; }
    
    static private $sVer = NULL;
    static public function SetVersionString(string $s) { self::$sVer = $s; }
    static public function GetVersionString() : string { return self::$sVer; }
    static protected function HasVersionString() : bool { return !is_null(self::$sVer); }

    static private $urlDoc = NULL;
    static public function SetDocsURL(string $url) { self::$urlDoc = $url; }
    static public function GetDocsURL() : string { return self::$urlDoc; }
    static protected function HasDocsURL() : bool { return !is_null(self::$urlDoc); }
    
    /*----
      NOTE: each app must define its own cOpts class.
    */
    public function __construct(array $arCmdArgs) {
        self::$oApp = $this;
        echo static::DescribeSelf();
        csTTY::Open();
        $this->oOpts = new cOpts($arCmdArgs);
        echo $this->oOpts->DescribeSelf()."\n";
        $this->Go();
    }
    public function __destruct() {
        csTTY::Shut();
    }

    private $oOpts;
    public function Options() : cOpts { return $this->oOpts; }

    // -- SETUP -- //
    // ++ STARTUP ++ //
    
    static protected function DescribeSelf() : string {
        self::CheckSetup();
        $sVer = self::GetVersionString();
        $sBar = str_repeat('=',strlen($sVer))."\n";
        cli_set_process_title($sVer);
        return $sBar.$sVer."\n".$sBar;
    }
    static protected function CheckSetup() {
        $sc = get_called_class();
        if (!self::HasVersionString()) {
            die("CODING INCOMPLETE: please call $sc::SetVersionString().\n");
        } elseif (!self::HasDocsURL()) {
            die("CODING INCOMPLETE: please call $sc::SetDocsURL().\n");
        }
    }
    abstract protected function Go();
    
    // -- STARTUP -- //
    
}
// PURPOSE: an App with a Screen object
abstract class caAppScreen extends caAppBase {
    abstract protected function ScreenClass() : string;
    private $oScrn = NULL;
    public function Screen() : caScreen {
        if (is_null($this->oScrn)) {
            $cs = $this->ScreenClass();
            $this->oScrn = new $cs;
        }
        return $this->oScrn;
    }
}
