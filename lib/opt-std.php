<?php namespace woozalia\futil;
/*
  PURPOSE: classes for common option settings
  HISTORY:
    2022-10-20 moved the tOpts_* traits from ./opt.php.
*/


/*::::
  PURPOSE: support for a standard source-path option
    This version is specifically if we *don't* want remote-mount support.
    Use tOpts_HasSource_Mountable instead if we *do* want that.
*/
trait tOpts_HasSourcePath {

    private $fpSrce = NULL;
    protected function SetSourcePath(string $fs) { $this->fpSrce = $fs; }
    public function HasSourcePath() : bool { return !is_null($this->fpSrce); }
    public function GetSourcePath() : string {
        if ($this->HasSourcePath()) {
            $fp = $this->fpSrce;
        } else {
            $fp = '';
        }
        return $fp;
    }
}
trait tOpts_HasTargetPath {
    private $fpTarg = NULL;
    protected function SetTargetPath(string $fs) { $this->fpTarg = $fs; }
    public function HasTargetPath() : bool { return !is_null($this->fpTarg); }
    public function GetTargetPath() : string { return $this->fpTarg; }
}
trait tOpts_HasSource_Mountable {
    use tOpts_HasSourcePath { tOpts_HasSourcePath::GetSourcePath as GetSourcePath_req; }

    // OVERRIDE tOpts_HasSourcePath
    public function GetSourcePath() : string { return $this->SourceMount()->GetLocalSpec(); }

    // ++ OBJECTS ++ //

    private $oMountSrce = NULL;
    protected function SourceMount() : caFolderAnywhere {
        if (is_null($this->oMountSrce)) {
            $fpBase = $this->GetSourcePath_req();
            #echo "FPBASE: [$fpBase]\n";
            $oMount = caFolderAnywhere::MakeObject($fpBase);
            #$oMount->SetUseSudo($this->Options()->UseSudo());
            $oMount->Open();
            #$fpA = $oMountA->GetLocalSpec();
            $this->oMountSrce = $oMount;
        }
        return $this->oMountSrce;
    }

    // -- OBJECTS -- //
}
// NOTE: 2022-10-24 This is just tOpts_HasSource_Mountable with "target/targ" changed to "source/srce". Surely there's a way to generalize?
trait tOpts_HasTarget_Mountable {
    use tOpts_HasTargetPath { tOpts_HasTargetPath::GetTargetPath as GetTargetPath_req; }

    // OVERRIDE tOpts_HasTargetPath
    public function GetTargetPath() : string { return $this->TargetMount()->GetLocalSpec(); }

    // ++ OBJECTS ++ //

    private $oMountTarg = NULL;
    protected function TargetMount() : caFolderAnywhere {
        if (is_null($this->oMountTarg)) {
            $fpBase = $this->GetTargetPath_req();
            #echo "FPBASE: [$fpBase]\n";
            $oMount = caFolderAnywhere::MakeObject($fpBase);
            #$oMount->SetUseSudo($this->Options()->UseSudo());
            $oMount->Open();
            #$fpA = $oMountA->GetLocalSpec();
            $this->oMountTarg = $oMount;
        }
        return $this->oMountTarg;
    }

    // -- OBJECTS -- //
}
