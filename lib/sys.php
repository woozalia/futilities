<?php namespace woozalia\futil;
/*
  PURPOSE: system information class --
    anything system-environment-related that doesn't fit cleanly into another file
  FUTURE:
    This should eventually be a replaceable wrapper that can handle things differently,
      if needed, in different environments. (Does Windows even do mounts? Maybe this
      functionality is already *ix-specific... but I won't know how to design a
      fully-portable class system until I know how other environments handle this stuff.)
  HISTORY:
    2023-01-01 started just so I'd have a wrapper for "where should I mount stuff"
      Temporarily abandoned work since FF does seem to be basically working and
      there are more urgent concerns (starting with db backups) to focus on.
      POSSIBLE BUG: FF should report matching folders as well as files; don't know if it does.
*/
class csSysFiles {

    // RETURNS: path where the current user should create mounts, if that's a thing the user wants to do
    static public function UserMountPath() : string {
    }
}
