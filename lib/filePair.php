<?php namespace woozalia\futil;
/*
  HISTORY:
    2017-12-21 extracting general-purpose classes from ensure_subset.php
    2022-09-09 copied from old FileFerret repo; tidying and updating
*/

class cFileCompareStatus {
    private $n,$dx;	// total size, bytes of progress

    public function __construct($n,$dx) {
        $this->n = $n;
        $this->dx = $dx;
    }
    public function GetBytesLeft() { return $this->dx; }
    public function GetByteSize() { return $this->n; }
}

class cFilePair extends cFSNodePair {

    // ++ SETUP ++ //

    private $oOpts, $oLog;

    /*----
      TODO: 2022-10-26 Document why $nCount and $nIndex need to be passed here. Are they ever not zero?
    */
    public function __construct(caPathPiece $oA,caPathPiece $oB,$nCount,$nIndex, cOpts $oOpts) {
        if (($nCount + $nIndex) > 0) { throw new \exception('2022-10-26 Okay, how *does* this happen, anyway?'); }
        parent::__construct($oA,$oB);
        $this->SetIndices($nCount,$nIndex);
        $this->oOpts = $oOpts;
        $this->oLog = $oOpts->LogFile();
    }

    protected function LogFile() : cLogMove { return $this->oLog; }
    protected function Options() : cOpts { return $this->oOpts; }
    
    private $nCount,$nIndex;
    protected function SetIndices($nCount,$nIndex) {
        $this->nCount = $nCount;
        $this->nIndex = $nIndex;
    }
    protected function GetIndexString() { return $this->nIndex.'/'.$this->nCount; }
    protected function IsLastOne() { return ($this->nIndex == $this->nCount); }
    
    // -- SETUP -- //
    // ++ FEEDBACK ++ //

    public function UpdateCopyStatus(cPathCompareStatus $oPath,$nBytesLeft) {
        #$sStat = $oPath->GetProgressText();
        #$s = "$sStat: $nBytesLeft bytes to copy";
        $s = "$nBytesLeft bytes to copy";
        #csTTY::ShowLine($s);
        cApp::Me()->Screen()->ShowFileStatus($s);
    }
    public function UpdateFileRead(cPathCompareStatus $osPath, cFileCompareStatus $osFile) {
        $nSize = $osFile->GetByteSize();

        $dx = $osFile->GetBytesLeft();
        #$sStat = $osPath->GetProgressText();
        if ($dx > 0) {
            #$s = "$sStat: $dx to read";
            $s = "$dx to read";
        } else {
            #$s = "$sStat: $nSize bytes ok";
            $s = "$nSize bytes ok";
        }

        cApp::Me()->Screen()->ShowFileStatus($s);
        #} else {
            // clear the always-display flag here since it won't get cleared by UpdateStatus()
        #    $this->GetAlwaysShowNext();
        #}
    }

    // -- FEEDBACK -- //
    // ++ OBJECTS ++ //
    
    private $oFileA=NULL;
    public function GetFileA() {
        if (is_null($this->oFileA)) {
            $this->oFileA = $this->GetPathA()->GetFileNode();
        }
        return $this->oFileA;
    }
    private $oFileB=NULL;
    public function GetFileB() {
        if (is_null($this->oFileB)) {
            $this->oFileB = $this->GetPathB()->GetFileNode();
        }
        return $this->oFileB;
    }
    
    // -- OBJECTS -- //
    // ++ ACTION ++ //
    
    /*----
      ACTION: 
        1. Check that there is a file at $urlB
        2. Make sure it is the same as the file at $urlA
        3. If either condition not met, inform the user.
      FUTURE: Maybe this should be called EnsureB()
      HISTORY:
        2022-10-27 Updating for HF.
          KF_DO_COPY ->
    */
    public function Ensure(cPathCompareStatus $oStat) {
        $ofA = $this->GetFileA();
        $ofB = $this->GetFileB();
        $ofiA = $this->GetPathA()->Info();
        $ofiB = $this->GetPathB()->Info();
    //	if ($ofB->Exists() && !is_link($urlB)) {	// for now, not dealing with links
        $urlA = $ofiA->Raw();
            
        // first, make sure neither is a special file
        $isFileA = $ofiA->IsCopyable();
        $isFileB = $ofiB->IsCopyable();

        /* 2022-10-27 debugging
        echo "\n"
          ."A: ".$ofiA->Raw()."\n"
          ."B: ".$ofiB->Raw()."\n"
          .'A is copyable: ['.$ofiA->IsCopyable()."]\n"
          .'B is copyable: ['.$ofiB->IsCopyable()."]\n"
          ;
        */

        if ($isFileA) {
            $canCopy = FALSE;
            if ($isFileB) {
                // same-name file exists and is actual file, so now let's see if they're the same
                
                // obvious check: are they the same size?
                $nSizeA = $ofiA->GetSize();
                $nSizeB = $ofiB->GetSize();
                if ($nSizeA == $nSizeB) {
                    $oOpts = $this->Options();
                    if ($oOpts->DoCompFound()) {
                        $this->ShowCompare($oStat,$nSizeA);
                    }
                } elseif ($nSizeB == 0) {
                    // if target is empty, go ahead and assume it's ok to overwrite
                    $canCopy = TRUE;
                } else {
                    $s = "FILE $urlA DIFFERENT SIZE - A=$nSizeA , B=$nSizeB";
                    #csTTY::KeepLine($s);
                    // TODO: This was originally supposed to scroll, but that won't work now; need a new UX paradigm for that. Split-screen? Smol window?
                    cApp::Me()->Screen()->ShowIssue($s);
                    $oLog = $this->LogFile();
                    $oLog->RecordConflict($this);
                }
            } else {
                $canCopy = TRUE;
            }

            if ($canCopy) {
                // NOTE: This can also happen if A is a file and B is a link or pipe
                //$oApp->ShowMessage("NO FILE: $urlB");
                $this->LogFile()->RecordMissing($this); // log that file is missing from B

                $doCopy = !$this->Options()->DoInfoOnly();
                if ($doCopy) {
                    if ($ofiA->IsPipe()) {
                        $s = "skipping pipe $urlA";
                        #$oApp->ShowMessage($s);
                        cApp::Me()->Screen()->ShowIssue($s);
                    } else {
                        $ok = $this->CopyAToB($oStat);	// copy to B, verify
                    // TODO: original note indicated that this might delete from A, but actual fx() does not afaict.
                    // TODO: should we be doing something with $ok?
                    }
                }
            }
        } else {
            if ($isFileB) {
                $sDescr = 'local file is not';
            } else {
                $sDescr = 'neither local nor remote is';
            }
            $s = "FILE $urlA: ".$sDescr.' a copyable file';
            #$oApp->ShowMessage($s);
            cApp::Me()->Screen()->ShowIssue($s);
            if (KF_DO_DEL_LINKS && $ofA->GetIsLink()) {
                $ofA->DeleteVisibly();
            }
        }
    }
    /*----
      ACTION: compare the contents of the two files, byte for byte
      ASSUMES: files are same size
    */
    protected function ShowCompare(cPathCompareStatus $oPathStat, $nSize) : bool {
        $oLog = $this->LogFile();

        $ofA = $this->GetFileA();
        $ofB = $this->GetFileB();
        $urlA = $ofA->GetSpec();
        $urlB = $ofB->GetSpec();

        $nBytesLeft = $nSize;
        $nBlockBytes = cConfigApp::BytesToReadPerBlock();
        $ok = TRUE;
        $fpA = $ofA->OpenForRead();
        $fpB = $ofB->OpenForRead();

        while($ok && ($nBytesLeft > 0)) {
            $sFileA = fread($fpA,$nBlockBytes);
            $sFileB = fread($fpB,$nBlockBytes);
            if ($sFileA != $sFileB) {
                csTTY::KeepLine("FILE [$urlA] CONTENTS ARE DIFFERENT");
                $oLog->RecordConflict($this);
                $ok = FALSE;
            } elseif($sFileA === FALSE) {
                csTTY::KeepLine("FILE [$urlA] COULD NOT BE READ");
                $ok = FALSE;
            } else {
                $nBytesLeft -= strlen($sFileA);
                $sIdx = $this->GetIndexString();
                $oFileStat = new cFileCompareStatus($nSize,$nBytesLeft);
                if ($nBytesLeft > 0) {
                    $this->UpdateFileRead($oPathStat,$oFileStat);
                } else {
                    /* 2022-10-27 This doesn't mean anything for now. If we go back to time-interval-based updates, then it may have a use.
                    if (($nBytesLeft > $nBlockBytes) || $this->IsLastOne()) {
                        $oApp->SetAlwaysShowNext();
                    }
                    */
                    $sMsg = "$sIdx : $nSize bytes ok";
                    $this->UpdateFileRead($oPathStat,$oFileStat);
                }
            }
        }
        
        // close files
        $ofA->Shut();
        $ofB->Shut();
        
        if ($ok) {	// if file contents are equal...
            // 2017-12-24 apparently filectime cannot be modified programmatically, so no point in checking it
        
            // if file content matches, check timestamps
            $nTimeA = filemtime($urlA);
            $nTimeB = filemtime($urlB);
            if ($nTimeA != $nTimeB) {
                $this->LogFile()->RecordTimeMismatch($urlA,$nTimeA,$nTimeB,'mod');
                $ok = FALSE;
                $sTimeA = date(KS_FMT_TIMESTAMP,$nTimeA);
                $sTimeB = date(KS_FMT_TIMESTAMP,$nTimeB);
                $sMsg = "timestamp mismatch: A=$sTimeA B=$sTimeB";
                cApp::Me()->Screen()->ShowIssue($sMsg);
            }
        }
        if ($ok && $this->Options()->DoDelete()) {
            // A and B are identical, and we're deleting identical files from A, so:
            $this->DeleteA();
        }
        return $ok;
    }
    protected function CopyAToB(cPathCompareStatus $oPathStat) : bool {
        $oLog = $this->LogFile();

        $ofA = $this->GetFileA();
        $ofB = $this->GetFileB();
        $ofiA = $this->GetPathA()->Info();
        $ofiB = $this->GetPathB()->Info();

        $nSize = $ofiA->GetSize();

        $urlA = $ofiA->Raw();
        $urlB = $ofiB->Raw();
        
        $nBytesLeft = $nSize;
        $ok = TRUE;
        //$fpA = fopen($urlA,'r');
        //$fpB = fopen($urlB,'w');	// B may exist as a pipe or link; this will overwrite
        
        $fpA = $ofA->OpenForRead();
        $fpB = $ofB->OpenForWrite();
        $oLog->RecordCopyAttempt();
        $nBlockBytes = cConfigApp::BytesToReadPerBlock();
        while($ok && ($nBytesLeft > 0)) {
            // TODO 2024-04-19 need to check for error in this line, and log the file which couldn't be read
            $sFileA = fread($fpA,$nBlockBytes);
            $nCopied = fwrite($fpB,$sFileA);
            if ($nCopied === FALSE) {
                $ok = FALSE;
                //$sMsg = "FILE $urlA - ERROR copying ($nBytesLeft to go)";
                $oLog->UpdateCopyingError($oPathStat,$nBytesLeft);
                //$oApp->UpdateStatusAlways($sMsg);
                $oLog->RecordCopyFailure($urlA,$urlB);
            } else {
                $nBytesLeft -= strlen($sFileA);
                $this->UpdateCopyStatus($oPathStat,$nBytesLeft);
            }
        }
        
        // if there were no errors copying:
        if ($ok) {
            // set the timestamp on B:
            $nTime = filemtime($urlA);
            touch($urlB,$nTime);
            
            // verify same content, to make sure:
            $this->ShowCompare($oPathStat,$nSize);
        }
        return $ok;
    }
    protected function DeleteA() : bool {
        $ok = $this->GetFileA()->DeleteVisibly();
        return $ok;
    }
}
