<?php namespace woozalia\futil;
/*
  FILE: readout.php
  PURPOSE: structured status readout UI classes
  HISTORY:
    2022-10-27 started
*/

/* 2022-10-27 not sure this actually goes here. Lifted from filePair.php.
class cFileCompareStatus {
    private $n,$dx;	// total size, bytes of progress

    public function __construct($n,$dx) {
        $this->n = $n;
        $this->dx = $dx;
    }
    public function GetBytesLeft() { return $this->dx; }
    public function GetByteSize() { return $this->n; }
}
*/

class csFileDataReadout extends csTTY {

    static private $sAct = '';
    static protected function SetAction(string $sAct) { self::$sAct = $sAct; }
    static protected function GetAction() : string { return self::$sAct; }

    static private $sFName = '';
    static protected function SetFName(string $fp) { self::$sFName = $fp; }
    static protected function GetFName() : string { return self::$sFName; }

    // ACTION: refresh what's on the screen
    static public function Refresh() {
        $sAct = self::GetAction();
        self::ShowLine("[$sAct]
    }
}
