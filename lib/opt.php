<?php namespace woozalia\futil;
/*
  PURPOSE: classes for CLI file-utilities
  HISTORY:
    2022-07-29 split off from ff.php
    2022-09-08 moved csTTY to cio.php and renamed this from fileutil.php to opt.php
*/

/*::::
  PURPOSE: basically just a key-value pair
  HISTORY:
    2022-10-13 created for cSubOpts -- is basically the core functionality of what is currently cOpts (to be renamed)
      TODO: rename this to cOpt, and rename cOpt to something more specific
    2022-10-16 renamed from cOptBase to cOptPair
*/
class cOptPair {
    public function SetBoth(string $sKey, string $sVal) {
        $this->SetKey($sKey);
        $this->SetVal($sVal);
    }

    private $sKey;
    protected function SetKey(string $s) { $this->sKey = $s; }
    public function GetKey() : string { return $this->sKey; }

    private $sVal;
    protected function SetVal(string $s) { $this->sVal = $s; }
    public function GetVal() : string { return $this->sVal; }
    public function HasVal() : bool { return $this->sVal != ''; }

    // ++ DEBUGGING ++ //

    public function ShowSelf() : string { return '['.$this->GetKey().'] = ['.$this->GetVal().']'; }
}
class cProtectedTokens {

    private $sProt,$arRep;

    public function __construct(string $s, array $arReplaced) {
        $this->sProt = $s;          // string ready for replacement ops
        $this->arRep = $arReplaced; // the bits we want to protect
    }
    public function SafeString() : string { return $this->sProt; }
    public function ProtBits() : array { return $this->arRep; }

    private $dxRepl;
    public function ResetRepl() { $this->idxRepl = 0; }
    /*----
      ACTION: for each token found, replace it with the next item from $arRep
      HISTORY:
        2022-10-16 written, but not really tested
    */
    public function ReplaceTokens(string $sMain, string $sToken) : string {
        $arMain = explode($sToken,$sMain);
        $arRepl = $this->ProtBits();

        $out = '';
        foreach($arMain as $ndx => $sMainBit) {
            $out .= $sMainBit;
            if (array_key_exists($ndx,$arRepl)) {
                $sRepl = $arRepl[$ndx];
                $out .= $sRepl;
            }
        }
        #echo "RESULT: [$out]\n";
        return $out;
    }
}
/*::::
  PURPOSE: To allow a single option-value to contain multiple additional values,
    so we don't have to overpopulate the namespace with lots of related names
  FORMAT: <so name>=<so value>[:<so name>=<so value>[:...]]
  HISTORY:
    2022-10-13 started
    2022-10-20 working; added option for no args in constructor,
      to allow object to indicate that nothing is set
*/
class cSubOpts {
    public function __construct(string $s=NULL) {
        if (is_string($s)) {
            // replace quoted bits with tokens
            echo "SUBOPTS: [$s]\n";
            $oDequot = $this->ReplaceQuotes($s);
            $this->ParseData($oDequot);
            echo "ARRAY ENTRIES: ".count($this->arOpts)."\n";
        }
    }

    // ++ DATA ACCESS ++ //

    private $arOpts = [];
    public function HasAny() : bool { return count($this->arOpts) > 0; }
    protected function SetOption(string $sName, string $sVal) {
        $oOpt = new cOptPair;
        $oOpt->SetBoth($sName,$sVal);
        $this->arOpts[$sName] = $oOpt;
    }
    public function HasOption(string $sName) : bool { return array_key_exists($sName, $this->arOpts); }
    public function GetOption(string $sName) : cOptPair {
        if ($this->HasOption($sName)) {
            return $this->arOpts[$sName];
        } else {
            throw new \exception("CALLER ERROR: No value found for option [$sName].");
        }
    }
    public function GetOptionNz(string $sName, $sDefault='') : string {
        if ($this->HasOption($sName)) {
            return $this->GetOption($sName)->GetVal();
        } else {
            return $sDefault;
        }
    }

    // -- DATA ACCESS -- //
    // ++ INTERNAL FIGURING ++ //

    const KS_QUOTE_TOKEN = "\tQ";

    protected function ReplaceQuotes(string $s) : cProtectedTokens {
        $isQuo = TRUE;
        $inQuo = FALSE;
        $sOut = '';
        $dxQuo = 0;
        $arQuotes = [];
        while ($isQuo) {
            $dxPrev = $dxQuo;
            $dxQuo = strpos($s,'"',$dxQuo); // search for next quote, from $dxQuo forward
            if (FALSE === $dxQuo) {
                $isQuo = FALSE;
                $sOut .= substr($s,$dxPrev);    // append remainder of input string
            } else {
                if ($inQuo) {
                    // we've found the matching quote-mark
                    $dxStop = $dxQuo;
                    $sQuoted = substr($s,$dxQuo,$dxStop-$dxQuo);
                    $dxQuo++;   // start next search *after* the found quotemark
                    // use place-holder for quoted bit
                    $sOut .= self::KS_QUOTE_TOKEN;
                    // stash quoted bit in an array
                    $arQuotes[] = $sQuoted;
                } else {
                    // non-quoted -- add verbatim to output
                    $dxStart = $dxQuo;
                    $sOut .= substr($s,$dxPrev,$dxQuo-$dxPrev);
                }
                $inQuo = !$inQuo;
            }
        }
        $oSafe = new cProtectedTokens($sOut,$arQuotes);
        return $oSafe;
    }
    /*----
      ACTION: break up the string into key-value pairs and store internally
    */
    protected function ParseData(cProtectedTokens $oProt) {
        $sSafe = $oProt->SafeString();
        // split the string into "key=value" pieces
        #echo "SSAFE=[$sSafe]\n";
        $arTerms = explode(':',$sSafe);
        $oProt->ResetRepl();
        foreach ($arTerms as $dx => $sTerm) {
            $sPost = $oProt->ReplaceTokens($sTerm,self::KS_QUOTE_TOKEN);
            // we now have a term with any original quoted pieces restored
            if (strpos($sTerm,'=') === FALSE) {
                // there's no value
                $sKey = $sTerm;
                $sVal = '';
            } else {
                // split key=value into key and value
                [$sKey,$sVal] = explode('=',$sTerm);    // don't know if this will work when there's no '='
            }
            $this->SetOption($sKey,$sVal);
        }
    }
    // -- INTERNAL FIGURING -- //
}
/*::::
  ADDED: key-value pair is parsed from a single string
    This defines the standard format for primary command options.
  HISTORY:
    2022-08-01 created
    2022-10-16 renamed from cOpt to cOptMain
*/
class cOptMain extends cOptPair {
    public function __construct(string $sArg) {
        $npMark = strcspn($sArg,':=');
        if ($npMark > 0) {
            // get just the option's UI key
            $sKey = substr($sArg,0,$npMark);
            // get the value (if any)
            $sVal = substr($sArg,$npMark+1);
            // store both parts locally
            // NOTE that this is the key used in the UI, not the app's internal key
            $this->SetKey($sKey);
            $this->SetVal($sVal);
        } else {
            throw new \exception('2022-08-01 This should never happen. Internal error.');
        }
    }
    
    public function GetOpts() : cSubOpts {
        $sc = $this->GetSubClass();
        return new $sc($this->GetVal());
    }

    private $sc = cSubOpts::class;
    public function SetSubClass(string $sc) { $this->sc = $sc; }
    protected function GetSubClass() : string { return $this->sc; }
}
/*::::
  PURPOSE: base class for parsing and managing command-line options
    I originally tried to use getopt(), but the way it does stuff just adds confusion
      and removes flexibility.
  DOCS: https://wooz.dev/Futilities/human/lib/caOptsBase
  TODO: split this up into a base class with no app options defined
    and a descendent "core" class with universal standard options
  HISTORY:
    2022-07-29 created
*/
abstract class caOptsBase {
    
    // ++ SETUP ++ //

    const ksOPT_CONFIG_FILE     = 'conf.file'; // name of config file to use
    const ksOPT_SHOW_PROGRESS   = 'show.prg';   // show progress (without filling up screen)

    public function __construct(array $arCmdArgs) {
        $this->SetupOptions();
        $this->ParseCommand($arCmdArgs);

        // do any app-specific setup
        $this->ProcessUserInput();
    }
    protected function SetupOptions() {
        $this->SetupBaseOptions();
        $this->SetupAppOptions();
    }
    protected function SetupBaseOptions() {
        $this->AddAppOptions([
          self::ksOPT_CONFIG_FILE   => ['-cf','--config-file'],
          self::ksOPT_SHOW_PROGRESS => ['-sp','--show-progress'],
          ]);
    }
    abstract protected function SetupAppOptions();

    protected function ParseCommand(array $arCmdArgs) {
        // remove first argument (path to self) from command args list
        $arCmdArgs = array_reverse($arCmdArgs);
        $fsSelf = array_pop($arCmdArgs);
        $this->SetSelfName($fsSelf);
        $arCmdArgs = array_reverse($arCmdArgs);

        // parse command line for app options and other terms
        foreach ($arCmdArgs as $sText) {
            $oo = new cOptMain($sText);
            if ($this->HasAppOption($oo->GetKey())) {
                // if it matches an app option, handle it that way
                $this->AddCmdOption($oo);
            } else {
                // otherwise, handle it like a loose argument (term)
                $this->AddCmdTerm($sText);
            }
        }
        // for debugging
        #echo 'Input terms: '.print_r($arTerms,TRUE);
    }

    protected function ProcessUserInput() {

        // process base options
        if ($this->HasCmdOption(self::ksOPT_CONFIG_FILE)) {
            // a specific conf file is requested
            $oOpt = $this->GetCmdOption(self::ksOPT_CONFIG_FILE);
            $fnConf = $oOpt->GetVal();
        } else {
            // use default conf file
            $fnConf = static::GetDefaultConfig();
        }
        // include the conf file; access settings via cConfig static fx().
        require_once($fnConf);

        // process unnamed terms
        $arTerms = $this->GetCmdTerms();
        foreach ($arTerms as $idx => $sTerm) {
            $this->HandleTerm($idx,$sTerm);
        }
        $nTFnd = count($arTerms);
        $nTReq = $this->MinQtyTerms();
        if ($nTFnd < $nTReq) {
            echo
                'INPUT ERROR: '.$this->RenderTooFewTerms($nTFnd)."\n"
                .'USAGE: '.$this->RenderAppUsage()."\n"
                .$this->RenderCommonHelp()
                ;
            die();
        }
    }
        
    protected function RenderCommonHelp() : string {
        $urlDocs = cApp::GetDocsURL();
        return
            "DOCS: $urlDocs\n"
            .$this->ShowAppOptions()
            ."\n"
            ;
    }
    abstract protected function HandleTerm($idx,$sTerm);
    protected function HandleUnknownTerm($idx,$sTerm) {
        echo "Unexpected term: [$sTerm]\n";
    }
    abstract public function DescribeSelf() : string;
        
    // -- SETUP -- //
    // ++ CONFIG ++ //

    // name of default configuration file (might vary by app, but conf.php is a good default-default)
    static private $fnConfDef = 'conf.php';
    static public function SetDefaultConfig(string $fn) { self::$fnConfDef = $fn; }
    static protected function GetDefaultConfig() : string { return self::$fnConfDef; }

    // RETURNS: the minimum number of terms (non-option arguments) needed for usefulness
    abstract protected function MinQtyTerms() : int;
    // RETURNS: string showing basic command structure for the app
    abstract protected function RenderAppUsage() : string ;
    // RETURNS: string describing what is missing
    abstract protected function RenderTooFewTerms(int $qTerms) : string;
    
    // ++ COMMAND: MISC ++ //
    
    private $fnSelf;
    protected function SetSelfName(string $fn) { $this->fnSelf = $fn; }
    protected function GetSelfName() : string { return $this->fnSelf; }
    
    // ++ COMMAND: APP OPTIONS ++ //
     
    // APP OPTIONS are the options that the application will recognize in a command
    // See https://wooz.dev/Futilities/human/lib/caOptsBase

    private $arAppOpts = [];
    protected function GetAppOptions() : array { return $this->arAppOpts; }
    protected function AddAppOption(string $sKey, string $sText) {
        $this->arAppOpts[strtolower($sText)] = $sKey;
    }
    // $ar is [code name => [array of CLI strings which indicate that option]]
    protected function AddAppOptions(array $ar) {
        foreach ($ar as $sKey => $arText) {
            foreach ($arText as $sText) {
                $this->AddAppOption($sKey,$sText);
            }
        }
    }
    /*----
      PURPOSE: checks to see if the option-index includes the given option
      INPUT:
        $sText = full option parameter, possibly including value (demarcated by ':' or '=')
      HISTORY:
        2022-08-01 added check for value
    */
    protected function HasAppOption(string $sText) : bool {
        if ($sText != '') {
            // check for value demarcator (':' or '=')
            $npMark = strcspn($sText,':=');
            if ($npMark > 0) {
                // get just the option name
                $sOptName = substr($sText,0,$npMark);
                $isFnd = array_key_exists(strtolower($sOptName),$this->arAppOpts);
                return $isFnd;
            }
            throw new \exception("2022-09-30 shouldn't we be checking \$sText here?");
        }
        return FALSE;
    }
    protected function GetAppOptionKey(string $sText) : string { return $this->arAppOpts[$sText]; }

    private $arSub = [];
    public function GetSubOpts(string $sKey,string $sc) : cSubOpts {
        if (array_key_exists($sKey,$this->arSub)) {
            $oSub = $this->arSub[$sKey];
        } else {
            $oSub = NULL;  // if this remains NULL, then option is not set
            if ($this->HasCmdOption($sKey)) {
                $oOpt = $this->GetCmdOption($sKey);    // get formatted suboption string
                $oSub = new $sc($oOpt->GetVal());      // $oSub parses that and wraps the results
            }
            if (is_null($oSub)) {
                $oSub = new $sc; // blank object, nothing set
            }
            $this->arSub[$sKey] = $oSub;
        }
        return $oSub;
    }

    /*----
      HISTORY:
        2022-10-20 Improving this a bit so it groups all option aliases together
          TODO: make it possible for each option to have a brief description as well
    */
    protected function ListAppOptions(string $sPfx = '  ', bool $isDebug = FALSE) : ?string {
        $arExt = $this->GetAppOptions();    // array of external codes
        $out = NULL;
        ksort($arExt);
        // build array of internal codes
        foreach ($arExt as $sExt => $sText) {
            #$out .= "$sPfx($sText) $sKey\n";
            $arInt[$sText][] = $sExt;
        }
        foreach ($arInt as $sInt => $arExt) {
            $sIntShow = ($isDebug ? " ($sInt):" : '');
            $out .= $sPfx.$sIntShow;
            foreach ($arExt as $sExt) {
                $out .= ' '.$sExt;
            }
            $out .= "\n";
        }
        return $out;
    }
    protected function ShowAppOptions() : string {
        $sOpt = $this->ListAppOptions();
        if (is_null($sOpt)) {
            $out = "(no options available)\n";
        } else {
            $out = "Options: \n".$this->ListAppOptions();
        }
        return $out;
    }

    // ++ COMMAND: CMD OPTIONS ++ //

    // CMD OPTIONS are the options actually specified in the current command
    // See https://wooz.dev/Futilities/human/lib/caOptsBase

    private $arCmdOpts = [];
    protected function AddCmdOption(cOptMain $oo) {
        $sKey = $this->GetAppOptionKey($oo->GetKey());
        $this->arCmdOpts[$sKey] = $oo;
    }
    protected function GetCmdOptions() : array { return $this->arCmdOpts; }
    protected function HasCmdOption(string $sText) : bool {
        return array_key_exists($sText,$this->arCmdOpts,);
    }
    protected function GetCmdOption(string $sText) : cOptMain {
        if (array_key_exists($sText,$this->arCmdOpts)) {
            return $this->arCmdOpts[$sText];
        } else {
            echo "INTERNAL ERROR: option key [$sText] is not found.\nAvailable options:\n"
            .$this->ListAppOptions('  ',TRUE)
            ."Options set in command:\n"
            .$this->ListCmdOptions()
            ;
            throw new \exception('Probably an error in the Futilities code.');
        }
    }
    /*----
      HISTORY:
        2022-10-20 created
    */
    public function ListCmdOptions(string $sPfx = '  ') : ?string {
        $arExt = $this->GetCmdOptions();    // array of external codes
        $out = NULL;
        foreach ($arExt as $sExt => $oOpt) {
            $out .= "$sPfx$sExt: ".$oOpt->GetVal()."\n";
        }
        return $out;
    }

    // ++ COMMAND: TERMS ++ //
    
    private $arTerms = [];
    protected function AddCmdTerm(string $sText) {
        $this->arTerms[] = $sText;
    }
    protected function GetCmdTerms() : array { return $this->arTerms; }
    
    // 2022-08-05 Should probably be renamed to "HasTerms()", OSLT, for generalization
    public function HasSearchArgs() : bool { return count($this->GetCmdTerms()) > 0; }
    
    // -- COMMAND -- //
    // ++ UI: FEEDBACK ++ //

    /*----
      NOTE: This sort of includes DescribeSelf(), but gives the information
        in more technical terms -- largely for debugging purposes
    */
    public function DescribeRequest() : string  {
        $arOpt = $this->GetCmdOptions();
        $arTrm = $this->GetCmdTerms();
        
        $out = "++ INPUT DESCRIPTION ++\n";
        $nOpt = count($arOpt);
        if ($nOpt > 0) {
            $sPlur = (($nOpt == 1) ? '' : 's');
            $out .= "Option$sPlur set:\n";
            foreach ($arOpt as $sKey => $oOpt) {
                $sOpt = $oOpt->ShowSelf();
                $out .= " - [$sKey]: $sOpt\n";
            }
        } else {
            $out .= "No input options set.\n";
        }
        
        $nTrm = count($arTrm);
        if ($nTrm > 0) {
            $sPlur = (($nTrm == 1) ? '' : 's');
            $out .= "Term$sPlur:\n";
            foreach ($arTrm as $nIdx => $sVal) {
                $out .= " - [$sVal]\n";
            }
        } else {
            $out .= "No input terms given.\n";
        }
        $out .= "-- INPUT DESCRIPTION --\n";
        return $out;
    }
    
    // -- UI -- //
    // ++ OPTIONS DEFINED ++ //

    public function ShowProgress()  : bool {
        // if we're listing anything, that fights with the progress-shower
        return ($this->HasCmdOption(self::ksOPT_SHOW_PROGRESS) and !$this->ListingOptions()->UseScreen());
    }

    // -- OPTIONS DEFINED -- //
}
