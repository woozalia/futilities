<?php namespace woozalia\futil;
/*
  PURPOSE: classes for file I/O
  NOTE:
    2022-10-07 need to document the relationship between this and cFile in file.php
  HISTORY:
    2022-09-03 split off from fileutil.php
*/
abstract class caFile {

    // ++ SETUP ++ //

    public function __construct(string $fs) { $this->Open($fs); }
    public function __destruct() { $this->Shut(); }
    
    // -- SETUP -- //
    // ++ CONFIG ++ //
    
    abstract protected function FileMode() : string;
    
    // -- CONFIG -- //
    // ++ ACTION ++ //
    
    protected $r;
    protected function Open(string $fs) {
        $sMode = $this->FileMode();
        $r = fopen($fs,$sMode);
        if ($r === FALSE) {
            echo "ERROR: Could not open file '$fs'.\n";
            die();
        }
        $this->r = $r;
    }
    protected function Shut() { fclose($this->r); }
    
    public function Restart() { fseek($this->r,0); }
    
    // -- ACTION -- //
    
}
class cLog extends caFile {

    // ++ CONFIG ++ //
    
    protected function FileMode() : string { return 'a'; }
    
    // -- CONFIG -- //
    // ++ OUTPUT ++ //

    public function Write(string $s) { fwrite($this->r,$s); }
    public function WriteLn(string $s) { fwrite($this->r,$s."\n"); }

    public function WriteHeader() {
            $sDescr = cApp::Me()->Options()->DescribeSelf();
            $sTime = date('Y/m/d H:i:s');

            $this->WriteLn(cApp::GetVersionString());
            $this->Write($sTime.' '.$sDescr);         // $sDescr ends with a CRLF
    }

    // ++ OUTPUT: conditional: progress ++ //

    public function LogSourceFolder(iPathNamed $ofSrce) {} // DEFAULT: do nothing
    public function LogSourceFile(iPathNamed $ofSrce) {}   // DEFAULT: do nothing

    // -- OUTPUT -- //
}
class cReadResult {
    private $ok, $s;
    
    public function __construct(bool $ok, string $s) { $this->ok = $ok; $this->s = $s;}
    
    public function IsOkay() : bool { return $this->ok; }
    public function Text() : string { return $this->s; }
}
class cInputFile extends caFile {
    // ++ CONFIG ++ //
    
    protected function FileMode() : string { return 'r'; }
    
    // -- CONFIG -- //
    // ++ INPUT ++ //
    
    public function ReadLn() : cReadResult {
        $res = fgets($this->r);
        $isOk = is_string($res);
        if ($isOk) {
            // strip off ending CR or LF
            $res = rtrim($res,"\n");
        }
        return new cReadResult($isOk,($isOk?$res:''));
    }
    
    // -- INPUT -- //
}
class cInputTextDataFile extends cInputFile {

    private $sLineComment = '#';

    // ACTION: Reads next uncommented line
    public function ReadDataLine() : cReadResult {
        $isDone = FALSE;
        while (!$isDone) {
            $or = $this->ReadLn();
            if ($or->IsOkay()) {
                $s = $or->Text();
                $s = $this->RemoveComment($s);
                $isDone = ($s != '');
            } else {
                $isDone = TRUE; // EOF
            }
        }
        return $or;
    }
    protected function RemoveComment(string $s) : string {
        $npRmk = strpos($s,$this->sLineComment);  // search for comment mark
        if (is_int($npRmk)) {
            $s = substr($s,0,$npRmk); // get string up to comment mark
            $s = rtrim($s); // trim any whitespace from end
        }
        return $s;
    }
}
