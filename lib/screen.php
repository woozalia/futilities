<?php namespace woozalia\futil;
/*
  PURPOSE: full-screen text classes
  HISTORY:
    2022-11-03 started; basic screen positioning was working several days ago
      decided these should be completely separate from csTTY, and also not static
      split Screen classes out of cio.php
*/

/*::::
  PURPOSE: abstract wrapper for TTY with whole-screen control (typically via ESC codes)
  HISTORY:
    2022-10-29 started
*/
abstract class caScreen {
    abstract public function EraseScreenAll();
    abstract public function EraseScreenToTop();
    abstract public function EraseScreenToEnd();

    abstract public function EraseLineAll();
    abstract public function EraseLineLeft();
    abstract public function EraseLineRight();

    abstract public function SetCursor(int $colX, int $rowY);
    abstract public function MoveCursorLeft(int $n=1);
    abstract public function MoveCursorRight(int $n=1);
    abstract public function MoveCursorUp(int $n=1);
    abstract public function MoveCursorDown(int $n=1);

    // ++ FIGURING: RELATIVE ++ //

    private $rx=1;  // horizontal position relative to start of line (start=1)
    private $ry=0;  // vertical position relative to starting location (start=0)

    const CR = "\r";    // production
    #const CR = '[CR]';  // debugging

    public function SetCursorLeft() {
        echo self::CR;
        $this->rx = 1;
    }

    // ACTION: make $n lines of vertical space for relative cursor ops at the bottom of the screen
    public function AllocateLines(int $n) {
        echo str_repeat("\n",$n);
        $this->rx = 1;
        $this->ry = $n;
    }
    protected function MoveToRelLine(int $n) {
        $nDiff = $n - $this->ry;  // # of lines the cursor needs to move
        if ($nDiff < 0) {
            // move up
          #echo "\n===\nMOVING UP - diff=$nDiff\n===\n";
            $this->MoveCursorUp(-$nDiff);
        } elseif ($nDiff > 0) {
          #echo "\n===\nMOVING DOWN - diff=$nDiff\n===\n";
            $this->MoveCursorDown($nDiff);
        }
        $this->ry = $n;
    }
    public function ShowOnRelLine(int $n, string $s) {
        $this->MoveToRelLine($n);
        $this->SetCursorLeft();
        echo $s;
        $this->EraseLineRight();
    }
}
/*::::
  PURPOSE: experimental af
  CODES (for easy reference; see https://en.wikipedia.org/wiki/ANSI_escape_code#Description)
    <ESC>[<n>A - cursor up <n> lines
    <ESC>[<n>B - cursor down <n> lines
    <ESC>[<n>C - cursor forward (right) <n> spaces
    <ESC>[<n>D - cursor back (left) <n> spaces
    <ESC>[<n>;<m>H - position cursor at row <n>, column <m>
    <ESC>[<n>J - erase part/all of display (3 possible modes)
    <ESC>[<n>K - erase part/all of line (3 possible modes)
  HISTORY:
    2022-10-29 started
*/
class cScreenANSI extends caScreen {

    const ESC = "\e";     // for production
    #const ESC = "[ESC]";  // for debugging

    protected function CtrlSeq(string $sCode) : string { return self::ESC . '[' . $sCode; }
    protected function EraseScreenPart(int $sCode) : string { return $this->CtrlSeq($sCode.'J'); }
    protected function EraseLinePart(int $sCode) : string { return $this->CtrlSeq($sCode.'K'); }

    public function EraseScreenAll() { echo $this->EraseScreenPart(2); }
    public function EraseScreenToTop() { echo $this->EraseScreenPart(1); }
    public function EraseScreenToEnd() { echo $this->EraseScreenPart(0); }

    public function EraseLineAll()   { echo $this->EraseLinePart(2); }
    public function EraseLineLeft() { echo $this->EraseLinePart(1); }
    public function EraseLineRight() { echo $this->EraseLinePart(0); }

    public function SetCursor(int $colX, int $rowY) { echo $this->CtrlSeq($rowY.';'.$colX.'H'); }
    public function MoveCursorLeft(int $n=1)  { echo $this->CtrlSeq($n.'D'); }
    public function MoveCursorRight(int $n=1) { echo $this->CtrlSeq($n.'C'); }
    public function MoveCursorUp(int $n=1)    { echo $this->CtrlSeq($n.'A'); }
    public function MoveCursorDown(int $n=1)  { echo $this->CtrlSeq($n.'B'); }

}
/*::::
  PURPOSE: fakes a scrolling window area for showing a short log
  HISTORY:
    2022-12-31 started
*/
class cScreenLog {
}
