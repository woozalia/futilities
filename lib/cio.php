<?php namespace woozalia\futil;
/*
  PURPOSE: console I/O classes
  HISTORY:
    2022-09-08 split off from fileutil.php
*/

/*::::
  PURPOSE: static class for output to TTY
*/
class csTTY {

    // ++ CONFIG ++ //
    
    // later: somehow get this via ANSI?
    /*
    static private $nWidth = 80;
    static public function SetWidth(int $n) { self::$nWidth = $n; }
    static protected function GetWidth() : int { return self::$nWidth; }
    */
    static protected function GetWidth() : int { return cConfig::ScreenWidth(); }
    
    // -- CONFIG -- //
    // ++ ACTION ++ //

    static public function Open() {
        $h = fopen("php://stdin","r");
        self::SetKbdHandle($h);
    }
    static public function Shut() { fclose(self::GetKbdHandle()); }
    
    // -- ACTION -- //
    // ++ INTERNALS ++ //
    
    static private $hKbd;
    static protected function SetKbdHandle($h) { self::$hKbd = $h; }
    static protected function GetKbdHandle() { return self::$hKbd; }
    
    static public function GetChar() : string { return fgetc(self::GetKbdHandle()); }
    
    // -- INTERNALS -- //
    // ++ OUTPUT ++ //
    
    static private $sLastLen = 0;
    static private $doKeepLine = FALSE;
    static public function ShowLine(string $sLine) {
        if (self::$doKeepLine) {
            echo "\n";  // preserve currently displayed line
            self::$doKeepLine = FALSE;  // reset the flag
        }
        $sShow = self::MakeLineFit($sLine);
        self::EraseLine();
        echo $sShow;
        self::$sLastLen = strlen($sShow);
    }
    static public function Append(string $s) {
        echo $s;
        self::$sLastLen += strlen($s);
    }
    static public function NewLine() {
        self::$sLastLen = 0;
        echo "\n";
    }
    static public function KeepLine(string $s) {
        echo "\n$s";
        self::$doKeepLine = TRUE;
    }
    static public function EraseLine() {
        echo
          "\r"    // return to beginning of line
          .str_repeat(' ',self::$sLastLen)
          ."\r"   // return to beginning of line
          ;
    }
    
    // ++ OUTPUT: PROCESSING ++ //
    
    static protected function MakeLineFit(string $sLine) : string {
        // config
        $nMax = static::GetWidth();
        $sBreak = ' ... ';
        
        // calculations
        $nLen = strlen($sLine);
        $nBrk = strlen($sBreak);
        if (($nLen - $nBrk) > $nMax) {
            $nHalf = ($nMax - $nBrk) / 2;
            $sLeft = substr($sLine,0,$nHalf);
            $sRight = substr($sLine,-$nHalf);
            $sOut = $sLeft . $sBreak . $sRight;
            #echo "HALF=$nHalf LEFT=[$sLeft] RIGHT=[$sRight]\nLINE=[$sOut]";
            #self::GetChar();
        } else {
            $sOut = $sLine;
        }
        return $sOut;
    }
    
    // -- OUTPUT -- //
}
