#!/usr/bin/php
<?php
/*
  USAGE: invoke on command line like this:
    go.php dispatch_filespec
  HISTORY:
    2021-06-13 started
*/
#$fsLog = '/home/woozle/Nextcloud/statler/woozle/AppDev/PHP/web-dispatch/action.log';  // debugging kluge
$fsLog = __DIR__.'/godispatch.log';
$out = 
    'Started: '.date('Y/m/d H:i:s')."\n"
    ."Logging to $fsLog\n"
    ;
if (array_key_exists(1,$argv)) {
    $sReq = $argv[1];
    $urlReq = urldecode($sReq);
    #$urlReq = $sReq;  // for CLI testing only

    $out .= "ARG: $sReq\nURL: $urlReq\n\n";

    $n = strpos($urlReq,'wooz://');
    if ($n === FALSE) {
        $out .= "USAGE ERROR: Request was not 'wooz://' protocol.\nREQUEST: $urlReq";
    } elseif ($n > 0) {
        $out .= 'USAGE ERROR: Request string must begin with "wooz://".';
    } else {
        $jsReq = substr($urlReq,7);  // 7 = length of protocol prefix

        $arReq = json_decode($jsReq,TRUE);  // TRUE: return as array

        $out .= "JSON: $jsReq\nDATA: ".print_r($arReq,TRUE);
        
        if (array_key_exists('do',$arReq)) {
            $sAct = $arReq['do'];
            switch($sAct) {
              case 'edit':
                $urlHost = $arReq['host'];
                $fsEdit = $arReq['fs'];
                $urlFull = $urlHost.$fsEdit;
                
                if (array_key_exists('line',$arReq)) {
                    $nLine = $arReq['line'];
                    $sCmdOpt = " --line $nLine";
                } else {
                    $sCmdOpt = '';
                }
                if (array_key_exists('project',$arReq)) {
                    $sProj = $arReq['project'];
                } else {
                    $sProj = 'debug'; // default project name
                }
                
                $sCmd = "kate --start '$sProj'$sCmdOpt $urlFull";
                $out .= 'COMMAND: '.$sCmd."\n";
                
                $ok = exec($sCmd,$arRtn);
                $out .= 'SUCCESS? '.($ok?'yes':'no')."\n";
                if (count($arRtn) > 0) {
                    $out .= 'OUTPUT:'.print_r($arRtn,TRUE);
                } else {
                    $out .= "NO OUTPUT\n";
                }
                break;
              default:
                $out .= "UNKNOWN ACTION [$sAct] requested.\n";
            }
        } else {
            $out .= "DATA ERROR: 'do' not found in array.\n";
        }
    }
} else {
    $out .= "DATA ERROR: no input request found.\n";
}
/*
if (file_exists($fsReq)) {
    $jsReq = file_get_contents($fsReq);
    if (is_null($arReq)) {
        $out = "DATA ERROR: The data in [$fsReq] was not readable as JSON.";
    } else {
        $out = 'RESULT: '.print_r($arReq,TRUE);
    }
} else {
    $out = "USAGE ERROR: File [$fsReq] not found.\n";
}
*/
echo $out."\n";
file_put_contents($fsLog,$out);
