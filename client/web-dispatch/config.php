<?php
/*
  PURPOSE: configuration for go.php
  HISTORY:
    2023-04-04 created
*/

class csConfig {
    static public function LogFileSpec() : string { return __DIR__.'/go-dispatch.log'; }
    static public function DefaultProject(string $sProto) : string {
        switch ($sProto) {
          case 'wooz':      return 'debug';
          case 'phpstorm':  return 'local: OSW';
          default:          return 'debug';
        }
    }

    // 2023-04-04 For now, this is just "replace anywhere"; later, maybe regex.
    static public function PathMaps() : array {
        return [
          '/home/vagrant/OSWZeus' => '/home/woozle/Documents/dev/OSW/osw-zeus/',
          ];
    }
}
