#!/usr/bin/php
<?php
/*
  USAGE: invoke on command line like this:
    go-storm.php URL
  NOTES:
    TYPICAL URL: phpstorm://open?file=%2Fhome%2Fvagrant%2Fpath%2Ffile.php&line=37
    When testing from CLI, keep in mind that bash will strip out "&" -- so put quotes around the whole URL when testing that way.
      Firefox runs the command in such a way that this stripping does not happen.
  HISTORY:
    2023-04-04 Adapting from go.php to work with "phpstorm:" URLs
*/

require 'config.php';

class csLog {
    static protected function FileSpec() : string { return csConfig::LogFileSpec(); }

    static private $sOut = '';
    static public function AddLine(string $s) {
        $sWrite = $s."\n";
        self::$sOut .= $sWrite;
        echo $sWrite;
    }
    static public function DoHeader() {
        echo "\n";
        self::AddLine('Started: '.date('Y/m/d H:i:s'));
        $fsLog = self::FileSpec();
        self::AddLine("Logging to $fsLog\n");
    }

    static public function WriteOut() {
        $out = self::$sOut;
        #echo $out."\n";
        $fsLog = self::FileSpec();
        file_put_contents($fsLog,$out);
    }
}

class cEditorKate {
    public function __construct(string $uriFile, int $nLine, string $sProj) {
        $sCmdOpt = " --line $nLine";
        $sCmd = "kate --start '$sProj'$sCmdOpt $uriFile";
        csLog::AddLine('COMMAND: '.$sCmd);

        $this->SetCmd($sCmd);
    }

    private $sCmd;
    protected function SetCmd(string $sCmd) { $this->sCmd = $sCmd; }
    protected function GetCmd() : string { return $this->sCmd; }

    public function Load() {
        $sCmd = $this->GetCmd();
        $ok = exec($sCmd,$arRtn);
        csLog::AddLine('SUCCESS? '.($ok?'yes':'no'));
        if (count($arRtn) > 0) {
            csLog::AddLine('OUTPUT:'.print_r($arRtn,TRUE));
        } else {
            csLog::AddLine("NO OUTPUT");
        }
    }
}

if (array_key_exists(1,$argv)) {
    $sProto = 'phpstorm';

    csLog::DoHeader();

    csLog::AddLine("ARGS: ".print_r($argv,TRUE));

    $sReq = $argv[1];
    $urlReq = urldecode($sReq);
    csLog::AddLine("ARG: $sReq\nURL: $urlReq\n");

    $arURL = parse_url($urlReq);
    csLog::AddLine('URL ARRAY: '.print_r($arURL,TRUE));

    $sScheme = $arURL['scheme'];
    // 2023-04-04 For now, we'll assume the command ("host") is always "open", since I don't know what else it can be.
    $sQuery = $arURL['query'];
    // "query" has 2 parts, the filepsec and the line number (which may be optional)
    // Typical: file=/home/vagrant/path/file.php&line=37

    parse_str($sQuery,$arQry);
    #$out .= 'QUERY ARRAY: '.print_r($arQry,TRUE);
    $fsReq = $arQry['file'];
    $nLine = $arQry['line'];

    csLog::AddLine("REQUEST:\n - FILE: $fsReq\n - LINE: $nLine");

    $arMaps = csConfig::PathMaps();
    $fsReal = str_replace(array_keys($arMaps),array_values($arMaps),$fsReq);

    csLog::AddLine("ACTUAL FILE: $fsReal");
    /*
    foreach ($arMaps as $fsRaw => $fsReal) {
        $dxMap = strpos($fsReq,$fsRaw);
        if ($dxMap !== FALSE) {
            $dlRaw = strlen($fsRaw);    // length of thing to be replaced
            $dlReal = strlen($fsReal);  // length of replacement
            $sBef = str
        }
    }*/

    $sProj = csConfig::DefaultProject($sProto);

    $oEdit = new cEditorKate($fsReal,$nLine,$sProj);
    $oEdit->Load();
} else {
    csLog::AddLine("DATA ERROR: no input request found.");
}

csLog::WriteOut();
#echo $out."\n";
#file_put_contents($fsLog,$out);
