<?php namespace woozalia\futil\init;
/**
 * PURPOSE: simplified Ferreteria classloader ("woozpfx" instar)
 * HISTORY:
 * * 2024-04-29 adapting Ferreteria class-loader to work with futils. Maybe eventually this can be integrated better.
 */

use Exception;  // PHP native class
use ParseError;
use Throwable;

class cNspaceMatch {

    static public function FromNSpace(string $ns) {
        $oThis = new self;
        $oThis->_NSpace($ns);
        return $oThis;
    }
    static public function FromNotFound() { return new self; }

    private $nsFound = NULL;
    protected function _NSpace(string $ns) { $this->nsFound = $ns; }
    public function NSpace() : string { return $this->nsFound; }
    public function HasNSpace() : bool { return is_string($this->nsFound); }
}

class cNspaceAndClass {
    public const KC_NSP_SEP = '\\';  // namespace-separator

    // ++ SETUP ++ //

    public function __construct(private string $sNSpace, private string $sClass) {}
    public function NSpace() : string { return $this->sNSpace; }
    public function CName() : string { return $this->sClass; }

    static public function FromFullName(string $scFull) : self {
        $dxFinal = strrpos($scFull,self::KC_NSP_SEP);
        $ns = substr($scFull,0,$dxFinal); // namespace only
        $sc = substr($scFull,$dxFinal+1);   // end class only
        #echo "NSPACE=[$ns] CLASS=[$sc]\n";
        return new self($ns,$sc);
    }

    // -- SETUP -- //
    // ++ STATE ++ //

    public function FullName() : string { return $this->NSpace().self::KC_NSP_SEP.$this->CName(); }

    // -- STATE -- //
}

/**
 * NOTE: When namespaces are added to the list, a '\' is appended in order to prevent sub-element matches --
 *  e.g. we don't want Woozalia\Ferret to match Woozalia\FerretX.
 */
class csClassLoader {

    // ++ CONFIG ++ //

    // Why doesn't PHP have built-in constants for these?
    private const KC_PATH_SEP = '/';  // path-separator (Windows will probably want a backslash here.)

    static private $arNSP = [];
    static protected function NSpacePaths() : array { return self::$arNSP; }
    static public function SetNSpacePath(string $ns, string $fp) {
        // DEBUGGING
        #echo "Namespace [$ns] folder is now [$fp].\n";

        $sNSSfxd = $ns.cNspaceAndClass::KC_NSP_SEP;
        self::$arNSP[$sNSSfxd] = $fp;
    }
    static public function GetNSpacePath(string $ns) : string { return self::$arNSP[$ns]; }
    static public function HasNSPaths() : bool { return count(self::$arNSP) > 0; }

    // -- CONFIG -- //
    // ++ SETUP ++ //

    /*----
      PURPOSE: configures the PHP autoloader to use this class
    */
    static public function Init() : void {
        spl_autoload_register(__CLASS__ . '::_TryClassoidLoad');
        #echo 'Autoloader registered: '.get_called_class().'<br>';
    }

    // -- SETUP -- //
    // ++ CALLBACK ++ //

    /**----
      CALLBACK via spl_autoload_register()
      HISTORY:
        2023-08-20 started; will refine as needed
        2023-11-23 moved the "no paths" check from Init() to here
        2023-12-08 renamed _LoadClassoid() -> _TryClassoidLoad()
          so I can make a separate function for "we know this is a
          loadable classoid, so just load it with no extra checking"
    */
    static public function _TryClassoidLoad(string $scName) {
        if ($scName != 'self') { // "self" can be ignored because it means the invoking class, which is therefore already loaded.
            if (!static::HasNSPaths()) {
                echo "CONFIG ERROR: You need to SetNSpacePath() for at least the Ferreteria library.\n";
            }
            self::LoadRegularClassoid($scName);
        }
    }

    // -- CALLBACK -- //
    // ++ STATE ++ //

    static private $oNamer;
    static protected function SetFullClassName(string $scFull) {
        self::$oNamer = cNspaceAndClass::FromFullName($scFull);
        $fsSource = self::ClassNameToFileSpec();  // absolute spec for classoid-file
        self::SetFileSpec($fsSource);
    }
    static protected function GetClassoidNamer() : cNspaceAndClass { return self::$oNamer; }

    static private $fsCls;
    static protected function SetFileSpec(string $fs) { self::$fsCls = $fs; }
    static protected function GetFileSpec() : string  { return self::$fsCls; }

    // -- STATE -- //
    // ++ ACTION: API ++ //

    static public function LoadRegularClassoid(string $scName) {
        self::SetFullClassName($scName);
        $fsSource = self::GetFileSpec();
        #echo "LOADING classoid [$scName] from $fsClass<br>";
        if (file_exists($fsSource)) {
            // DEBUGGING
            #echo "LOADING classoid $scName FROM $fsSource\n";
            require_once $fsSource;
            self::VerifyClassoidCreated($scName,$fsSource);
        } else {
            $ftFile = csScreen::BoldIt($fsSource);
            $CRLF = csScreen::NewLine();
            echo "ERROR for classoid $scName :".$CRLF
              ." - File $ftFile not found.".$CRLF
              ;
        }
    }

    // -- ACTION: internal -- //

    static protected function VerifyClassoidCreated() {
        $oClsInfo = self::GetClassoidNamer();
        $scFull = $oClsInfo->FullName();
        $scFinal = $oClsInfo->CName();
        $chType = $scFinal[0]; // wooz convention: first character indicates which type of classoid
        switch($chType) {
          case 'c':
          case 'e': // enums count as classes, for some reason
            $sType = 'class';
            $ok = class_exists($scFull,FALSE);
            break;
          case 'i':
            $sType = 'interface';
            $ok = interface_exists($scFull,FALSE);
            break;
          case 't':
            $sType = 'trait';
            $ok = trait_exists($scFull,FALSE);
            break;
          default;
            $sType = 'unknown classoid';
            $ok = FALSE;
        }
        if (!$ok) {
            $fsSource = self::GetFileSpec();
            echo " - ERROR: $sType $scFull did not load!\n";
            self::LintTheFile($fsSource);
        }
    }
    static protected function LintTheFile(string $fsCode) {
        $fsCodeCmd = escapeshellarg($fsCode);
        $sCmd = "php -l $fsCodeCmd 2>&1";
        $CRLF = csScreen::NewLine();
        echo "Trying to execute [$sCmd]...".$CRLF;
        #$sRes = shell_exec($sCmd);
        $sLast = exec($sCmd,$arRtn);
        if (is_string($sLast)) {
            $sRes = csScreen::OpenList();
            foreach ($arRtn as $sLine) {
                $sRes .= csScreen::ListItem($sLine);
            }
            $sRes .= csScreen::ShutList();
            echo "Lint results:$sRes";
        } else {
            echo "Could not lint the file. :-/".$CRLF;
        }
    }
    /**
     * PURPOSE: for code that might get called *during* a class-lookup (generally, diagnostic code
     *  for when the class won't load) to directly load classes so as not to trigger another error
     */
    static public function EnsureRegularClassoid(string $sName) {
        if (!self::IsRegularClassoid($sName)) {
            self::LoadRegularClassoid($sName);
        }
    }

    // -- ACTION -- //
    // ++ FIGURING ++ //

    /**
     * ACTION: checks to see if $sName is an already-loaded classoid
     * NOTE: class_exists() will return TRUE for enums, so no need to check enum_exists()
     */
    static protected function IsRegularClassoid(string $sName) : bool {
        return class_exists($sName,FALSE) || trait_exists($sName,FALSE) || interface_exists($sName,FALSE);
    }

    /**
     * ACTION: translates a full classname (with namespace) into a filespec
     * INPUT: $scFull = full classoid name. Must include the actual classoid as the final segment.
     */
    static protected function ClassNameToFileSpec() : string {
        $oNSC = self::GetClassoidNamer();
        $nsFind = $oNSC->NSpace();
        $scFind = $oNSC->CName();
        $oMatch = static::NamespaceMatch($nsFind);
        if (!$oMatch->HasNSpace()) {
            $ar = static::NSpacePaths();
            $nns = count($ar);
            $sPlur = ($nns==1) ? '' : 's';

            $CRLF = csScreen::NewLine();

            $sDiags = "Registered namespace$sPlur:$CRLF";
            foreach ($ar as $ns => $fp) {
                $sDiags .= " - NS:[$ns] => PATH:[$fp]$CRLF";
            }

            $htNS = ($nsFind == '') ? 'no namespace' : "NS:[$nsFind]";
            #throw new Exception("Namespace-path for class [$scFull] ($htNS) is not known.");

            $sError = "Classoid [$scFull] has Unknown namespace [$nsFind].";
            throw new Exception('ERROR: '.$sError.$sDiags);
        }

        $nsBase = $oMatch->NSpace();      // get the matching namespace
        $fpBase = static::GetNSpacePath($nsBase); // get the folder for that namespace
        // get the portion of the classname that is relative to the found namespace
        $nsRelative = substr($nsFind,strlen($nsBase));
        // replace namespace-delimiters with filepath delimiters
        #echo "FULL CLASSOID:[$scFull] BASE NS:[$nsBase] REL NS:[$nsRelative]\n";
        $fpRelative = str_replace(cNspaceAndClass::KC_NSP_SEP,self::KC_PATH_SEP,$nsRelative);

        $fnClass = static::NormalizeClassName($oNSC->CName());
        $fpRelMarked = ($fpRelative=='') ? '' : ($fpRelative.self::KC_PATH_SEP);  // if rel path is not empty, add a path-sep after it
        #echo "BASE PATH:[$fpBase] REL PATH:[$fpRelMarked] FILE: [$fnClass]\n";
        $fsOut = $fpBase . self::KC_PATH_SEP . $fpRelMarked . $fnClass . '.php';

        return $fsOut;
    }
    /**
     * ACTION: Determine if the given namespace/classname ($csFull) is within a known namespace folder.
     *  Return a NspaceMatch object indicating the search result.
     * HISTORY:
     *  2023-09-23 We can't just use the first match, because there might be a sub-namespace
     *    that matches more exactly.
     */
    static public function NamespaceMatch(string $csFull) : cNspaceMatch {
        $ar = static::NSpacePaths();
        $csFullSfxd = $csFull.cNspaceAndClass::KC_NSP_SEP;
        $nlMatchBest = 0; // length of match
        $oMatch = NULL;
        foreach ($ar as $nsBase => $fpBase) {
            // if $csFull starts with the path to this repo...
            if (str_starts_with($csFullSfxd,$nsBase)) {
                $nlMatchThis = strlen($nsBase);
                #echo "NSBASE=[$nsBase] LEN=[$nlMatchThis]\n";
                if ($nlMatchThis > $nlMatchBest) {
                    $nlMatchBest = $nlMatchThis;
                    $oMatch = cNspaceMatch::FromNSpace($nsBase);
                }
            }
        }
        if (is_null($oMatch)) {
            $oMatch = cNspaceMatch::FromNotFound();  // indicate lack-of-match
        }
        return $oMatch; // return what we have (or haven't) found
    }
    /**
     * ACTION: strips off the lowercase prefix and returns only the main part of the class name
     * INPUT: de-namespaced class name
     */
    static protected function NormalizeClassName(string $sName) : string {
        // find the first uppercase character
        $dxLen = strlen($sName);
        $isFnd = FALSE;
        for($dxCur=0; (($dxCur<$dxLen) && !$isFnd); $dxCur++) {
            $ch = substr($sName,$dxCur,1); // get current character
            if (ctype_upper($ch)) {
                $isFnd = TRUE;
            } elseif (ctype_digit($ch)) {
                // class name can be numeric after the first character
                $isFnd = TRUE;
            }
        }
        if ($isFnd) {
            // get the remainder of the class name, including the initial capital character:
            $scNorm = substr($sName,$dxCur-1);
            return $scNorm;
        } else {
            throw new Exception("Class name [$sName] has no non-prefix characters; cannot autoload.");
        }
    }

    // -- FIGURING -- //
    // ++ DIAGS ++ //

    static protected function GotToHere(string $fs, int $nLine, string $sXtra='') {
        echo "<span style='color:blue; font-weight:bold;'>Got to</span> <b>$fs</b> line <b>$nLine</b> $sXtra<br>";
    }

    // -- DIAGS -- //
}
