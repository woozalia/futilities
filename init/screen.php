<?php namespace woozalia\futil\init;

use Woozalia\Ferret\IO\O\Screen\iEnv;
use Woozalia\Ferret\IO\O\Screen\Layout\caTable;
use Woozalia\Ferret\IO\O\Screen\TTY\cTable;

/**
  PURPOSE: environment class variant for CLI
    based on csText (which is used by csEnv)
    but renamed to csScreen, and with superflous methods removed
    In the Futilities context, this is a type of Screen class for linear output with markup.
    TODO: should be an abstract class here, with TTY.php in a Markup subfolder. (Eventually, we'll have a UIML version as well, hopefully...)
*/
class csScreen {

    // text formatting

    static public function BoldIt(?string $s) : string { return $s; } // TODO: change color or something
    static public function ItalIt(?string $s) : string { return $s; } // TODO: change color or something

    static public function NewLine() : string { return "\n"; }
    // 2021-08-23 Written but not tested.
    static public function TitleIt(string $s) : string {
        $nLen = strlen($s);
        $sLine = '+'.str_repeat('-',$nLen+2).'+';
        $CRLF = self::NewLine();
        return $CRLF.$sLine.$CRLF."| $s |".$CRLF.$sLine.$CRLF;
    }
    // TODO: colors in terminal emulator
    static public function ErrorIt(string $s) : string { return $s; }
    // 2021-09-24 This will need improving. Not tested.
    static public function BoxIt(string $s) : string { return $s; }

    // block formatting

    static public function NewTable() : caTable { return new cTable; }
    static public function OpenPage(string $sTitle) : string {
        if (strlen($sTitle) > 0) {
            $soTitle = " [$sTitle] ";
        } else {
            $soTitle = '';
        }
        $nTitle = strlen($soTitle);
        $nBar = 60 - $nTitle;
        $nHalf = $nBar/2;
        $nHalf1 = floor($nHalf);
        $nHalf2 = ceil($nHalf);
        $sOut =
          str_repeat('=',$nHalf1)
          .$soTitle
          .str_repeat('=',$nHalf2)
          ."\n"
          ;
        return $sOut;
    }
    static public function ShutPage() : string { return "\n".str_repeat('=',60)."\n"; }

    // There really isn't any CLI equivalent, but maybe we can pretty this up more later.
    static public function HideDrop(string $sSummary, string $sContent) : string {
        return "\n==== [+] $sSummary ====\n$sContent\n==== [-] $sSummary ====\n";
    }

    // bulk text formatting

    // crude indentation
    static public function MakeList(string $s) : string { return str_replace("\n","\n  ",$s); }

    // graphic elements

    static public function HorizLine() : string { return str_repeat('-',40); }

    // hierarchy

    static private int $ind = 0;
    static protected function IndentIt() : string { return str_repeat(' -',self::$ind); }
    static public function OpenList() : string { self::OpenIndent(); return "\n"; }
    static public function ShutList() : string { self::ShutIndent(); return "\n"; }
    static public function OpenIndent() : string { self::$ind++; return ''; }
    static public function ShutIndent() : string { self::$ind--; return ''; }
    static public function ListItem(string $s) : string { return self::IndentIt().' '.$s.self::NewLine(); }

    // characters

    static public function LeftArrow() : string { return '<-'; }
    static public function RightArrow() : string { return '->'; }

}
