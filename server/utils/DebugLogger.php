<?php namespace debug;
#echo 'GOT TO '.__FILE__.' line '.__LINE__.'<br>';

/*
if (class_exists(cLogger::class,FALSE)) {
    throw new exception('How??');
}
*/
/*
  USAGE: 
    1. (optional) call \debug\cLogger::SetFileName()
    2. Use \debug\cLogger::Me()->Write[Ln]() to add to the logfile
*/
class cLogger {
    static private $me = NULL;
    static public function Me() : cLogger {
        if (is_null(self::$me)) {
            $sClass = get_called_class();
            self::$me = new $sClass;
        }
        return self::$me;
    }
    
    static private $isActive = TRUE;
    static public function SetActive(bool $b) { self::$isActive = $b; }
    static protected function GetActive() : bool { return self::$isActive; }
    
    static private $fn = 'debug'; // default base name for log files
    static public function SetFileName(string $fn) { self::$fn = $fn; }
    
    static private $fp = __DIR__.'/debug';     // default folder for logs
    static public function SetFolder(string $fp) { self::$fp = $fp; }
    
    static protected function MakeFileSpec() : string {
        $sStamp = date('Y-m-d-H.i.s');
        return self::$fp.'/'.$sStamp.'.'.self::$fn.'.log';
    }
    
    static private $fs = NULL;
    static protected function GetFileSpec() : string {
        if (is_null(self::$fs)) {
            self::$fs = self::MakeFileSpec();
        }
        return self::$fs;
    }
        
    static public function Write(string $s) {
        if (self::GetActive()) {
            $fs = self::GetFileSpec();
            file_put_contents($fs,$s,FILE_APPEND);
        }
    }
    static public function WriteLn(string $s) { self::Write($s."\n"); }
}
