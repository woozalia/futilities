<?php

// for logging/naming
$sProjName = 'TootCat';

// folder for journalled backups
#$fpDest = '/mnt/links/backups/tootcat';
$fpDest = '/root/backups/tootcat'; 
// filespec for constant-name backup
$fsMainBkup = "/var/backups/postgres.$sProjName.sql.gz";
// spec for log file
$fsLog = $fpDest.'/db-bak.log';

// db creds(ish)
$sDBName = 'mastodon';
$sDBUser = 'mastodon';

$nJrnQty = 3; // number of journalled backups

$doReal = TRUE;  // TRUE = actually do the backup
#$doReal = FALSE;  // FALSE = debugging: create temp file to use as test for rotation
