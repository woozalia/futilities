<?php
/*
  PURPOSE: Crude attempt to make backup scripts just a little bit more manageable
    *without* writing a full-blown db-based backup app.
  HISTORY:
    2016-09-07 started
    2017-10-17 mkdir() should recursively create target folders when needed
    2018-04-15 moved backup list into separate file, for easier management
    2019-03-18 modifying to use Apache-like configuration
*/

require('serverlib.php');

// for debugging
$fErrLevel = E_ALL | E_STRICT;
error_reporting($fErrLevel);

define('KFP_CONFIG',__DIR__.'/config/actual');
define('KFP_SOURCES',KFP_CONFIG.'/defs/enabled');
require(KFP_CONFIG.'/main.php');

$o = new cBackupSession();
$o->Go();

class cBackupSession {
    public function Go() {
        $this->OpenLog();

        // Log start of backup session
        $this->LogLine("\nSESSION START ".date(KS_FMT_TIMESTAMP).' - copying to '.KFP_BACKUP_DEST);

        // Iterate through SOURCES folder to get things to backup:
        $fp = KFP_SOURCES;
        $poDir = dir($fp);
        while (FALSE !== ($fn = $poDir->read())) {
            if (($fn != '.') && ($fn != '..')) {
                $fs = $fp.'/'.$fn;
                $this->ProcessSource($fs,$fn);
            }
        }
        
        // Log end of backup session
        $this->LogLine("SESSION FINISH ".date(KS_FMT_TIMESTAMP));
    }

    // ++ LOGGING ++ //
    
    private $rLog;
    protected function OpenLog() {
        $fsLog = KFP_BACKUP_LOGS.'/BackupFerret-'.date('Y').'wk'.date('W').'.log';
        $this->rLog = fopen($fsLog,'a');	// open log file for appending
        if (!is_resource($this->rLog)) {
            echo "ERROR: could not open log file $fsLog.";
            die();
        }
    }
    protected function LogLine($s) { fwrite($this->rLog,$s."\n"); }
    protected function LogText($s) { fwrite($this->rLog,$s); }
    
    // -- LOGGING -- //
    // ++ PROCESS ++ //

    function ProcessSource($fs,$fn) {
        require($fs);	// run the source file (it's PHP - output to $DEF)
        $oSource = new ffcBackupSource(
          $DEF['dest'],
          $DEF['alias'],
          $DEF['suffix'],
          $DEF['folders']
          );

        $sHostDomain = $oSource->DomainString();
        $fpHostDest = $oSource->DestinationPath();
        $arPaths = $oSource->FolderArray();
        echo "@$sHostDomain - ";
        $this->LogText("\t$fn\t$sHostDomain ".date('H:i:s').' ');
        if (file_exists ($fpHostDest)) {
            if (is_writable($fpHostDest)) {
                echo 'destination exists';
            } else {
                echo "ERROR: Can't write to folder $fpHostDest\n";
                die();
            }
        } else {
            mkdir($fpHostDest);
            echo 'destination created';
        }
        echo "\n";
        foreach ($arPaths as $fpFolder) {
            $fpFolderDest = $fpHostDest.'/'.$fpFolder;
            echo "\t/$fpFolderDest - ";
            if (file_exists($fpFolderDest)) {
                if (is_writable($fpFolderDest)) {
                    echo 'destination exists';
                } else {
                    echo "ERROR: can't write to folder $fpFolderDest";
                    die();
                }
            } else {
                mkdir($fpFolderDest, 0777 , TRUE);	// TRUE: create recursively if needed
                echo 'destination created';
            }
            echo "\n";

            $this->LogText("($fpFolder) ");	// indicate progress in log file
            echo "--FOLDER BACKUP BEGINS--\n";
            $sCmd = "rsync -Pav root@$sHostDomain:/$fpFolder/* $fpFolderDest/";
            echo "COMMAND: $sCmd\n";
            system($sCmd);
            echo "--FOLDER BACKUP ENDS--\n";
        }
        $this->LogText(date('H:i:s')."\n");
    }

    // -- PROCESS -- //
}







