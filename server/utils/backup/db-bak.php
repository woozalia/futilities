#!/usr/bin/php
<?php

require 'db-bak.conf.php';

$sTime = date('Y-m-d h:i:s');

// rotate backup files

class cFolderRotater {
    public function __construct(string $fpBase, int $nJrnMax) {
        $this->fpBase = $fpBase;
        $this->nJrnMax = $nJrnMax;
    }
    
    private $fpBase, $nJrnMax;

    public function FolderForNumber(int $nJrn) : string {
        $fpBase = $this->fpBase;
        return "$fpBase/$nJrn";
    }
    public function RotateBackups(int $nJrn) {
        $nJrnNext = $nJrn + 1;
        $fpDest = $this->fpBase;
        $fpJrnThis = $this->FolderForNumber($nJrn);
        $fpJrnNext = "$fpDest/$nJrnNext";
        
        if (file_exists($fpJrnThis)) {
            // get a list of files in the folder...
            echo "Checking [$fpJrnThis]:\n";
            $arScan = scandir($fpJrnThis);
            $arFiles = [];
            foreach ($arScan as $fn) {
                $fs = $fpJrnThis.'/'.$fn;
                echo "ENTRY [$fs]: ".(is_file($fs) ? 'file' : 'NOT file')."\n";
                if (is_file($fs)) {
                    $arFiles[] = $fs;
                }
            }
            // if there *are* any files...
            if (count($arFiles) > 0) {
                // rotate the "next" folder first, if we're doing that
                $nJrnMax = $this->nJrnMax;
                if ($nJrn < $nJrnMax) {
                    // move backup files into next folder
                    if (!file_exists($fpJrnNext)) {
                        // make sure next folder exists
                        echo "Making folder $fpJrnNext\n";
                        mkdir($fpJrnNext,0700);
                    }
                    // rotate the next folder forward
                    $this->RotateBackups($nJrn + 1);
                    // rotate *this* folder
                    foreach ($arFiles as $fs) {
                        $sCmd = "mv '$fs' '$fpJrnNext/'";
                        echo $sCmd;
                        $res = exec($sCmd);
                        if ($res !== FALSE) {
                            echo " - ok!";
                        } else {
                            echo " - ERROR";
                        }
                        echo "\n";
                    }
                } else {
                    // this is the last folder -- delete contents instead of rotating
                    foreach ($arFiles as $fs) {
                        echo "DELETING $fs\n";
                        unlink($fs);
                    }
                }
            }
        } else {
            // no folder yet, so nothing to move; just create the folder
            mkdir($fpJrnThis,0700);
            echo "Created [$fpJrnThis].\n";
        }
    }
}

$oBkups = new cFolderRotater($fpDest,$nJrnQty);
$oBkups->RotateBackups(0);
$fpThis = $oBkups->FolderForNumber(0);
if ($doReal) {
    $fnOut = "$sTime.$sProjName.sql.gz";
} else {
    $fnOut = "$sTime.$sProjName.test.tmp";
}
$fsOut = $fpThis.'/'.$fnOut;

// feedback for manual usage
echo "Backing up $sProjName data to $fsOut.\n=====\n";

if ($doReal) {
    #$sCmd = "sudo -u '$sDBUser' pg_dump --format=c --verbose --oids --compress=9 > '$fsOut'";
    $sCmd = "sudo -u '$sDBUser' pg_dump --format=c --oids --compress=9 > '$fsOut'";
} else {
    $sCmd = "touch '$fsOut'";
}

// open the log file

$rLog = fopen($fsLog,'a');
if ($rLog === FALSE) {
    die(" -- ERROR: could not open log file [$fsLog].\n");
}

// log the project name and starting time
fwrite($rLog, "$sProjName: ".date('Y-m-d h:i:s'));


// invoke the backup process
echo "BACKUP COMMAND: $sCmd\n";
$okBak = passthru($sCmd,$nResult);
if ($okBak === FALSE) {
    $sRes = 'Error code '.$nResult;
} else {
    $sRes = '(ok)';
    
    // copy successful backup to back-uppable folder
    $sCmdCopy = "cp '$fsOut' '$fsMainBkup'";
    passthru($sCmdCopy,$nResult);
}

// DONE -- tidy up the backup

fwrite($rLog, ' - '.date('Y-m-d h:i:s')."\n");
fclose($rLog);
