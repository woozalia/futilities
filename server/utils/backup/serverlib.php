<?php
/*
  PURPOSE: server management library
  HISTORY:
    2017-10-21 extracted from backup.php because it looks like it will be useful for migration too
*/
class ffcBackupSource {

    // ++ SETUP ++ //

    public function __construct($fpDest,$sNickname,$sSuffix,array $arFolders) {
        $this->DestinationFolder($fpDest);
        $this->NickString($sNickname);
        $this->SuffixString($sSuffix);
        $this->FolderArray($arFolders);
    }

    private $fpDest;
    protected function DestinationFolder($fp=NULL) : string {
        if (!is_null($fp)) {
            $this->fpDest = $fp;
        }
        return $this->fpDest;
    }
    private $sNick;
    public function NickString($s=NULL) : string {
        if (!is_null($s)) {
            $this->sNick = $s;
        }
        return $this->sNick;
    }

    private $sSfx;
    public function SuffixString($s=NULL) : string {
        if (!is_null($s)) {
            $this->sSfx = $s;
        }
        return $this->sSfx;
    }
    private $arFldrs;
    public function FolderArray(array $ar=NULL) : array {
        if (!is_null($ar)) {
            $this->arFldrs = $ar;
        }
        return $this->arFldrs;
    }

    // -- SETUP -- //
    // ++ CALCULATIONS ++ //

    public function DomainString() : string { return $this->NickString().$this->SuffixString(); }
    public function DestinationPath() : string { return $this->DestinationFolder().'/'.$this->NickString(); }

    // -- CALCULATIONS -- //
}
