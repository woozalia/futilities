<?php
/*
  PURPOSE: to automate server migration tasks as much as possible
  HISTORY;
    2017-10-21 started
    2017-10-25 basic sequencing is working
*/
$kfpConfigFerreteria = '/root/scripts/config/ferreteria';
require('/root/scripts/git/ferreteria/start.php');
ferreteria\classloader\cLoader::SetDebugMode(TRUE);
ferreteria\classloader\cLoader::SetBasePath(__DIR__);
new ferreteria\classloader\cLibraryExec('service.migrate','lib/@lib.php');
ferreteria\classloader\cLoader::LoadLibrary('service.migrate');


// for debugging
$fErrLevel = E_ALL | E_STRICT;
error_reporting($fErrLevel);

$fpDefs = 'local/defs-active';
$poDir = dir($fpDefs);
$ar = array();
while (FALSE !== ($fnFile = $poDir->read())) {
    if (($fnFile!='.') && ($fnFile!='..')) {
        $fs = $fpDefs.'/'.$fnFile;
        $ar[$fs] = $fnFile;
    }
}
$cnt = count($ar);
echo "Found $cnt migration def".(($cnt == 1)?'':'s')."\n";

foreach ($ar as $fs => $fn) {
    echo "++ MIGRATING [$fn]:\n";
    require($fs);
    $os->DoProjects();
    echo "-- MIGRATING [$fn] done.\n";
}
echo "END OF MIGRATIONS.\n";

