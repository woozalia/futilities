<?php

class ffcServer {
    public function __construct($sHost) { $this->SetHost($sHost); }

    private $sHost;
    protected function SetHost($s) { $this->sHost = $s; }
    public function GetHost() { return $this->sHost; }
    
    // ++ STRUCTURE ++ //

    private $arProjects = NULL;
    public function AddProject($sName) {
        $op = new ffcProject();
        $this->arProjects[$sName] = $op;
        $op->Server($this);
        return $op;
    }
    public function DoProjects() {
        foreach ($this->arProjects as $sName => $oProj) {
            echo "PROJECT: $sName\n-----\n";
            $oProj->DoActions();
            echo "-----\n";
        }
    }
    
    // -- STRUCTURE -- //
}
