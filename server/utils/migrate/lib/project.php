<?php

class ffcProject {

    // ++ SETUP ++ //

    private $oServer;
    public function Server(ffcServer $oServer=NULL) {
	if (!is_null($oServer)) {
	    $this->oServer = $oServer;
	}
	return $this->oServer;
    }
    private $sFileOwner = NULL;
    public function SetFileOwner($s) {
	$this->sFileOwner = $s;
    }
    public function GetFileOwner() {
	return $this->sFileOwner;
    }
    public function FileOwner_isSet() {
	return !is_null($this->sFileOwner);
    }

    // -- SETUP -- //
    // ++ STRUCTURE ++ //

    private $arFolders = NULL;
    public function AddFolder($sName,$fpRemote,$fpLocal) {
	$this->arFolders[$sName] = new ffcFolder($fpRemote,$fpLocal);
    }
    public function GetFolder($sName) {
	if (is_array($this->arFolders)) {
	    if (array_key_exists($sName,$this->arFolders)) {
		return $this->arFolders[$sName];
	    } else {
		echo "ERROR: folder set [$sName] not found.\n";
	    }
	} else {
	    echo "ERROR: no folder sets have been defined.\n";
	}
    }
    
    private $arActions = NULL;
    public function AddAction(ffcAction $oAction) {
	$this->arActions[] = $oAction;
	$oAction->Project($this);
    }
    public function DoActions() {
	foreach ($this->arActions as $oAction) {
	    $oAction->Go();
	}
    }
    
    // -- STRUCTURE -- //
}
