<?php

abstract class ffcAction {
    abstract public function Go();
    
    private $oProj;
    public function Project(ffcProject $oProj=NULL) {
	if (!is_null($oProj)) {
	    $this->oProj = $oProj;
	}
	return $this->oProj;
    }
}
abstract class ffcAction_FileCopy extends ffcAction {

    // ++ SETUP ++ //

    public function __construct($sFolder,$fpSub) {
	$this->CommonFolderName($sFolder);
	$this->TargetPath($fpSub);
    }
    
    // -- SETUP -- //
    // ++ VALUE ACCESS ++ //
    
    private $sFolder;
    protected function CommonFolderName($s=NULL) {
	if (!is_null($s)) {
	    $this->sFolder = $s;
	}
	return $this->sFolder;
    }
    protected function GetCommonFolder() {
	return $this->Project()->GetFolder($this->CommonFolderName());
    }
    private $fpSub;
    protected function TargetPath($fp=NULL) {
	if (!is_null($fp)) {
	    $this->fpSub = $fp;
	}
	return $this->fpSub;
    }
    
    // -- VALUE ACCESS -- //
    // ++ VALUE CALCULATION ++ //

    abstract protected function RemoteSpec();
    abstract protected function LocalSpec();

    // -- VALUE CALCULATION -- //
    // ++ EVENTS ++ //

    public function Go() {
	$oProj = $this->Project();
	$oServ = $oProj->Server();
	$sHostDomain = $oServ->GetHost();

	$oFolder = $this->GetCommonFolder();
	$fpSrce = $oFolder->GetRemotePath();
	$fpDest = $oFolder->GetLocalPath();
	
	$fsSrce = "$fpSrce/".$this->RemoteSpec();
	$fsDest = "$fpDest/".$this->LocalSpec();
	
	$sCmd = "rsync -Pav root@$sHostDomain:$fsSrce $fsDest";
	
	echo " ++ FILE COPY COMMAND: $sCmd\n";
	
	// make sure the folder for $fsDest exists
	$fpDestFolder = dirname($fsDest);
	if (file_exists($fpDestFolder)) {
	    echo " .... FOUND destination folder [$fpDestFolder]\n";
	} else {
	    echo " ++++ CREATING destination folder [$fpDestFolder]\n";
	    mkdir($fpDestFolder);
	}
	ffcApp::Execute($sCmd);
	
	echo " -- [/COMMAND]\n";
	if ($oProj->FileOwner_isSet()) {
	    $sOwner = $oProj->GetFileOwner();
	    $sCmd = "chown -R $sOwner $fsDest";
	    echo " ++ SETTING OWNER: $sCmd\n";
	    ffcApp::Execute($sCmd);
	}
    }

   // -- EVENTS -- //
}
class ffcAction_SingleFileCopy extends ffcAction_FileCopy {
    // ++ VALUE CALCULATION ++ //
    
    protected function RemoteSpec() {
	return $this->TargetPath();
    }
    protected function LocalSpec() {
	return $this->TargetPath();
    }
    
    // -- VALUE CALCULATION -- //
}
class ffcAction_FolderFileCopy extends ffcAction_FileCopy {
    // ++ VALUE CALCULATION ++ //
    
    protected function RemoteSpec() {
	return $this->TargetPath().'/';
    }
    protected function LocalSpec() {
	return $this->TargetPath().'/';
    }
    
    // -- VALUE CALCULATION -- //
}
class ffcAction_DataCopy extends ffcAction {

    // ++ SETUP ++ //
    
    public function __construct($sSchema,$sPassword) {
	$this->SetSchemas($sSchema);
	$this->SetPassword($sPassword);
    }

    private $sSchemaLocal;
    public function SetLocalSchema($s) {
	$this->sSchemaLocal = $s;
    }
    protected function GetLocalSchema() {
	return $this->sSchemaLocal;
    }
    
    private $sSchemaRemote;
    public function SetRemoteSchema($s) {
	$this->sSchemaRemote = $s;
    }
    protected function GetRemoteSchema() {
	return $this->sSchemaRemote;
    }

    protected function SetSchemas($s) {
	$this->SetRemoteSchema($s);
	$this->SetLocalSchema($s);
    }
    
    private $sPass;
    protected function SetPassword($s) {
	$this->sPass = $s;
    }
    protected function GetPassword() {
	return $this->sPass;
    }
    
    // -- SETUP -- //

    // ++ EVENTS ++ //

    public function Go() {

	$sSchemaRemote = $this->GetRemoteSchema();
	$sSchemaLocal = $this->GetLocalSchema();
	
	$sSchemaDesc = $sSchemaLocal;
	if ($sSchemaRemote != $sSchemaLocal) {
	    $sSchemaDesc .= " (from $sSchemaRemote)";
	}
	
	echo "DATA COPY: $sSchemaDesc\n";
	
	$oProj = $this->Project();
	$oServ = $oProj->Server();
	$sHostDomain = $oServ->GetHost();
	$oFolder = $this->Project()->GetFolder('TEMP');
	$fsLocal = $oFolder->GetLocalPath();
	$sPass = $this->GetPassword();
	
	// dump the file from the remote into the local temp folder:
	$sCmd = "ssh $sHostDomain \"mysqldump -uroot -p$sPass $sSchemaRemote\" > $fsLocal/$sSchemaLocal.sql";
	echo " ++ EXPORT COMMAND: $sCmd\n";
	ffcApp::Execute($sCmd);
	echo " -- [/COMMAND]\n";

	// local MySQL commands
	
	$sCmdPfx = "mysql -hlocalhost -uroot -p$sPass";
	$sqlSchemaLocal = '`'.$sSchemaLocal.'`';
	
	$sql = "DROP DATABASE IF EXISTS $sqlSchemaLocal;";
	$sqlArg = escapeshellarg($sql);
	$sCmd = "$sCmdPfx --execute=$sqlArg";
	ffcApp::Execute($sCmd);
	
	$sql = "CREATE DATABASE $sqlSchemaLocal;";
	$sqlArg = escapeshellarg($sql);
	$sCmd = "$sCmdPfx --execute=$sqlArg";
	ffcApp::Execute($sCmd);
	
	// import the local file into the local database
//	$sCmd = "$sCmdPfx -D$sSchemaLocal -v < $fsLocal/$sSchemaLocal.sql";	// "-v"? why did this ever work?
	$sCmd = "$sCmdPfx -D$sSchemaLocal < $fsLocal/$sSchemaLocal.sql";
	echo " ++ IMPORT COMMAND: $sCmd\n";
	ffcApp::Execute($sCmd);
	echo " -- [/COMMAND]\n";
    }

    // -- EVENTS -- //
}
