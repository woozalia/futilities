<?php

class ffcFolder {
    
    function __construct($fpRemote,$fpLocal) {
	$this->SetRemotePath($fpRemote);
	$this->SetLocalPath($fpLocal);
    }
    
    private $fpRemote, $fpLocal;
    protected function SetRemotePath($fp) {
	$this->fpRemote = $fp;
    }
    public function GetRemotePath() {
	return $this->fpRemote;
    }
    protected function SetLocalPath($fp) {
	$this->fpLocal = $fp;
    }
    public function GetLocalPath() {
	return $this->fpLocal;
    }
}
