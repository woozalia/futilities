<?php
/*
  PURPOSE: define locations for libraries using modloader.php
  FILE SET: server migration classes
  HISTORY:
    2017-12-28 created
	2020-10-21 updated for latest Ferreteria
*/

/*
$fp = dirname( __FILE__ );
fcCodeModule::BasePath($fp.'/');
fcCodeLibrary::BasePath($fp.'/');
*/

$oLib = $this;

$om = $oLib->MakeModule('app-migrate.php');
  $om->AddClass('ffcApp');
$om = $oLib->MakeModule('server.php');
  $om->AddClass('ffcServer');
$om = $oLib->MakeModule('project.php');
  $om->AddClass('ffcProject');
$om = $oLib->MakeModule('folder.php');
  $om->AddClass('ffcFolder');
$om = $oLib->MakeModule('action.php');
  $om->AddClass('ffcAction_SingleFileCopy');
  $om->AddClass('ffcAction_FolderFileCopy');
  $om->AddClass('ffcAction_DataCopy');
