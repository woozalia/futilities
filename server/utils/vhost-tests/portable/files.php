<?php

$fprWeb = 'public_html/';   // relative folder for web files root (WWW), aka public HTML
$fpInsRel = 'vhost-test-temp/';        // folder inside WWW for inserting test files
$fprInsRepo = 'inserts/';   // folder relative to vhost-tests/ for insert-file repository
$fpCertsBase = '/etc/letsencrypt/live/'; // base folder for SSL certificate sources

$arInsFiles = ['php' => 'info-json.php'];

