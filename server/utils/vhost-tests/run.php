<?php
/*
  HISTORY:
    2021-09-13 Now checks group ownership as well as user. No option yet for the group to be different from the user,
      but that shouldn't be difficult to add if needed.
*/

$fErrLevel = E_ALL | E_STRICT;
error_reporting($fErrLevel);
if (!ini_get('display_errors')) {
    ini_set('display_errors', 'TRUE');
}

require_once('portable/files.php');
require_once('local/vhosts.php');

ob_implicit_flush(TRUE);

foreach ($arSites as $sKey => $arSite) {
    echo "Checking $sKey:";
    
    
    $fsHome = $arSite['home'];
    
    // PART 1: file checks
    
    $sUser = $arSite['user'];
    $sGroup = $sUser;   // for now, group name is always the same as user name
    $ar = posix_getpwnam($sUser);
    $idUsr = $ar['uid'];
    #$ar = posix_getgrnam($sGroup);
    $idGrp = $ar['gid'];
    CheckFilePerms($fsHome,$idUsr,$idGrp);
    echo ' (files ok)';
    
    // PART 2: web test
    
    if (array_key_exists('lang',$arSite)) {
        $sLang = $arSite['lang'];
        $fnIns = $arInsFiles[$sLang];
        
        $fsWWW = $fsHome.$fprWeb;
        $fsInsSrce = __DIR__.'/'.$fprInsRepo.$fnIns;
        $fpInsFull = $fsWWW.$fpInsRel;
        $fsInsTarg = $fpInsFull.$fnIns;
    
        // make sure test folder exists
        if (!file_exists($fpInsFull)) {
            #echo " creating [$fpInsFull]";
            // setting permissions here does not work, for some reason (maybe 8.0-only?)
            $ok = mkdir($fpInsFull);
            if ($ok) {
                echo ' (folder ok)';
                chown($fpInsFull,$sUser);
                chgrp($fpInsFull,$sUser);
            } else {
                die("\nCould not create the testing folder $fpInsFull.\n");
            }
        }
        // even if config is wrong, we still want info -- make globally writeable:
        $ok = chmod($fpInsFull, 0777);
        if (!$ok) {
            die("\nCould not chmod the testing folder $fpInsFull.\n");
        }
        
        // folder created; write test executable:
        copy($fsInsSrce,$fsInsTarg);
        // this probably doesn't matter, but might as well:
        chown($fsInsTarg,$sUser);
        chgrp($fsInsTarg,$sUser);
        
        // now invoke the executable via web;
        $sDomain = $arSite['dom'];
        $url = "https://$sDomain/$fpInsRel$fnIns";
        
        $h = curl_init($url);
        curl_setopt($h, CURLOPT_RETURNTRANSFER, TRUE);
        $jsInfo = curl_exec($h);
        $sErr = curl_error($h);
        $nHTTP = curl_getinfo($h,CURLINFO_RESPONSE_CODE);
        curl_close($h);
        
        if ($nHTTP > 299) {
            echo "\n  ERROR: got HTTP code $nHTTP from [$url].\n";
        } elseif ($jsInfo === FALSE) {
            echo "\n  ERROR: could not access [$url]: $sErr\n";
        } else {
        
            // check results
            $arInfo = json_decode($jsInfo, TRUE);
            if (is_null($arInfo)) {
                echo "\n  ERROR: could not parse received data [$jsInfo].\n";
            } else {
                #echo "\nRESULTS: ".print_r($arInfo,TRUE);
                $idUserFnd = $arInfo['user.id.file'];
                if ($idUserFnd == $idUsr) {
                    echo ' (user ok)';
                } else {
                    $sUserFnd = $arInfo['user.name.file'];
                    echo "\n  ERROR: need user [$sUser], found user [$sUserFnd].\n";
                }
                $sVerFnd = $arInfo['php version'];
                $sVerCur = phpversion();
                if ($sVerFnd != $sVerCur) {
                    echo "\n  ERROR: need PHP version [$sVerCur], found user [$sVerFnd].\n";
                }
            }
            
            // check for PHP execution under http (shouldn't happen):
            $url = "http://$sDomain/$fpInsRel$fnIns";
            $h = curl_init($url);
            curl_setopt($h, CURLOPT_RETURNTRANSFER, TRUE);
            $jsInfo = curl_exec($h);
            $sErr = curl_error($h);
            $nHTTP = curl_getinfo($h,CURLINFO_RESPONSE_CODE);
            curl_close($h);
            
            if ($nHTTP < 400) {
                echo "\n  WARNING: able to access PHP via HTTP: $url\n";
            }
        }
        
        // - remove the test executable:
        $ok = unlink($fsInsTarg);
        if (!$ok) {
            echo "\n  WARNING: could not remove [$fpInsTarg].\n";
        }
        
        // - remove the test folder:
        $ok = rmdir($fpInsFull);
        if (!$ok) {
            echo "\n  WARNING: could not remove [$fpInsFull].\n";
        }
    } else {
        echo ' (user test skipped)';
    }
    echo "\n";
    
    // SSL cert checks
    $fpCerts = $fpCertsBase.$sDomain.'/';
    $oSSL = new cCerts($fpCerts,$fsHome);
    $oSSL->CheckCertFile('cert.pem','ssl.cert');
    $oSSL->CheckCertFile('privkey.pem','ssl.key');
    $oSSL->CheckCertFile('chain.pem','ssl.ca');
    
}

/*----
  ACTION: Ensures that all files within the given folder belong to the given user.
    Checks recursively, including but not following links.
*/
function CheckFilePerms(string $fp,int $idUsrWant,int $idGrpWant) {
    $arDir = scandir($fp);
 
    foreach ($arDir as $idx => $fn) {
        // remove the self and parent fake folders
        if ($fn == '.' or $fn == '..') { unset($arDir[$idx]); }
    }
    #echo "LISTING FOR [$fp]: ".print_r($arDir,TRUE);
    foreach ($arDir as $idx => $fn) {
        $fs = $fp.$fn;
        $isFirst = TRUE;
        
        $isLink = is_link($fs);
        
        // check file ownership and fix if needed
        if ($isLink) {
            $ar = lstat($fs);
            $idUsrFnd = $ar['uid'];
            $idGrpFnd = $ar['gid'];
        } else {
            $idUsrFnd = fileowner($fs);
            $idGrpFnd = filegroup($fs);
        }
        
        $okUsr = ($idUsrFnd == $idUsrWant);
        $okGrp = ($idGrpFnd == $idGrpWant);
        if (!$okUsr or !$okGrp) {
            if ($isLink) {
                // change ownership for the *link*, not the target
                if (!$okUsr) {
                    lchown($fs,$idUsrWant);
                }
                if (!$okGrp) {
                    lchgrp($fs,$idGrpWant);
                }
                $sType = '(link)';
            } else {
                if (!$okUsr) {
                    chown($fs,$idUsrWant);
                }
                if (!$okGrp) {
                    chgrp($fs,$idGrpWant);
                }
                $sType = '';
            }
            if ($isFirst) {
                echo "\n";
                $isFirst = FALSE;
            }
            
            $sAct = NULL;
            if (!$okUsr) {
                $sAct = 'user';
            }
            if (!$okGrp) {
                if (!is_null($sAct)) {
                    $sAct .= ',';
                }
                $sAct .= 'group';
            }
            
            // get human-friendly strings instead of IDs, for readout
            
            $ar = posix_getpwuid($idUsrWant);
            $sUsrWant = $ar['name'];
            
            $ar = posix_getpwuid($idUsrFnd);
            if (is_array($ar)) {
                $sUsrFnd = $ar['name'];
            } else {
                $sUsrFnd = '?'.$idUsrFnd;
            }

            $ar = posix_getgrgid($idGrpFnd);
            if (is_array($ar)) {
                $sGrpFnd = $ar['name'];
            } else {
                $sGrpFnd = '?'.$idGrpFnd;
            }
            // Somehow this is getting an extra newline, so... no \n for now
            echo "  CHOWN $sAct [$sUsrFnd:$sGrpFnd] => [$sUsrWant]: $sType$fs";
        }
    
        if (!$isLink and is_dir($fs)) {
            // this is a folder, and not a link: process its contents too
            CheckFilePerms($fs.'/',$idUsrWant,$idGrpWant);
        }
    }
}
/*::::
  PURPOSE: certificate functions
  HISTORY:
    2021-09-13 created
*/
class cCerts {
    public function __construct(string $fpSrce,string $fpDest) {
        $this->SetSrcePath($fpSrce);
        $this->SetDestPath($fpDest);
    }
    
    private string $fpSrce;
    protected function SetSrcePath(string $fp) { $this->fpSrce = $fp; }
    protected function GetSrceSpec(string $fn) : string { return $this->fpSrce.$fn; }
    
    private string $fpDest;
    protected function SetDestPath(string $fp) { $this->fpDest = $fp; }
    protected function GetDestSpec(string $fn) : string { return $this->fpDest.$fn; }
    
    /*----
      ACTION: if $sNameDest has an earlier timestamp than $sNameSrce, replaces $sNameDest with $sNameSrce.
      TODO: use openssl to check for expired cert in source
    */
    public function CheckCertFile(string $fnSrce, string $fnDest) {
        $fsSrce = $this->GetSrceSpec($fnSrce);
        $fsDest = $this->GetDestSpec($fnDest);
        
        // TODO: check for existence of source
        $ndSrce = filemtime($fsSrce);
        $sdSrce = date('Y/m/d',$ndSrce);
        
        if (file_exists($fsDest)) {
            $ndDest = filemtime($fsDest);
            $sdDest = date('Y/m/d',$ndDest);
            $ok = ($ndDest >= $ndSrce);
            if (!$ok) {
                echo "  NOTE: Local SSL file $fnDest ($sdDest) older than source $fnSrce ($sdSrce) - updating...";
            }
        } else {
            $ok = FALSE;
            echo "  NOTE: Local SSL file $fnDest not found; creating from source $fnSrce ($sdSrce)...";
        }
        
        if (!$ok) {
            $ok = copy($fsSrce,$fsDest);
            if ($ok) {
                echo " ok.\n";
            } else {
                echo "\n  ERROR: could not update SSL file [$fsSrce] -> [$fsDest].\n";
            }
        }
    }
}
