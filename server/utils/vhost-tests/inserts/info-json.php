<?php
/*
  PURPOSE: returns information about the environment in the current virtual host, running within that host
  HISTORY:
    2021-09-12 adapting from userid.php (which reported in HTML)
      Thought about using tmpfile() or tempnam() instead of touch()/unlink(), but that just complicates things.
*/

$sVer = phpversion();

$idUserPrc = getmyuid();
$idGroupPrc = getmygid();

$ar = posix_getpwuid($idUserPrc);
$sUserPrc = $ar['name'];

$ar = posix_getgrgid($idGroupPrc);
$sGroupPrc = $ar['name'];

$fs = 'userid.tmp';
$ok = touch($fs);

if ($ok) {
    $idUserFile = fileowner($fs);
    $idGroupFile = filegroup($fs);
    
    $ar = posix_getpwuid($idUserFile);
    $sUserFile = $ar['name'];
    
    $ar = posix_getgrgid($idGroupFile);
    $sGroupFile = $ar['name'];
    
    unlink($fs);
} else {
    echo "Could not create test file '$fs'!<br>";
}
$arOut = [
  'php version' => $sVer,
  'user.id.prc' => $idUserPrc,
  'group.id.prc' => $idGroupPrc,
  'user.name.prc' => $sUserPrc,
  'group.name.prc' => $sGroupPrc,
  'file.ok' => $ok
  ];
if ($ok) {
    $arMore = [
      'user.id.file' => $idUserFile,
      'group.id.file' => $idGroupFile,
      'user.name.file' => $sUserFile,
      'group.name.file' => $sGroupFile
      ];
    $arOut = array_merge($arOut,$arMore);
}
$out = json_encode($arOut,JSON_PRETTY_PRINT);
header('Content-Type: text/json');
echo $out."\n";
