<?php

$arSites = [
  'site1'  => [ // name is arbitrary
      'user' => 'user1',
      'home' => '/home/user1/',
      'dom' => 'cule.oslt.ovh',
      'lang' => 'php'
    ],
  'site2' => [
      'user'  => 'user2',
      'home'   => '/home/user2/domains/sub.site2.ext/',
      'dom' => 'sub.site2.ext',
      'lang' => 'php'
    ],
  ];
