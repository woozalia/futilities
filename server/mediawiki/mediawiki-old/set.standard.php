<?php
/*
  PURPOSE: standard settings for most MediaWiki deployments
  HISTORY:
    2021-06-22 created for HypertWiki
*/


// ++ DEBUGGING ++ //

$fErrLevel = E_ALL | E_STRICT;
error_reporting($fErrLevel);
if (!ini_get('display_errors')) {
    ini_set('display_errors', 'TRUE');
}
$wgShowExceptionDetails = true;
$wgShowDBErrorBacktrace = true;
$wgShowSQLErrors = true;

$wgForceHTTPS = TRUE;

$wgDisableOutputCompression = FALSE;

// -- DEBUGGING -- //
// ++ SETUP ++ //

$fpHTML = $_SERVER['CONTEXT_DOCUMENT_ROOT'];
$fpUser = dirname($fpHTML);

// -- SETUP -- //
// ++ OPTIONS ++ //

$wgAllowExternalImages = TRUE;

// -- OPTIONS -- //
// ++ CONFIG ++ //

#$wgMainCacheType = CACHE_NONE;
$wgMainCacheType = CACHE_ACCEL;
$wgMessageCacheType = CACHE_ACCEL;
$wgCacheDirectory = $fpUser.'/tmp/mw';
$wgUseLocalMessageCache = true;
$wgParserCacheType = CACHE_DB;
$wgMemCachedServers = [];
$wgUseGzip = true;
$wgEnableSidebarCache = true;

$wgArticlePath = "/mw/$1";
$wgUsePathInfo = true;

$wgUploadPath = "/wikiup";
$wgUploadDirectory = "$fpUser/site/var/mw/wikiup";
$wgAllowCopyUploads	= true;
$wgCopyUploadsFromSpecialUpload = TRUE;
$wgGroupPermissions['user']['upload_by_url'] = true;

//$wgUploadSizeWarning = 2147483647;
$wgMaxUploadSize = 2 * 1024 * 1024 * 1024;	// 2GB
ini_set( 'post_max_size', '50M' );
ini_set( 'upload_max_filesize', '50M' );

$wgFileExtensions[] = 'pdf';				// allow upload of PDF files
$wgFileExtensions[] = 'ods';				// allow upload of OpenOffice spreadsheets
$wgFileExtensions[] = 'odt';				// allow upload of OpenOffice text documents
$wgFileExtensions[] = 'doc';				// allow upload of MS Word documents
$wgFileExtensions[] = 'xls';				// allow upload of MS Excel documents
$wgFileExtensions[] = 'ogg';				// allow upload of OGG audio/video

$wgNamespacesWithSubpages = array(
        NS_MAIN			=> true,
        NS_TALK			=> true,
        NS_USER			=> true,
        NS_USER_TALK		=> true,
        NS_PROJECT 		=> TRUE,
        NS_PROJECT_TALK		=> true,
        NS_FILE_TALK		=> true,
        NS_MEDIAWIKI_TALK	=> true,
        NS_TEMPLATE_TALK	=> true,
        NS_HELP			=> true,
        NS_HELP_TALK		=> true,
        NS_CATEGORY		=> true,
        NS_CATEGORY_TALK	=> true,
        NS_SPECIAL	=> true
);
$wgNamespacesToBeSearchedDefault = array(
	NS_MAIN =>		true,
	NS_TALK =>		TRUE,
	NS_USER =>		TRUE,
	NS_USER_TALK =>		TRUE,
	NS_PROJECT =>		TRUE,
	NS_PROJECT_TALK =>	false,
	NS_FILE =>          	TRUE,
	NS_FILE_TALK =>	TRUE,
	NS_MEDIAWIKI =>		false,
	NS_MEDIAWIKI_TALK =>	false,
	NS_TEMPLATE =>		false,
	NS_TEMPLATE_TALK =>	false,
	NS_HELP =>		TRUE,
	NS_HELP_TALK =>		TRUE,
	NS_CATEGORY =>		TRUE,
	NS_CATEGORY_TALK =>	TRUE
);

// ++ EXTENSIONS: packaged ++ //

wfLoadExtension( 'Cite' );
wfLoadExtension( 'CodeEditor' );
wfLoadExtension( 'Interwiki' );
wfLoadExtension( 'MultimediaViewer' );
wfLoadExtension( 'PageImages' );
wfLoadExtension( 'ParserFunctions' );
wfLoadExtension( 'PdfHandler' );
wfLoadExtension( 'Poem' );
wfLoadExtension( 'Scribunto' );
wfLoadExtension( 'SyntaxHighlight_GeSHi' );
wfLoadExtension( 'TemplateData' );
wfLoadExtension( 'TextExtracts' );
wfLoadExtension( 'VisualEditor' );
wfLoadExtension( 'WikiEditor' );

// ParserFunctions
$wgPFEnableStringFunctions = TRUE;
// InterWiki
$wgGroupPermissions['*']['interwiki'] = false;
$wgGroupPermissions['interwiki']['interwiki'] = true;
