#!/usr/bin/php
<?php namespace woozalia\futil\builder;

$fpApp = dirname(__DIR__);      // Builder app home folder

require_once($fpApp.'/config/init/start.php');

cApp::SetVersionString('Config Builder v0.1 / 2024-05-06');
cApp::SetDocsURL('https://wooz.dev/Futilities/server'); // app-specific page not created yet

new cConf($fpApp,$fpAppConfig,$fpAppSource,$fpFutils);
$oApp = new cApp($argv);
$oApp->Go();

echo "All done.\n";
