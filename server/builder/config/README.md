## Purpose: structural definitions

This is where we include sets of other files in a specific order, and define which sets to use.

(Yes, that's kind of vague; I keep changing the details...)

Things that need structure:
* **Configuration scripts** --
  * Top folder: ./config/src
    * This is organized the same as other /src/ folders (class-per-folder, same naming convention).
    * ./config/src/opts/<slug>/ defines the structure for a  server/deployment-target, given that server's "slug"
* **Configuration pieces**
  * Top folder: ./config/inc
* **Stuff on the server/environment**
  * Top folder: ./config/LOCAL
    * Think this should be (in abstract terms) where various options are kept, but we link the ones we want... somewhere.
    * ...and they're namepspaced to be found in that place where they are linked.
