<?php namespace woozalia\futil\builder;
/**
 * INPUT: Domain object from which this is being called
 * OUTPUT: calls one or more Include*() methods in $this->Domain()
 */

// parameters:
$this->WithSlug_forServer('sekretwiki');
$this->WithHostName('stuff.htm.oslt.ovh');
$this->WithUserName('oslt');
$this->WithGroupName('oslt');

// conf chunks:
$this->IncludeConfList([
    'vars/common',

    'vars/http',
#    'main/http.static',
    'vhost/head',
    'vhost/index/static',
    'vhost/redirect/if-na',
    'vhost/redirect/root',
    'vhost/logs',
    'vhost/exclude/php',
    'vhost/dir/main',
    'vhost/redirect/to-https',
    'vhost/foot',

    'vars/https',
    'vhost/head',
    'vhost/index/php',
    'vhost/logs',
    'vhost/dir/main',
    'vhost/lang/php-cgi',
    'vhost/ssl/common',
    'vhost/ssl/certbot',
    'vhost/foot',
  ]); // only named static HTML files should be served without SSL

