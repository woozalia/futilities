<?php namespace woozalia\futil\builder;
/**
 * INPUT: Domain object from which this is being called
 * OUTPUT: calls one or more Include*() methods in $this->Domain()
 * INSTAR: Just serves static files via http or https. No indexes, no redirects.
 */

// parameters:
$this->WithSlug_forServer('vbz-demo');
$this->WithSlug_forUser('demo');
$this->WithHostName('demo.vbz.net');
$this->WithUserName('vbz-net');
$this->WithGroupName('vbz-net');

// conf chunks:
$this->IncludeConfList([
    'vars/common',

    'vars/http',
#    'main/http.static',
    'vhost/head',
    'vhost/index/static',
    'vhost/logs',
    'vhost/exclude/php',
    'vhost/dir/main',
    'vhost/foot',

    'vars/https',
    'vhost/head',
    'vhost/logs',
    'vhost/dir/main',
    'vhost/ssl/common',
    'vhost/ssl/certbot',
    'vhost/foot',

  ]); // only named static HTML files should be served without SSL

