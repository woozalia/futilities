<?php namespace woozalia\futil\builder;
/**
 * INPUT: $fpApp = path to top folder for current app
 * OUTPUT:
 *  $fpFutils = path to top Futilities project folder
 *  $fpAppConfig = path to config folder for current app
 *  $fpAppSource = path to class-library folder for current app
 * ACTION:
 *  loads the class-loader
 */

use woozalia\futil\init\csClassLoader as ClassLoader;

$fpFutils = dirname(__DIR__,4);     // Futilities project home folder

// auto-include startup classes:
  $fsAuto = $fpFutils.'/init/*.php';
  #echo "AUTO-INCLUDE SPEC: $fsAuto\n";
  // Get a list of all .php files in the auto-init folder:
  $arFS = glob($fsAuto);
  // Loop through the files and include each one
  foreach ($arFS as $fs) {
      #echo "INCLUDING $fs\n";
      include $fs;
  }

$fpAppConfig = $fpApp.'/config';
$fpAppSource = $fpApp.'/src';

// set up namespace autoloading:
ClassLoader::Init();
ClassLoader::SetNSpacePath(__NAMESPACE__,$fpAppSource);
ClassLoader::SetNSpacePath(\woozalia\futil::class,$fpFutils.'/src');
ClassLoader::SetNSpacePath(\woozalia\futil\builder\config::class,$fpAppConfig.'/src');
