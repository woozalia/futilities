<?php namespace woozalia\futil\builder\config;

use woozalia\futil\builder\caScripts as BaseClass;
use woozalia\futil\builder\Aux\Server\cConf as ServerConfClass;

/**
  PURPOSE: includes all the remaining var-dependent stuff, after the vars have been defined
*/
class cScripts extends BaseClass {

    // ++ SETUP ++ //

    public function __construct(private string $fpaConfig) {}
    protected function FPA_Config() : string { return $this->fpaConfig; }

    // -- SETUP -- //
    // ++ CONFIG ++ //

    protected function FPA_Local() : string {
        $fpaLocal = $this->FPA_Config().'/LOCAL';
        return $fpaLocal;
    }
    // PUBLIC so Domain can access
    /*
    public function FPA_LocalServer() : string {
        $fpaLocal = $this->FPA_Local();
        $fpaServer = $fpaLocal.'/server';
        return $fpaServer;
    }
    */
    public function FPA_LocalInput() : string {
        $fpaLocal = $this->FPA_Local();
        $fpaSites = $fpaLocal.'/in';
        return $fpaSites;
    }
    public function FPA_LocalOutput() : string {
        $fpaLocal = $this->FPA_Local();
        $fpaOutput = $fpaLocal.'/out';
        return $fpaOutput;
    }
    /* I no longer remember exactly what these are for...
    protected function FPA_IncInit() : string {
        $fpaIncInit = __DIR__;                    // where we are now: base of src files
        return $fpaIncInit;
    }
    protected function FPA_IncStruct() : string {
        $fpaIncInit = $this->FPA_IncInit();
        $fpaIncStruct = dirname($fpaIncInit);     // /struct/ is parent folder of /init/
        return $fpaIncStruct;
    }
    protected function FPA_IncPortable() : string {
        $fpaIncStruct = $this->FPA_IncStruct();
        $fpaIncPortable = dirname($fpaIncStruct); // /portable/ is parent folder of /struct/
        return $fpaIncPortable;
    }
    protected function FPA_ServerTypes() : string {
        $fpaStruct = $this->FPA_IncStruct();
        $fpaServer = $fpaStruct.'/opts';
        return $fpaServer;
    }
    */
    // PUBLIC so Domain instars can use it
    public function FPA_IncBits() : string {
        $fpaIncPortable = $this->FPA_Config();
        $fpaIncBits = $fpaIncPortable.'/inc';
        return $fpaIncBits;
    }
    /*
    protected function FSA_ServerClass() : string {
        $fpaServers = $this->FPA_ServerTypes();
        $fsaServer = $fpaServers.$kfnPortable;
        return $fsaServer;
    }
    */

    // -- CONFIG -- //
    // ++ ACTION ++ //

    public function Go() {
        $fpaIncLocal = $this->FPA_LocalInput();

        // create all config-defined objects

        #require($fpaIncLocal.'/misc.php');  // defines $sServerTypeSlug
        $fsAuto = $fpaIncLocal.'/server/*.php';
        $oServConf = ServerConfClass::FromFileMask($fsAuto);

        // setup for processing local config stuff
        // namespace for structure-class options folder:
        #$snStruct = \woozalia\futil\builder\config\opts::class;
        #$snType = $snStruct.'\\'.$oServConf->TypeSlug().'\\';
        $snType = $oServConf->TypeNamespace();
        $scServer = $snType.'cServer';
        $scDomain = $snType.'cDomain';
        echo " - $scServer\n - $scDomain\n";

        $oServ = new $scServer($oServConf,$this);  // implement Server object from included class
        $oDomain = new $scDomain($oServ);

        // process the locally-defined sites

        $fsAuto = $fpaIncLocal.'/sites/*.php';
        $arFS = glob($fsAuto);
        // Loop through the files and include each one
        foreach ($arFS as $fs) {
            #echo " - Loading $fs\n";
            #include $fs;
            $oDomain->WithDefFile($fs);
        }

        $oServ->WithDomain($oDomain);

        #echo "Loading local settings from $fpaIncLocal...\n";
        // define IP addresses
        #$oServ->WithIPsFile($fpaIncLocal.'/ip.php');

        #echo "Loading DONE; running Server ops...\n";

        #$oServ->Go();

        echo "FINISHED.\n";
    }
}

#$oScripts = new cScripts($fpAppConf);
