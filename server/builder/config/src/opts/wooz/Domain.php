<?php namespace woozalia\futil\builder\config\opts\wooz;

use woozalia\futil\builder\caDomain as BaseClass;

/**
  PURPOSE: Instar subclass of caDomain
  INSTAR: wooz experimental layout (not VirtualMin)
*/
class cDomain extends BaseClass {

    // ++ FILE PATHS ++ //

    protected function FPA_UserHome()    : string {
        $fpaUserHome = $this->Server()->FPA_UserHomeFor($this->UserName());
        return $fpaUserHome;
    }
    protected function FPA_DomainHome() : string {
        $sSlug = $this->Slug_forUser();
        $fpaHome = $this->Server()->FPA_UserHomeFor($this->UserName());
        return "$fpaHome/sites/$sSlug";
    }
    protected function FPA_WebOps() : string { return $this->FPA_DomainHome().'/www'; }
    protected function FPA_DomainPub() : string { return $this->FPA_WebOps().'/public'; }
    // PUBLIC for conf snippets
    public function FPA_LogAccess() : string {
        $fpaLogs = $this->Server()->FPA_Logs();
        $sHostName = $this->HostName();
        $fpaLogAccess = "$fpaLogs/$sHostName.access.log";
        return $fpaLogAccess;
    }
    // PUBLIC for conf snippets
    public function FPA_LogErrors() : string {
        $fpaLogs = $this->Server()->FPA_Logs();
        $sHostName = $this->HostName();
        $fpaLogErrors = "$fpaLogs/$sHostName.errors.log";
        return $fpaLogErrors;
    }
    public function FPA_Certs() : string {
        $sHostName = $this->HostName();
        $fpaCert = "/etc/letsencrypt/live/$sHostName";
        return $fpaCert;
    }

    // -- FILE PATHS -- //
    // ++ WEB PATHS ++ //

    protected function WP_CanonicSecureHome() : string { return 'https://'.$this->HostName().'/'; }

    // -- WEB PATHS -- //
}
