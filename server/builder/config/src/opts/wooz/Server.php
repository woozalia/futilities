<?php namespace woozalia\futil\builder\config\opts\wooz;

use woozalia\futil\builder\caServer as BaseClass;

/**
  PURPOSE: Instar subclass of caServer
  INSTAR: wooz experimental layout (not VirtualMin)
  INPUT:
    $sHostName (set in each LOCAL/sites/LIVE file)
  OUTPUT:
    makes cServer available
*/
class cServer extends BaseClass {
    // TODO: look this up properly (pretty sure there's a way)
    public function FPA_UserHomeFor(string $sUser) : string {
        $fpaUserHome = "/home/$sUser";
        return $fpaUserHome;
    }
    // PUBLIC so Domain can access
    public function FPA_Logs() : string { return "/var/log/apache2/vhost"; }
    protected function CheckConfig() {
        $this->Scripts()->Emitter()->CheckFile($this->FPA_Logs(),'log folder');
    }
}
