<?php namespace woozalia\futil\builder;
/**
  INPUT:
    $this = Scripts object
*/
$oServ = $this->Server();
$oConf = $oServ->Config();

$sHostName = $this->HostName();
$sUserName = $this->UserName();
$sGroupName = $this->GroupName();

$sIPv4 = $oConf->IPv4();
$sIPv6 = $oConf->IPv6();

$fpaUserHome = $this->FPA_UserHome();
$fpaDomainPub = $this->FPA_DomainPub();

$fpaLogErrors = $this->FPA_LogErrors();
$fpaLogAccess = $this->FPA_LogAccess();

$wpCanonicHome = $this->WP_CanonicSecureHome();
