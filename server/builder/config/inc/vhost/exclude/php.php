<?php namespace woozalia\futil\builder;

$sEmit = <<<__END__

    # no access at all to PHP files (usually for http mode)
    <FilesMatch ".+\.ph(ar|p|tml)$">
        Require all denied
    </FilesMatch>
__END__;
