<?php namespace woozalia\futil\builder;

$sEmit = <<<__END__
<VirtualHost $sAddresses>

    SuexecUserGroup $sUserName $sGroupName
    ServerName $sHostName

    DocumentRoot $fpaDomainPub
__END__;
