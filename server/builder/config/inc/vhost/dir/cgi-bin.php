<?php namespace woozalia\futil\builder;

$sEmit = <<<__END__
    <Directory $fpaUserHome/cgi-bin>
        allow from all
        AllowOverride All Options=ExecCGI,Includes,IncludesNOEXEC,Indexes,MultiViews,SymLinksIfOwnerMatch
        Require all granted
    </Directory>__END__;
