<?php namespace woozalia\futil\builder;
/**
 * NOTE 2024-05-13 This has not been tested yet.
*** OLD DOCS ***
# PURPOSE:
#   This needs to be separate because it will cause Apache start to fail if the cert files aren't yet present.
# REQUIRES:
#   username : name of virtual host's user (i.e. /home/<what goes here>)
#   hostname : domain for virtual host (i.e. http[s]://<what goes here>/)
# OPTIONAL
#   IsSubHost : flag (no value) -- if present, server is a subsidiary of another
# HISTORY
#   2021-09-12 This worked on cloud1, I think? With some permissions mods?
#     But I'm not sure what those mods were, and I can't get it to work on cloud5.
#     It seems like a better plan to have the unit-tester check for the files
#     in the ${fpaUserHome}, and report/update if they're out of date.
#     Renaming this to 06a.ssl-direct.conf in case I figure this out later.
*/

$sEmit = <<<__END__
    SSLEngine on

    # CertBot documentation: $fsCertDoc

    ## from VirtualMin, I guess?
    # ssl.cert -> cert.pem
    SSLCertificateFile $fsCertMain
    # ssl.key -> privkey.pem
    SSLCertificateKeyFile $fsCertPriv

    SSLProtocol all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1

    # ssl.ca -> chain.pem
    SSLCACertificateFile $fsCertChain

__END__;
