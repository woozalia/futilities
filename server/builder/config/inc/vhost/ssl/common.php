<?php namespace woozalia\futil\builder;

$fsCertMain = $fpaCert.'/cert.pem';
$fsCertPriv = $fpaCert.'/privkey.pem';
$fsCertChain = $fpaCert.'/chain.pem';
$fsCertFull = $fpaCert.'/fullchain.pem';
$fsCertDoc =  $fpaCert.'/README';

$arCerts = [
  $fsCertMain,
  $fsCertPriv,
  $fsCertChain,
  $fsCertFull,
  ];

$oEmit = $oServ->Scripts()->Emitter();
$oEmit->CheckFiles($arCerts,'certificate file');
