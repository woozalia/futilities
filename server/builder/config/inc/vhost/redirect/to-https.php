<?php namespace woozalia\futil\builder;

$sEmit = <<<__END__
    # 2024-05-09 This stuff ended up in the live conf -- not yet sure what it's for, or if necessary.
    #   Maybe it redirects http to https?
    RewriteEngine on
    RewriteCond %{SERVER_NAME} =$sHostName
    RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
__END__;
