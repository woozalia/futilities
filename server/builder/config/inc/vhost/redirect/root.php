<?php namespace woozalia\futil\builder;

$sEmit = <<<__END__

    # also redirect root requests
    <If "-z %{REQUEST_FILENAME}">
        Redirect / $wpCanonicHome
    </If>
__END__;
