<?php namespace woozalia\futil\builder;

$sEmit = <<<__END__
    # redirect to https if exact file doesn't exist:
    <If "!-f %{REQUEST_FILENAME}">
        Redirect / $wpCanonicHome
    </If>
__END__;
