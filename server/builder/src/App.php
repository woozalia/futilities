<?php namespace woozalia\futil\builder;

use woozalia\futil\caApp as BaseClass;
use woozalia\futil\builder\config\cScripts;

class cApp extends BaseClass {

    // ++ CONFIG ++ //

    static protected function ConfigClass() : string { return cConf::class; }
    static protected function OptionsClass() : string { return cOpts::class; }
    static protected function ScriptsClass() : string  { return cScripts::class; }

    // -- CONFIG -- //
    // ++ SETUP ++ //

    private $oScripts;
    protected function WithScripts(iScripts $o) {  $this->oScripts = $o; }
    protected function Scripts() : iScripts { return $this->oScripts; }

    // -- SETUP -- //
    // ++ ACTION ++ //

    public function Go() {
        $oConf = cConf::Me();
        echo $oConf->DumpValues();

        $fpAppConf = $oConf->FP_Conf();
        #require($fpAppConf.'/portable/struct/init/scripts.php'); // defines script-loader class/object -> $oScripts
        $sc = $this->ScriptsClass();
        $oScripts = new cScripts($fpAppConf);

        $this->WithScripts($oScripts);
        $this->Scripts()->Go();
    }

    // -- ACTION -- //
}
