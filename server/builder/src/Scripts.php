<?php namespace woozalia\futil\builder;

interface iScripts {
    // ++ ACTION ++ //
    function Go();
    // -- ACTION -- //
    // ++ CONFIG ++ //
    function FPA_LocalInput() : string;
    function FPA_LocalOutput() : string;
    // -- CONFIG -- //
}
/**
 * PURPOSE: framework for defining where variable Scripts may be found
 */
abstract class caScripts implements iScripts {

    // ++ SETUP ++ //

    public function __construct() {
        $this->CheckConfig();
    }

    // -- SETUP -- //


    protected function CheckConfig() {}

    // ++ SETUP: dynamic/internal ++ //

    private $oEmit = NULL;
    protected function WithEmitter(cEmitter $o) { $this->oEmit = $o; }
    public function Emitter() : cEmitter {
        if (is_null($this->oEmit)) {
            $fpaOutput = $this->FPA_LocalOutput();
            $oEmit = cEmitter::FromPath($fpaOutput);
            $this->oEmit = $oEmit;
        }
        return $this->oEmit;
    }

    // -- SETUP -- //
    // ++ CONFIG ++ //

    abstract protected function FPA_Local() : string;       // local config files

    /* I no longer remember what these are for:
    abstract protected function FPA_ServerTypes() : string; // folder with subfolders for each server-type slug
    abstract protected function FPA_IncInit() : string;     // where the start-file is
    abstract protected function FPA_IncStruct() : string;
    abstract protected function FPA_IncPortable() : string;
    #abstract protected function FS_VSiteInit() : string;
    */

    // -- CONFIG -- //
}
