<?php namespace woozalia\futil\builder;

interface iDomain {
    #function __set(string $sName, string $sValue);
    #function __get(string $sName);
    function WithDefFile(string $fsa);
    function IncludeConfBits(string $sName);
    function FPA_Certs() : string;
}

/**
  PURPOSE: defines where things are for each Domain (virtual server)
  I/O:
    LOAD TIME (file):
      INPUT: $oServer
      OUTPUT: none here; instar makes Server interface available
    RUN TIME (object):
      INPUT: to $oDomain->WithHostName()
      OUTPUT: conf files, I think...
*/
abstract class caDomain implements iDomain {

    // ++ SETUP ++ //

    public function __construct(private iServer $oServer) {}
    protected function Server() : iServer { return $this->oServer; }
    protected function Scripts() : iScripts { return $this->Server()->Scripts(); }    // SHORTCUT
    protected function Emitter() : cEmitter { return $this->Scripts()->Emitter(); }  // SHORTCUT
    // ++ VALUES ++ //

    #protected function SitesPath() : string { return $this->Scripts()->FPA_Sites(); }

    // ++ VALUES: figuring ++ //

    #protected function SiteFilesArray() : Directory { return dir($this->SitesPath()); }
    protected function SiteFilesArray() : array { return glob($this->SitesPath().'/*.php'); }

    // -- VALUES -- //

    // 2024-04-23 I think this is the *only* reason that Server needs a pointer to Domain...
    public function WithDefFile(string $fsa) {
        echo "Including site definition from [$fsa]\n";
        include $fsa;
    }

    // ++ SETUP: dynamic: values ++ //

    // This slug must be unique across the whole server.
    private $sSlug_forServer;
    protected function WithSlug_forServer(string $s) {
        $this->sSlug_forServer = $s;
        $this->Emitter()->WithName($s);
    }
    // Q 2024-05-08 what else is this used for?
    protected function Slug_forServer() : string { return $this->sSlug_forServer; }

    // This slug only needs to be unique within its user; defaults to forUser slug.
    private $sSlug_forUser = NULL;
    protected function WithSlug_forUser(string $s) { $this->sSlug_forUser = $s; }
    protected function Slug_forUser() : string { return $this->sSlug_forUser ?? $this->Slug_forServer(); }

    private $sHostName;
    protected function WithHostName(string $s) { $this->sHostName = $s; }
    protected function HostName() : string { return $this->sHostName; }

    private $sUserName;
    protected function WithUserName(string $s) { $this->sUserName = $s; }
    protected function UserName() : string { return $this->sUserName; }

    private $sGrpName;
    protected function WithGroupName(string $s) { $this->sGrpName = $s; }
    protected function GroupName() : string { return $this->sGrpName; }

    abstract protected function WP_CanonicSecureHome() : string;

    // -- SETUP -- //
    // ++ CONFIG ++ //

    abstract protected function FPA_UserHome()    : string;
    abstract protected function FPA_DomainHome()  : string;
    abstract protected function FPA_DomainPub()   : string;
    abstract protected function FPA_LogAccess()   : string;
    abstract protected function FPA_LogErrors()   : string;

    // -- CONFIG -- //
    // ++ ACTION ++ //

    public function Go() {
        // TODO: include all the Domain stuff.
    }

    /**
     * PUBLIC so it can be called from local server config scripts
     * HISTORY:
     *  2024-05-07 changed to PUBLIC
     */
    public function IncludeConfBits(string $sName) {
        $fs = $this->Server()->Scripts()->FPA_IncBits().'/'.$sName.'.php';
        echo " - SOLO INCLUDING $fs\n";
        include $fs;
    }
    public function IncludeConfList(array $arNames) {
        $oServ = $this->Server();
        $oScr = $oServ->Scripts();
        foreach ($arNames as $sName) {
            $fs = $oScr->FPA_IncBits().'/'.$sName.'.php';
            $sEmit = NULL;  // reset the output var

            echo " - INCLUDING $fs\n";  // debugging
            include $fs;  // include the file

            if (is_string($sEmit)) {  // if there's output...
                $sSep = "\n";
                #$sSep .= "# $fs\n"; // optional: debugging

                // ...emit it.
                $sOut = $sSep.$sEmit;
                $oScr->Emitter()->Emit($sOut);
            }
        }
    }

    // -- ACTION -- //
}
