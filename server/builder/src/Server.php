<?php namespace woozalia\futil\builder;
/**
  PURPOSE: defines where web and system things are, relative to local paths.
    This ONLY applies to server-wide settings, not anything domain-specific.
    Local config determines which instar (subclass) to use.
    Does not include locations of scripts, which don't change across deployments.
  INPUT:
    $sHostName (set in each LOCAL/sites/LIVE file)
  OUTPUT:
    none here; instar makes cServer available
*/

#use Directory;
##
use woozalia\futil\builder\Aux\Server\cConf as ServerConfClass;
use woozalia\futil\builder\Aux\Server\iConf as ServerConfIface;

interface iServer {
    function Scripts() : iScripts;
    function WithDomain(iDomain $o);
    function FPA_Logs() : string;
}

abstract class caServer implements iServer {

    // ++ SETUP ++ //

    public function __construct(private ServerConfClass $oConf, private iScripts $oScripts) {
        #self::$oMe = $this;
        $this->CheckConfig();
    }
    public function Config() : ServerConfIface { return $this->oConf; }
    public function Scripts() : iScripts { return $this->oScripts; }

    private $oDomain = NULL;
    public function WithDomain(iDomain $o) { $this->oDomain = $o; }
    protected function Domain() : iDomain { return $this->oDomain; }

    // ++ CONFIG ++ //

    abstract protected function CheckConfig();

    // -- CONFIG -- //
}
