<?php namespace woozalia\futil\builder;

use woozalia\futil\cConf as BaseClass;

class cConf extends BaseClass {

    // ++ SETUP ++ //

    public function __construct(
      private string $fpApp,
      private string $fpAppConf,
      private string $fpAppLibs,
      private string $fpFutils) {
        parent::__construct();
    }

    // -- SETUP -- //
    // ++ VALUES ++ //

    public function FP_Home() : string { return $this->fpApp; }
    public function FP_Conf() : string { return $this->fpAppConf; }
    public function FP_Libs() : string { return $this->fpAppLibs; }
    public function FP_Futils() : string { return $this->fpFutils; }

    // -- VALUES -- //
    // ++ DIAGS ++ //

    public function DumpValues() : string {
        $fpAppConf = $this->FP_Conf();
        $fpAppLibs = $this->FP_Libs();

        $s =
          "CONFIG FOLDERS: $fpAppConf\n"
          ."LIBRARY FOLDERS: $fpAppLibs\n"
          ;
        return $s;
    }
}
