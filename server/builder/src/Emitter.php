<?php namespace woozalia\futil\builder;

class cEmitter {

    // ++ SETUP: class init ++ //

    protected function __construct(){}  // disallow direct construction

    static public function FromPath(string $fpOut) {
        $oThis = new static;
        $oThis->WithOutputPath($fpOut);
        return $oThis;
    }
    private $fpOut;
    protected function WithOutputPath(string $fp) { $this->fpOut = $fp; }
    protected function OutputPath() : string { return $this->fpOut; }

    // ++ SETUP: object factory ++ //

    /*
    static protected function FromName(string $s) {
        $oThis = new static;
        $oThis->WithName($s);
        return $oThis;
    }
    */

    // ++ SETUP: dynamic ++ //

    private $sName;
    protected function ClearName() { $this->sName = NULL; }
    public function WithName(string $s) {
        $this->sName = $s;
        $fsConf = $this->FSpecConf();
        if (file_exists($fsConf)) {
            $this->Report("NOTE: old [$s] output file exists; deleting.");
            $ok = unlink($fsConf);
            if (!$ok) {
                $this->Report("ERROR: Could not delete $fsConf.");
            }
        }
    }
    protected function FSpecConf() : string {
        $sName = $this->sName;
        if (is_null($sName)) {
            throw new \exception('Output file name has not been set.');
        } else {
            return $this->OutputPath()."/$sName.conf";
        }
    }

    // -- SETUP -- //
    // ++ OUTPUT: confs ++ //

    public function Emit(string $s) {
        file_put_contents(
          $this->FSpecConf(),
          $s,
          FILE_APPEND
        );
    }

    // ++ OUTPUT: UI ++ //

    protected function Report(string $sMsg) {
        echo $sMsg."\n";
    }

    // -- OUTPUT -- //
    // ++ CHECKING ++ //

    public function CheckFiles(array $arFiles, string $sName) {
        foreach ($arFiles as $fs) {
            $this->CheckFile($fs,$sName);
        }
    }
    public function CheckFile(string $fs, string $sName) : bool {
        $oOpts = cApp::Me()->Options();
        if ($oOpts->CheckEnvirons()) {
            $ok = file_exists($fs);
            if (!$ok) {
                $this->Report("ERROR: Cannot access $sName [$fs].");
            }
        } else {
            $ok = TRUE;
        }
        return $ok;
    }

    // -- CHECKING -- //
}
