<?php namespace woozalia\futil\builder;

use woozalia\futil\caOpt as BaseClass;

/**
 * USAGE:
 *  Both of these would be OFF for local development, and eventually turned on for deployment on-site.
 * TODO:
 *  2024-05-05 remote deployment via ssh/sftp
 */
class cOpts extends BaseClass {
    // ++ CONFIG ++ //

    const ksOPT_CHECK_ENV     = 'ck.env';   // check the environment
    const ksOPT_DO_DEPLOY     = 'do.dep';   // do deployment

    protected function SetupAppOptions() {
        $this->AddAppOptions([
          self::ksOPT_CHECK_ENV     => ['-ce','--chk-env'],
          self::ksOPT_DO_DEPLOY     => ['-dd','--do-deploy'],
          ]);
    }
    public function DescribeSelf() : string {
        $doChk = $this->CheckEnvirons();
        $doDep = $this->DoDeployment();
        if ($doChk || $doDep) {
            if ($doChk) {
                $s = "Check environment while building conf files";
            } else {
                $s = "Build the conf files";
            }
            if ($doDep) {
                $s .= " and then deploy the results";
            }
            $s .= '.';
        } else {
            $s = "Build conf files locally, with no environment interaction.";
        }
        return $s;
    }
    // ++ VALUES ++ //

    public function CheckEnvirons() : bool { return $this->HasCmdOption(self::ksOPT_CHECK_ENV); }
    public function DoDeployment() : bool { return $this->HasCmdOption(self::ksOPT_DO_DEPLOY); }

    // -- VALUES -- //
    // ++ SETUP ++ //

    // CEMENT caOptsBase
    protected function RenderAppUsage() : string {
        $sThis = $this->GetSelfName();
        return "$sThis [<options>][ <conf file slug>[ <conf file slug>[...]]]";
    }
    // CEMENT caOptsBase
    protected function MinQtyTerms() : int { return 0; }  // for now, anyway
    // CEMENT caOptsBase
    protected function HandleTerm($idx,$sTerm) {
        switch ($idx) {
          case 0:
            // TODO
            break;
        }
    }
}
