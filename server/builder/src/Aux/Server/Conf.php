<?php namespace woozalia\futil\builder\Aux\Server;

interface iConf {
    static function FromFileMask(string $fsMask) : self;
    function WithIPs(string $s4, string $s6);
    function IPv4() : string;
    function IPv6() : string;
}

/**
 * PURPOSE: config data for Server object
 *  We can't create the Server object until we have this stuff, because it tells us what class to use.
 * HISTORY:
 *  2024-05-08 created
 */
class cConf implements iConf {

    // ++ SETUP ++ //

    static public function FromFileMask(string $fsMask) : self {
        $oThis = new static;
        $oThis->WithFileMask($fsMask);
        return $oThis;
    }

    // ++ SETUP: include ++ //

    protected function WithFileMask(string $fsMask) {
        echo "Auto-including local server settings from: $fsMask\n";
        // Get a list of all .php files in the auto-init folder:
        $arFS = glob($fsMask);
        // Loop through the files and include each one
        foreach ($arFS as $fs) {
            echo " - Loading $fs\n";
            include $fs;
        }
    }

    // ++ SETUP: values ++ //

    private $sTypeSlug; // server-type slug
    protected function WithTypeSlug(string $s) { $this->sTypeSlug = $s; }
    protected function TypeSlug() : string { return $this->sTypeSlug; }

    private $sIPv4, $sIPv6;
    public function WithIPs(string $s4, string $s6) {
        $this->sIPv4 = $s4;
        $this->sIPv6 = $s6;
    }
    public function IPv4() : string { return $this->sIPv4; }
    public function IPv6() : string { return $this->sIPv6; }

    // -- SETUP -- //
    // ++ FIGURING ++ //

    public function TypeNamespace() : string {
        $snStruct = \woozalia\futil\builder\config\opts::class;
        $snType = $snStruct.'\\'.$this->TypeSlug().'\\';
        return $snType;
    }

    // -- FIGURING -- //
}
