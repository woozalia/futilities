<?php namespace woozalia\futil;
/*
  PURPOSE: common configuration stuff for FIC and FTI
    This should never include any sensitive information.
*/
class cConf {

    // ++ SETUP ++ //

    static private $oMe;
    static public function Me() : self { return self::$oMe; }
    static public function HasMe() : bool { return is_object(self::$oMe); }
    public function __construct() { self::$oMe = $this; }

    // -- SETUP -- //
    // ++ DATA ++ //

    // TODO: somehow get this via ANSI?
    public function ScreenWidth() : int { return 180; }
    // NOTE: Be careful not to accidentally type '.' instead of ',' for the list-item separator.
    // TODO: 2022-10-21 this should probably use regex instead of glob
    public function MasksToIgnore() : array {
        return [
          'cache*',
          '*.cache*',
          'files_trashbin',
          '*files_versions*',
          '.sync_*',
          '._sync_*',
          'temp',
          '*.tmp',
          '*thumbnails*',
          ];
    }
    // TODO: change this to a userspace folder
    #public function RemoteMountBase() : string { return '/mnt/futil'; }
    public function RemoteMountBaseRel() : string { return '/mnt/futil'; }

    public function MaxReadRetries() : int { return 5; }

    // -- DATA -- //
}
