<?php namespace woozalia\futil\Opt;


use woozalia\futil\caOpt as BaseClass;
use woozalia\futil\Opt\Aux\Has\Source\tMountable as SourceTrait;
use woozalia\futil\Opt\Aux\Sub\cListing as OptListClass;
use woozalia\futil\Opt\Aux\Sub\iListing as OptListIface;

/*::::
  PURPOSE: Options for folder-tree crawlers
  HISTORY:
    2022-08-31 created
    2022-09-13 moved from opt.php to fspider.php
    2024-04-29 converted to class-per-file
      renamed from .\caOptsSpider -> .\Opt\caSpider
*/
abstract class caSpider extends BaseClass {
    use SourceTrait;

    const ksOPT_LIST_FOUND      = 'list.chk';   // show found items, one line each

    /*
    const ksOPT_SHOW_COMMANDS   = 'show.cmd';   // show each command used (grep)
    const ksOPT_USE_SUDO        = 'use.sudo'  ; // where possible/applicable, use sudo
    */

    protected function SetupOptions() {
        parent::SetupOptions();
        $this->SetupSpiderOptions();
    }
    protected function SetupSpiderOptions() {
        $this->AddAppOptions([
          self::ksOPT_LIST_FOUND    => ['-lf','--list-found'],
        ]);
    }

    // ++ NEW OPTIONS ++ //

    // TODO: change name to something that indicates we're listing all items found (ListFound()? ListSearched()?)
    public function ListingOptions() : OptListIface { return $this->GetSubOpts(self::ksOPT_LIST_FOUND,OptListClass::class); }

    // mask for files to match
    private $fnMask = NULL;
    protected function SetFileMask(string $fn) { $this->fnMask = $fn; }
    public function GetFileMask() : string { return $this->fnMask; }
    public function HasFileMask() : bool { return is_string($this->fnMask); }

    // -- NEW OPTIONS -- //

}
