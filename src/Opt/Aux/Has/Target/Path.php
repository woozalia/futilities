<?php namespace woozalia\futil\Opt\Aux\Has\Target;

/**
 * HISTORY:
    2024-04-29 converted to trait-per-file
      renamed .\tOpts_HasTargetPath -> .\Opt\Aux\Has\Source\tPath
*/


trait tPath {
    private $fpTarg = NULL;
    protected function SetTargetPath(string $fs) { $this->fpTarg = $fs; }
    public function HasTargetPath() : bool { return !is_null($this->fpTarg); }
    public function GetTargetPath() : string { return $this->fpTarg; }
}
