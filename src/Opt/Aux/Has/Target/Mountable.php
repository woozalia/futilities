<?php namespace woozalia\futil\Opt\Aux\Has\Target;

use woozalia\futil\IO\Store\caFolder as FolderClass;
use woozalia\futil\IO\Store\iFolder as FolderIface;

/**
 * HISTORY:
    2022-10-24 NOTE: This is just tOpts_HasSource_Mountable with "target/targ" changed to "source/srce". Surely there's a way to generalize?
    2024-04-29 converted to trait-per-file
      renamed .\tOpts_HasTarget_Mountable -> .\Opt\Aux\Has\Target\tMountable
*/
// NOTE:
trait tMountable {
    use tPath { tPath::GetTargetPath as GetTargetPath_req; }

    // OVERRIDE tOpts_HasTargetPath
    public function GetTargetPath() : string { return $this->TargetMount()->GetLocalSpec(); }

    // ++ OBJECTS ++ //

    private $oMountTarg = NULL;
    protected function TargetMount() : FolderIface {
        if (is_null($this->oMountTarg)) {
            $fpBase = $this->GetTargetPath_req();
            #echo "FPBASE: [$fpBase]\n";
            $oMount = FolderClass::MakeObject($fpBase);
            #$oMount->SetUseSudo($this->Options()->UseSudo());
            $oMount->Open();
            #$fpA = $oMountA->GetLocalSpec();
            $this->oMountTarg = $oMount;
        }
        return $this->oMountTarg;
    }

    // -- OBJECTS -- //
}
