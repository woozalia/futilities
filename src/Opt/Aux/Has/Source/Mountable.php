<?php namespace woozalia\futil\Opt\Aux\Has\Source;

use woozalia\futil\IO\Store\caFolder as FolderClass;
use woozalia\futil\IO\Store\iFolder as FolderIface;

/**
 * HISTORY:
    2024-04-29 converted to trait-per-file
      renamed .\tOpts_HasSource_Mountable -> .\Opt\Aux\Has\Source\tMountable
*/
trait tMountable {
    use tPath { tPath::GetSourcePath as GetSourcePath_req; }

    // OVERRIDE tOpts_HasSourcePath
    public function GetSourcePath() : string { return $this->SourceMount()->GetLocalSpec(); }

    // ++ OBJECTS ++ //

    private $oMountSrce = NULL;
    protected function SourceMount() : FolderIface {
        if (is_null($this->oMountSrce)) {
            $fpBase = $this->GetSourcePath_req();
            #echo "FPBASE: [$fpBase]\n";
            $oMount = FolderClass::MakeObject($fpBase);
            #$oMount->SetUseSudo($this->Options()->UseSudo());
            $oMount->Open();
            #$fpA = $oMountA->GetLocalSpec();
            $this->oMountSrce = $oMount;
        }
        return $this->oMountSrce;
    }

    // -- OBJECTS -- //
}
