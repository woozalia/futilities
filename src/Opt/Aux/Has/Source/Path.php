<?php namespace woozalia\futil\Opt\Aux\Has\Source;

/*::::
  PURPOSE: support for a standard source-path option
    This version is specifically if we *don't* want remote-mount support.
    Use tMountable instead if that *is* wanted.
  HISTORY:
    2024-04-29 converted to trait-per-file
      renamed from .\tOpts_HasSourcePath to .\Opt\Aux\Has\Source\tPath
*/
trait tPath {

    private $fpSrce = NULL;
    protected function SetSourcePath(string $fs) { $this->fpSrce = $fs; }
    public function HasSourcePath() : bool { return !is_null($this->fpSrce); }
    public function GetSourcePath() : string {
        if ($this->HasSourcePath()) {
            $fp = $this->fpSrce;
        } else {
            $fp = '';
        }
        return $fp;
    }
}
