<?php namespace woozalia\futil\Opt\Aux\Pair;

use woozalia\futil\Opt\Aux\cPair as BaseClass;
use woozalia\futil\Opt\Aux\iPair as BaseIface;
use woozalia\futil\Opt\Aux\cSub as SubOptClass;
use woozalia\futil\Opt\Aux\iSub as SubOptIface;

interface iMain extends BaseIface {
    public function GetOpts() : SubOptIface;
    public function SetSubClass(string $sc);
}

/*::::
  ADDED: key-value pair is parsed from a single string
    This defines the standard format for primary command options.
  HISTORY:
    2022-08-01 created
    2022-10-16 renamed from cOpt to cOptMain
    2024-04-30 converted to class-per-file
      renamed from .\cOptMain -> .\Opt\Aux\Pair\cMain
      added iMain
*/
class cMain extends BaseClass implements iMain {
    public function __construct(string $sArg) {
        $npMark = strcspn($sArg,':=');
        if ($npMark > 0) {
            // get just the option's UI key
            $sKey = substr($sArg,0,$npMark);
            // get the value (if any)
            $sVal = substr($sArg,$npMark+1);
            // store both parts locally
            // NOTE that this is the key used in the UI, not the app's internal key
            $this->SetKey($sKey);
            $this->SetVal($sVal);
        } else {
            throw new \exception('2022-08-01 This should never happen. Internal error.');
        }
    }

    public function GetOpts() : SubOptIface {
        $sc = $this->GetSubClass();
        return new $sc($this->GetVal());
    }

    private $sc = SubOptClass::class;
    public function SetSubClass(string $sc) { $this->sc = $sc; }
    protected function GetSubClass() : string { return $this->sc; }
}
