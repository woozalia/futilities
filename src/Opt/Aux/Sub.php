<?php namespace woozalia\futil\Opt\Aux;

interface iSub {
    function HasAny() : bool;
    function HasOption(string $sName) : bool;
    function GetOption(string $sName) : cPair;
    function GetOptionNz(string $sName, $sDefault='') : string;
}

/*::::
  PURPOSE: To allow a single option-value to contain multiple additional values,
    so we don't have to overpopulate the namespace with lots of related names
  FORMAT: <so name>=<so value>[:<so name>=<so value>[:...]]
  HISTORY:
    2022-10-13 started
    2022-10-20 working; added option for no args in constructor,
      to allow object to indicate that nothing is set
    2024-04-29 converted to class-per-file
      renamed .\cSubOpts -> .\Opt\Aux\cSub
      added iSub
*/
class cSub implements iSub {
    public function __construct(string $s=NULL) {
        if (is_string($s)) {
            // replace quoted bits with tokens
            echo "SUBOPTS: [$s]\n";
            $oDequot = $this->ReplaceQuotes($s);
            $this->ParseData($oDequot);
            echo "ARRAY ENTRIES: ".count($this->arOpts)."\n";
        }
    }

    // ++ DATA ACCESS ++ //

    private $arOpts = [];
    public function HasAny() : bool { return count($this->arOpts) > 0; }
    protected function SetOption(string $sName, string $sVal) {
        $oOpt = new cOptPair;
        $oOpt->SetBoth($sName,$sVal);
        $this->arOpts[$sName] = $oOpt;
    }
    public function HasOption(string $sName) : bool { return array_key_exists($sName, $this->arOpts); }
    public function GetOption(string $sName) : cPair {
        if ($this->HasOption($sName)) {
            return $this->arOpts[$sName];
        } else {
            throw new \exception("CALLER ERROR: No value found for option [$sName].");
        }
    }
    public function GetOptionNz(string $sName, $sDefault='') : string {
        if ($this->HasOption($sName)) {
            return $this->GetOption($sName)->GetVal();
        } else {
            return $sDefault;
        }
    }

    // -- DATA ACCESS -- //
    // ++ INTERNAL FIGURING ++ //

    const KS_QUOTE_TOKEN = "\tQ";

    protected function ReplaceQuotes(string $s) : cProtectedTokens {
        $isQuo = TRUE;
        $inQuo = FALSE;
        $sOut = '';
        $dxQuo = 0;
        $arQuotes = [];
        while ($isQuo) {
            $dxPrev = $dxQuo;
            $dxQuo = strpos($s,'"',$dxQuo); // search for next quote, from $dxQuo forward
            if (FALSE === $dxQuo) {
                $isQuo = FALSE;
                $sOut .= substr($s,$dxPrev);    // append remainder of input string
            } else {
                if ($inQuo) {
                    // we've found the matching quote-mark
                    $dxStop = $dxQuo;
                    $sQuoted = substr($s,$dxQuo,$dxStop-$dxQuo);
                    $dxQuo++;   // start next search *after* the found quotemark
                    // use place-holder for quoted bit
                    $sOut .= self::KS_QUOTE_TOKEN;
                    // stash quoted bit in an array
                    $arQuotes[] = $sQuoted;
                } else {
                    // non-quoted -- add verbatim to output
                    $dxStart = $dxQuo;
                    $sOut .= substr($s,$dxPrev,$dxQuo-$dxPrev);
                }
                $inQuo = !$inQuo;
            }
        }
        $oSafe = new cProtectedTokens($sOut,$arQuotes);
        return $oSafe;
    }
    /*----
      ACTION: break up the string into key-value pairs and store internally
    */
    protected function ParseData(cProtectedTokens $oProt) {
        $sSafe = $oProt->SafeString();
        // split the string into "key=value" pieces
        #echo "SSAFE=[$sSafe]\n";
        $arTerms = explode(':',$sSafe);
        $oProt->ResetRepl();
        foreach ($arTerms as $dx => $sTerm) {
            $sPost = $oProt->ReplaceTokens($sTerm,self::KS_QUOTE_TOKEN);
            // we now have a term with any original quoted pieces restored
            if (strpos($sTerm,'=') === FALSE) {
                // there's no value
                $sKey = $sTerm;
                $sVal = '';
            } else {
                // split key=value into key and value
                [$sKey,$sVal] = explode('=',$sTerm);    // don't know if this will work when there's no '='
            }
            $this->SetOption($sKey,$sVal);
        }
    }
    // -- INTERNAL FIGURING -- //
}
