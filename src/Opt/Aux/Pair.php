<?php namespace woozalia\futil\Opt\Aux;

interface iPair {
    function SetBoth(string $sKey, string $sVal);
    function GetKey() : string;

    function GetVal() : string;
    function HasVal() : bool;
}

/*::::
  PURPOSE: basically just a key-value pair
  HISTORY:
    2022-10-13 created for cSubOpts -- is basically the core functionality of what is currently cOpts (to be renamed)
      TODO: rename this to cOpt, and rename cOpt to something more specific
    2022-10-16 renamed from cOptBase to cOptPair
    2024-04-29 converted to class-per-file
      renamed from .\cOptPair -> .\Opt\Aux\cPair
*/
class cPair implements iPair {
    public function SetBoth(string $sKey, string $sVal) {
        $this->SetKey($sKey);
        $this->SetVal($sVal);
    }

    private $sKey;
    protected function SetKey(string $s) { $this->sKey = $s; }
    public function GetKey() : string { return $this->sKey; }

    private $sVal;
    protected function SetVal(string $s) { $this->sVal = $s; }
    public function GetVal() : string { return $this->sVal; }
    public function HasVal() : bool { return $this->sVal != ''; }

    // ++ DEBUGGING ++ //

    public function ShowSelf() : string { return '['.$this->GetKey().'] = ['.$this->GetVal().']'; }
}
