<?php namespace woozalia\futil\Opt\Aux\Is;

/**
 * PURPOSE: for apps with scrolling output (vs. fixed-area),
 *  we might want to selectively enable what gets listed (to reduce scrollclutter).
 * USED BY: cOpt tree
 * HISTORY:
 *  2024-05-02 created - moved --show-progress stuff here
 */
interface iScroller {
    function ShowProgress()  : bool;
}
trait tScroller { // IMPLEMENTS: iScroller

    // ++ SETUP ++ //

    const ksOPT_SHOW_PROGRESS   = 'show.prg';   // show progress (without filling up screen)

    protected function SetupBaseOptions() {
        parent::SetupBaseOptions();
        $this->AddAppOptions([
          self::ksOPT_SHOW_PROGRESS => ['-sp','--show-progress'],
          ]);
    }

    // ++ SETUP ++ //
    // ++ OPTIONS ++ //

    public function ShowProgress()  : bool {
        // 2nd condition: if we're listing anything, that fights with the progress-shower - so don't.
        return ($this->HasCmdOption(self::ksOPT_SHOW_PROGRESS) and !$this->ListingOptions()->UseScreen());
    }

    // -- OPTIONS -- //
}
