<?php namespace woozalia\futil\Opt\Aux;

class cProtectedTokens {

    private $sProt,$arRep;

    public function __construct(string $s, array $arReplaced) {
        $this->sProt = $s;          // string ready for replacement ops
        $this->arRep = $arReplaced; // the bits we want to protect
    }
    public function SafeString() : string { return $this->sProt; }
    public function ProtBits() : array { return $this->arRep; }

    private $dxRepl;
    public function ResetRepl() { $this->idxRepl = 0; }
    /*----
      ACTION: for each token found, replace it with the next item from $arRep
      HISTORY:
        2022-10-16 written, but not really tested
    */
    public function ReplaceTokens(string $sMain, string $sToken) : string {
        $arMain = explode($sToken,$sMain);
        $arRepl = $this->ProtBits();

        $out = '';
        foreach($arMain as $ndx => $sMainBit) {
            $out .= $sMainBit;
            if (array_key_exists($ndx,$arRepl)) {
                $sRepl = $arRepl[$ndx];
                $out .= $sRepl;
            }
        }
        #echo "RESULT: [$out]\n";
        return $out;
    }
}
