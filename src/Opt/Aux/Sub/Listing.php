<?php namespace woozalia\futil\Opt\Aux\Sub;

use woozalia\futil\Opt\Aux\cSub as BaseClass;

interface iListing {
    function UseScreen()   : bool;
    function UseLogFile()  : bool;
    function LogFileSpec() : string;

    function DoFiles() : bool;
    function DoFolders() : bool;
}

/**
 * HISTORY:
 *  2024-04-30 converting to class-per-file
 *    renamed .\cListingOptions -> .\Opt\Aux\Sub\cListing
 */
class cListing extends BaseClass implements iListing {
    const ksARG_LIST_INPUT        = 'i';          // what to include in listings
      const ksVAL_LIST_FOUND_FILES   = 'fi';     // list found files
      const ksVAL_LIST_FOUND_FOLDERS = 'fo';     // list found folders
    const ksARG_LIST_OUTPUT_FILE  = 'f';      // filename for listing output
    const ksARG_LIST_OUTPUT_SCRN  = 's';      // show listing on screen

    public function UseScreen()   : bool { return $this->HasOption(self::ksARG_LIST_OUTPUT_SCRN); }
    public function UseLogFile()  : bool { return $this->HasOption(self::ksARG_LIST_OUTPUT_FILE); }
    public function LogFileSpec() : string { return $this->GetOption(self::ksARG_LIST_OUTPUT_FILE)->GetVal(); }

    protected function InputTypes() : string { return $this->GetOptionNz(self::ksARG_LIST_INPUT); }
    public function DoFiles() : bool { return strpos($this->InputTypes(),self::ksVAL_LIST_FOUND_FILES) !== FALSE; }
    public function DoFolders() : bool { return strpos($this->InputTypes(),self::ksVAL_LIST_FOUND_FOLDERS) !== FALSE; }

    // ++ UI ++ //

    public function DescribeSelf() : string {
        $bFi = $this->DoFiles();
        $bFo = $this->DoFolders();
        $bFB = ($bFi and $bFo);   // both files and folders
        $bScr = $this->UseScreen();
        $bLog = $this->UseLogFile();
        $bOB = ($bScr and $bLog); // both screen and logfile
        if ($bLog) { $fsLog = $this->LogFileSpec(); }

        $s = 'list '
          .($bFi?'files':'')
          .($bFB?' and ':'')
          .($bFo?'folders':'')
          .' '
          .($bScr?'onscreen':'')
          .($bOB?' and ':'')
          .($bLog?"logfile ($fsLog)":'')
          ;
        return $s;
    }

    // -- UI -- //
}
