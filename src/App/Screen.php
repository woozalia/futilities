<?php namespace woozalia\futil\App;

use woozalia\futil\caApp as BaseClass;
use woozalia\futil\IO\Screen\iFull as ScreenIface;

/**
  PURPOSE: App that outputs in full-screen mode
    i.e. an App with a Screen object
  HISTORY:
    2022-09-08 started
    2023-12-23 made Options() PUBLIC. If it is supposed to be accessed via some other means,
      that needs to be better documented.
    2024-04-29 converted to class-per-file
*/
abstract class caScreen extends BaseClass {

    // ++ CONFIG ++ //

    abstract static protected function ScreenClass() : string;

    // -- CONFIG -- //
    // ++ OBJECT ++ //

    private $oScrn = NULL;
    public function Screen() : ScreenIface {
        if (is_null($this->oScrn)) {
            $cs = static::ScreenClass();
            $this->oScrn = new $cs;
        }
        return $this->oScrn;
    }

    // -- OBJECT -- //
}
