<?php namespace woozalia\futil;

use woozalia\futil\caApp as BaseAppClass;
use woozalia\futil\Opt\Aux\Pair\cMain as OptPairClass;
use woozalia\futil\Opt\Aux\Pair\iMain as OptPairIface;
use woozalia\futil\Opt\Aux\cSub as SubOptClass;
use woozalia\futil\Opt\Aux\iSub as SubOptIface;

interface iOpt {
    static function SetDefaultConfig(string $fn);

    function DescribeSelf() : string;
    function GetSubOpts(string $sKey,string $sc) : SubOptIface;
    function ListCmdOptions(string $sPfx = '  ') : ?string;
    function HasSearchArgs() : bool;
    function DescribeRequest() : string;
}

/*::::
  PURPOSE: base class for parsing and managing command-line options
    I originally tried to use getopt(), but the way it does stuff just adds confusion
      and removes flexibility.
  DOCS: https://wooz.dev/Futilities/human/lib/caOptsBase
  TODO: split this up into a base class with no app options defined
    and a descendent "core" class with universal standard options
  HISTORY:
    2022-07-29 created
    2024-04-29 converted to class-per-file
      renamed from .\caOptsBase -> .\caOpt
*/
abstract class caOpt implements iOpt {

    // ++ SETUP ++ //

    // 2024-05-02 This has not actually been tested yet >.>
    const ksOPT_CONFIG_FILE     = 'conf.file'; // name of config file to use

    public function __construct(array $arCmdArgs) {
        $this->SetupOptions();
        $this->ParseCommand($arCmdArgs);

        // do any app-specific setup
        $this->ProcessUserInput();
    }
    protected function SetupOptions() {
        $this->SetupBaseOptions();
        $this->SetupAppOptions();
    }
    protected function SetupBaseOptions() {
        $this->AddAppOptions([
          self::ksOPT_CONFIG_FILE   => ['-cf','--config-file'],
          ]);
    }
    abstract protected function SetupAppOptions();

    protected function ParseCommand(array $arCmdArgs) {
        // remove first argument (path to self) from command args list
        $arCmdArgs = array_reverse($arCmdArgs);
        $fsSelf = array_pop($arCmdArgs);
        $this->SetSelfName($fsSelf);
        $arCmdArgs = array_reverse($arCmdArgs);

        // parse command line for app options and other terms
        foreach ($arCmdArgs as $sText) {
            $oo = new OptPairClass($sText);
            if ($this->HasAppOption($oo->GetKey())) {
                // if it matches an app option, handle it that way
                $this->AddCmdOption($oo);
            } else {
                // otherwise, handle it like a loose argument (term)
                $this->AddCmdTerm($sText);
            }
        }
        // for debugging
        #echo 'Input terms: '.print_r($arTerms,TRUE);
    }

    protected function ProcessUserInput() {

        // process base options
        if ($this->HasCmdOption(self::ksOPT_CONFIG_FILE)) {
            // a specific conf file is requested
            $oOpt = $this->GetCmdOption(self::ksOPT_CONFIG_FILE);
            $fnConf = $oOpt->GetVal();
            // include the specified conf file; access settings via cConfig static fx().
            require_once($fnConf);
        }

        // process unnamed terms
        $arTerms = $this->GetCmdTerms();
        foreach ($arTerms as $idx => $sTerm) {
            $this->HandleTerm($idx,$sTerm);
        }
        $nTFnd = count($arTerms);
        $nTReq = $this->MinQtyTerms();
        if ($nTFnd < $nTReq) {
            echo
                'INPUT ERROR: '.$this->RenderTooFewTerms($nTFnd)."\n"
                .'USAGE: '.$this->RenderAppUsage()."\n"
                .$this->RenderCommonHelp()
                ;
            die();
        }
    }

    protected function RenderCommonHelp() : string {
        $urlDocs = BaseAppClass::Me()->GetDocsURL();
        return
            "DOCS: $urlDocs\n"
            .$this->ShowAppOptions()
            ."\n"
            ;
    }
    abstract protected function HandleTerm($idx,$sTerm);
    protected function HandleUnknownTerm($idx,$sTerm) {
        echo "Unexpected term: [$sTerm]\n";
    }

    // -- SETUP -- //
    // ++ CONFIG ++ //

    // name of default configuration file (might vary by app, but conf.php is a good default-default)
    static private $fnConfDef = 'conf.php';
    static public function SetDefaultConfig(string $fn) { self::$fnConfDef = $fn; }
    static protected function GetDefaultConfig() : string { return self::$fnConfDef; }

    // RETURNS: the minimum number of terms (non-option arguments) needed for usefulness
    abstract protected function MinQtyTerms() : int;
    // RETURNS: string showing basic command structure for the app
    abstract protected function RenderAppUsage() : string ;
    // RETURNS: string describing what is missing
    protected function RenderTooFewTerms(int $qTerms) : string {
        $sc = get_called_class();
        $sf = __FUNCTION__;
        die("CODING PROMPT: class $sc needs to define $sf().\n");
    }

    // ++ COMMAND: MISC ++ //

    private $fnSelf;
    protected function SetSelfName(string $fn) { $this->fnSelf = $fn; }
    protected function GetSelfName() : string { return $this->fnSelf; }

    // ++ COMMAND: APP OPTIONS ++ //

    // APP OPTIONS are the options that the application will recognize in a command
    // See https://wooz.dev/Futilities/human/lib/caOptsBase

    private $arAppOpts = [];
    protected function GetAppOptions() : array { return $this->arAppOpts; }
    protected function AddAppOption(string $sKey, string $sText) {
        $this->arAppOpts[strtolower($sText)] = $sKey;
    }
    // $ar is [code name => [array of CLI strings which indicate that option]]
    protected function AddAppOptions(array $ar) {
        foreach ($ar as $sKey => $arText) {
            foreach ($arText as $sText) {
                $this->AddAppOption($sKey,$sText);
            }
        }
    }
    /*----
      PURPOSE: checks to see if the option-index includes the given option
      INPUT:
        $sText = full option parameter, possibly including value (demarcated by ':' or '=')
      HISTORY:
        2022-08-01 added check for value
    */
    protected function HasAppOption(string $sText) : bool {
        if ($sText != '') {
            // check for value demarcator (':' or '=')
            $npMark = strcspn($sText,':=');
            if ($npMark > 0) {
                // get just the option name
                $sOptName = substr($sText,0,$npMark);
                $isFnd = array_key_exists(strtolower($sOptName),$this->arAppOpts);
                return $isFnd;
            }
            throw new \exception("2022-09-30 shouldn't we be checking \$sText here?");
        }
        return FALSE;
    }
    protected function GetAppOptionKey(string $sText) : string { return $this->arAppOpts[$sText]; }

    private $arSub = [];
    public function GetSubOpts(string $sKey,string $sc) : SubOptIface {
        if (array_key_exists($sKey,$this->arSub)) {
            $oSub = $this->arSub[$sKey];
        } else {
            $oSub = NULL;  // if this remains NULL, then option is not set
            if ($this->HasCmdOption($sKey)) {
                $oOpt = $this->GetCmdOption($sKey);    // get formatted suboption string
                $oSub = new $sc($oOpt->GetVal());      // $oSub parses that and wraps the results
            }
            if (is_null($oSub)) {
                $oSub = new $sc; // blank object, nothing set
            }
            $this->arSub[$sKey] = $oSub;
        }
        return $oSub;
    }

    /*----
      HISTORY:
        2022-10-20 Improving this a bit so it groups all option aliases together
          TODO: make it possible for each option to have a brief description as well
    */
    protected function ListAppOptions(string $sPfx = '  ', bool $isDebug = FALSE) : ?string {
        $arExt = $this->GetAppOptions();    // array of external codes
        $out = NULL;
        ksort($arExt);
        // build array of internal codes
        foreach ($arExt as $sExt => $sText) {
            #$out .= "$sPfx($sText) $sKey\n";
            $arInt[$sText][] = $sExt;
        }
        foreach ($arInt as $sInt => $arExt) {
            $sIntShow = ($isDebug ? " ($sInt):" : '');
            $out .= $sPfx.$sIntShow;
            foreach ($arExt as $sExt) {
                $out .= ' '.$sExt;
            }
            $out .= "\n";
        }
        return $out;
    }
    protected function ShowAppOptions() : string {
        $sOpt = $this->ListAppOptions();
        if (is_null($sOpt)) {
            $out = "(no options available)\n";
        } else {
            $out = "Options: \n".$this->ListAppOptions();
        }
        return $out;
    }

    // ++ COMMAND: CMD OPTIONS ++ //

    // CMD OPTIONS are the options actually specified in the current command
    // See https://wooz.dev/Futilities/human/lib/caOptsBase

    private $arCmdOpts = [];
    protected function AddCmdOption(OptPairIface $oo) {
        $sKey = $this->GetAppOptionKey($oo->GetKey());
        $this->arCmdOpts[$sKey] = $oo;
    }
    protected function GetCmdOptions() : array { return $this->arCmdOpts; }
    protected function HasCmdOption(string $sText) : bool {
        return array_key_exists($sText,$this->arCmdOpts,);
    }
    protected function GetCmdOption(string $sText) : OptPairIface {
        if (array_key_exists($sText,$this->arCmdOpts)) {
            return $this->arCmdOpts[$sText];
        } else {
            echo "INTERNAL ERROR: option key [$sText] is not found.\nAvailable options:\n"
            .$this->ListAppOptions('  ',TRUE)
            ."Options set in command:\n"
            .$this->ListCmdOptions()
            ;
            throw new \exception('Probably an error in the Futilities code.');
        }
    }
    /*----
      HISTORY:
        2022-10-20 created
    */
    public function ListCmdOptions(string $sPfx = '  ') : ?string {
        $arExt = $this->GetCmdOptions();    // array of external codes
        $out = NULL;
        foreach ($arExt as $sExt => $oOpt) {
            $out .= "$sPfx$sExt: ".$oOpt->GetVal()."\n";
        }
        return $out;
    }

    // ++ COMMAND: TERMS ++ //

    private $arTerms = [];
    protected function AddCmdTerm(string $sText) {
        $this->arTerms[] = $sText;
    }
    protected function GetCmdTerms() : array { return $this->arTerms; }

    // 2022-08-05 Should probably be renamed to "HasTerms()", OSLT, for generalization
    public function HasSearchArgs() : bool { return count($this->GetCmdTerms()) > 0; }

    // -- COMMAND -- //
    // ++ UI: FEEDBACK ++ //

    /*----
      NOTE: This sort of includes DescribeSelf(), but gives the information
        in more technical terms -- largely for debugging purposes
    */
    public function DescribeRequest() : string  {
        $arOpt = $this->GetCmdOptions();
        $arTrm = $this->GetCmdTerms();

        $out = "++ INPUT DESCRIPTION ++\n";
        $nOpt = count($arOpt);
        if ($nOpt > 0) {
            $sPlur = (($nOpt == 1) ? '' : 's');
            $out .= "Option$sPlur set:\n";
            foreach ($arOpt as $sKey => $oOpt) {
                $sOpt = $oOpt->ShowSelf();
                $out .= " - [$sKey]: $sOpt\n";
            }
        } else {
            $out .= "No input options set.\n";
        }

        $nTrm = count($arTrm);
        if ($nTrm > 0) {
            $sPlur = (($nTrm == 1) ? '' : 's');
            $out .= "Term$sPlur:\n";
            foreach ($arTrm as $nIdx => $sVal) {
                $out .= " - [$sVal]\n";
            }
        } else {
            $out .= "No input terms given.\n";
        }
        $out .= "-- INPUT DESCRIPTION --\n";
        return $out;
    }

    // -- UI -- //
}
