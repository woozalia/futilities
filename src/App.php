<?php namespace woozalia\futil;

#use woozalia\futil\IO\Screen\iFull as ScreenIface;

/**
  PURPOSE: class for managing overall app structure
  HISTORY:
    2022-09-08 started
    2023-12-23 made Options() PUBLIC. If it is supposed to be accessed via some other means,
      that needs to be better documented.
    2024-04-29 converted to class-per-file
*/
abstract class caApp {

    // ++ CONFIG ++ //

    static abstract protected function ConfigClass() : string;
    static abstract protected function OptionsClass() : string;

    // -- CONFIG -- //
    // ++ SETUP ++ //

    static private $oApp = NULL;
    static public function Me() : self { return self::$oApp; }

    static private $sVer = NULL;
    static public function SetVersionString(string $s) { self::$sVer = $s; }
    static public function GetVersionString() : string { return self::$sVer; }
    static protected function HasVersionString() : bool { return !is_null(self::$sVer); }

    static private $urlDoc = NULL;
    static public function SetDocsURL(string $url) { self::$urlDoc = $url; }
    static public function GetDocsURL() : string { return self::$urlDoc; }
    static protected function HasDocsURL() : bool { return !is_null(self::$urlDoc); }

    /*----
      NOTE: each app must define its own cOpts class.
    */
    public function __construct(array $arCmdArgs) {
        register_shutdown_function(get_called_class().'::HandleShutdown');
        $ok = pcntl_signal(SIGTERM,get_called_class().'::HandleSigTerm');
        echo "SIGTERM handler registered: [$ok]\n";

        self::$oApp = $this;
        echo static::DescribeSelf();

        $sc = static::ConfigClass();
        if (!$sc::HasMe()) { new $sc; }

        $sc = static::OptionsClass();
        $this->WithOptions(new $sc($arCmdArgs));

        echo $this->oOpts->DescribeSelf()."\n";
        $this->Go();
    }

    // -- SETUP -- //
    // ++ SHUTDOWN ++ //

    public function __destruct() {
        echo "App closing.\n";
        #echo "DIEYEYEYINGGGG!!!\n";
        #csTTY::Shut();
    }
    // ++ neither of these seem to work
    static public function HandleShutdown() {
        echo "Execution finished.\n";
    }
    static public function HandleSigTerm() {
        echo "Terminating...\n\n\n\n";
    }
    // --

    private $oOpts;
    protected function WithOptions(iOpt $o) { $this->oOpts = $o; }
    public function Options() : iOpt { return $this->oOpts; }

    // -- SETUP -- //
    // ++ STARTUP ++ //

    static protected function DescribeSelf() : string {
        self::CheckSetup();
        $sVer = self::GetVersionString();
        $sBar = str_repeat('=',strlen($sVer))."\n";
        cli_set_process_title($sVer);
        return $sBar.$sVer."\n".$sBar;
    }
    static protected function CheckSetup() {
        $sc = get_called_class();
        if (!self::HasVersionString()) {
            die("CODING INCOMPLETE: please call $sc::SetVersionString().\n");
        } elseif (!self::HasDocsURL()) {
            die("CODING INCOMPLETE: please call $sc::SetDocsURL().\n");
        }
    }
    abstract protected function Go();

    // -- STARTUP -- //
    // ++ OBJECTS ++ //

    /* 2024-04-29 This code only belongs in App/cScreen
    abstract static protected function ScreenClass() : string;
    private $oScrn = NULL;
    protected function Screen() : ScreenIface {
        if (is_null($this->oScrn)) {
            $cs = $this->ScreenClass();
            $this->oScrn = new $cs;
        }
        return $this->oScrn;
    }
    */

    // -- OBJECTS -- //
}
