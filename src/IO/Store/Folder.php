<?php namespace woozalia\futil\IO\Store;

use woozalia\futil\IO\Store\Folder\cLocal as LocalClass;
use woozalia\futil\IO\Store\Folder\Remote\cSFTP as SFTPclass;

interface iFolder {
    /*----
      PURPOSE: Check to see if the location has already been mounted. Mount it if not.
      ACTION:
        * if URL includes a protocol, parse it: proto://[user@]host/path
        * invoke the appropriate class to handle the URL
    */
    static function MakeObject(string $url);

    // ++ OPTIONS ++ //
    function SetUseSudo(bool $b);
    // -- OPTIONS -- //
    // ++ FIGURING ++ //
    function GetLocalSpec() : string;
    // -- FIGURING -- //
    // ++ LIFECYCLE ++ //
    function Open();
    function Shut();
    // -- LIFECYCLE -- //
}

/**
  PURPOSE: translates URLs into file paths, mounting remote folders as needed
  HISTORY:
    2024-04-30 converted to class-per-file
      renamed from .\caFolderAnywhere -> .\IO\Store\caFolder
*/
abstract class caFolder implements iFolder {

    // ++ SETUP ++ //

    /*----
      PURPOSE: Check to see if the location has already been mounted. Mount it if not.
      ACTION:
        * if URL includes a protocol, parse it: proto://[user@]host/path
        * invoke the appropriate class to handle the URL
    */
    static public function MakeObject(string $url) {
        // see if there is a ':' before the first '/'
        $idxColon = strpos($url,':');
        $idxSlash = strpos($url,'/');
        //echo "COLON=[$idxColon] SLASH=[$idxSlash]\n";
        if ($idxColon < $idxSlash) {
            // ...then the URL starts with a protocol.
            $sProto = substr($url,0,$idxColon); // get the protocol string
            echo "PROTOCOL=[$sProto]\n";
            $sRem = substr($url,$idxColon+1);   // "://" etc.
            #echo "SREM=[$sRem]\n";
            $idxSlash = strpos($sRem,'//');
            $uri = substr($sRem,$idxSlash+2);  // everything after "://"
            #$oFolder = self::MakeMount($sProto,$sUser,$sHost,$sPath);
            $oFolder = self::MakeMount($sProto,$uri);
        } else {
            $sPath = $url;
            //echo "PATH=[$sPath]\n";
            $oFolder = new LocalClass($sPath);
        }
        return $oFolder;
    }
    static private $arMounts = array();
    static protected function MakeMount(string $sProto,string $uri) {
        switch ($sProto) {
          case 'dav':   $sClass = cFolderDAV::class; break; // 2024-05-03 to be refactored
          case 'file':  $sClass = cFolderFile::class; break;// 2024-05-03 to be refactored
          case 'sftp':  $sClass = SFTPclass::class; break;
          default:
            echo "ERROR: protocol [$sProto] is not currently supported.\n";
            die();
        }
        $oMount = new $sClass($uri);
        return $oMount;
    }

    // -- SETUP -- //
    // ++ OPTIONS ++ //

    // NOTE 2022-09-23 This is currently only used by caFolderRemote; documentation needed.
    private $useSudo = FALSE;
    public function SetUseSudo(bool $b) { $this->useSudo = $b; }
    protected function GetUseSudo() : bool { return $this->useSudo; }

    // -- OPTIONS -- //
}
