<?php namespace woozalia\futil\IO\Store\File;

use woozalia\futil\caApp as AppRootClass;
use woozalia\futil\IO\Store\Aux\Path\iPiece as PieceIface;
use woozalia\futil\IO\Store\caFile as BaseClass;
use woozalia\futil\IO\Store\iFile as BaseIface;

interface iLog extends BaseIface {
    function Write(string $s);
    function WriteLn(string $s);

    function WriteHeader();

    function LogSourceFolder(PieceIface $ofSrce);
    function LogSourceFile(PieceIface $ofSrce);
}

class cLog extends BaseClass implements iLog {

    // ++ CONFIG ++ //

    protected function FileMode() : string { return 'a'; }

    // -- CONFIG -- //
    // ++ OUTPUT ++ //

    public function Write(string $s) { fwrite($this->r,$s); }
    public function WriteLn(string $s) { fwrite($this->r,$s."\n"); }

    public function WriteHeader() {
            $oApp = AppRootClass::Me();
            $sDescr = $oApp->Options()->DescribeSelf();
            $sTime = date('Y/m/d H:i:s');

            $this->WriteLn($oApp::GetVersionString());
            $this->Write($sTime.' '.$sDescr);         // $sDescr ends with a CRLF
    }

    // ++ OUTPUT: conditional: progress ++ //

    public function LogSourceFolder(PieceIface $ofSrce) {} // DEFAULT: do nothing
    public function LogSourceFile(PieceIface $ofSrce) {}   // DEFAULT: do nothing

    // -- OUTPUT -- //
}
