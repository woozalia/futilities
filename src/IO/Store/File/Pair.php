<?php namespace woozalia\futil\IO\Store\File;

use woozalia\futil\caApp as AppClass;
use woozalia\futil\cConf as ConfClass;
use woozalia\futil\iOpt as OptsIface;
use woozalia\futil\IO\Store\Aux\Compare\cFile as FileCompareStatusClass;
use woozalia\futil\IO\Store\Aux\Compare\cFile as FileCompareStatusIface;
use woozalia\futil\IO\Store\Aux\Compare\cPath as PathCompareStatusClass;
use woozalia\futil\IO\Store\Aux\Compare\iPath as PathCompareStatusIface;
use woozalia\futil\IO\Store\Aux\Path\caPiece as PathPieceClass;
use woozalia\futil\IO\Store\Aux\Path\iPiece as PathPieceIface;
use woozalia\futil\IO\Store\Node\cPair as BaseClass;
use woozalia\futil\IO\Store\Node\iPair as BaseIface;

interface iPair extends BaseIface {
    #function UpdateCopyStatus(PathCompareStatusIface $oPath,$nBytesLeft);
    function UpdateFileRead(PathCompareStatusIface $osPath, FileCompareStatusIface $osFile);
    function GetFileA();  // TODO: should have a return type
    function GetFileB();  // TODO: should have a return type
    function Ensure(PathCompareStatusIface $oStat);
}

/**
  HISTORY:
    2024-05-01 converted to class-per-file
      renamed .\cFilePair -> .\IO\Store\File\cPair
*/
class cPair extends BaseClass implements iPair {

    // ++ SETUP ++ //

    private $oOpts, $oLog;

    /*----
      TODO: 2022-10-26 Document why $nCount and $nIndex need to be passed here. Are they ever not zero?
    */
    public function __construct(PathPieceIface $oA,PathPieceIface $oB,$nCount,$nIndex, OptsIface $oOpts) {
        if (($nCount + $nIndex) > 0) { throw new \exception('2022-10-26 Okay, how *does* this happen, anyway?'); }
        parent::__construct($oA,$oB);
        $this->SetIndices($nCount,$nIndex);
        $this->oOpts = $oOpts;
        $this->oLog = $oOpts->LogFile();
    }

    protected function LogFile() : iLog { return $this->oLog; }
    protected function Options() : OptsIface { return $this->oOpts; }

    private $nCount,$nIndex;
    protected function SetIndices($nCount,$nIndex) {
        $this->nCount = $nCount;
        $this->nIndex = $nIndex;
    }
    protected function GetIndexString() { return $this->nIndex.'/'.$this->nCount; }
    protected function IsLastOne() { return ($this->nIndex == $this->nCount); }

    // -- SETUP -- //
    // ++ FEEDBACK ++ //

    protected function UpdateCopyStatus(PathCompareStatusIface $oPath,$nBytesLeft) {
        #$sStat = $oPath->GetProgressText();
        #$s = "$sStat: $nBytesLeft bytes to copy";
        $s = "$nBytesLeft bytes to copy";
        #csTTY::ShowLine($s);
        AppClass::Me()->Screen()->ShowFileStatus($s);
    }
    public function UpdateFileRead(PathCompareStatusIface $osPath, FileCompareStatusIface $osFile) {
        $nSize = $osFile->GetByteSize();

        $dx = $osFile->GetBytesLeft();
        #$sStat = $osPath->GetProgressText();
        if ($dx > 0) {
            #$s = "$sStat: $dx to read";
            $s = "$dx to read";
        } else {
            #$s = "$sStat: $nSize bytes ok";
            $s = "$nSize bytes ok";
        }

        AppClass::Me()->Screen()->ShowFileStatus($s);
        #} else {
            // clear the always-display flag here since it won't get cleared by UpdateStatus()
        #    $this->GetAlwaysShowNext();
        #}
    }

    // -- FEEDBACK -- //
    // ++ OBJECTS ++ //

    private $oFileA=NULL;
    public function GetFileA() {
        if (is_null($this->oFileA)) {
            $this->oFileA = $this->GetPathA()->GetFileNode();
        }
        return $this->oFileA;
    }
    private $oFileB=NULL;
    public function GetFileB() {
        if (is_null($this->oFileB)) {
            $this->oFileB = $this->GetPathB()->GetFileNode();
        }
        return $this->oFileB;
    }

    // -- OBJECTS -- //
    // ++ ACTION ++ //

    /*----
      ACTION:
        1. Check that there is a file at $urlB
        2. Make sure it is the same as the file at $urlA
        3. If either condition not met, inform the user.
      FUTURE: Maybe this should be called EnsureB()
      HISTORY:
        2022-10-27 Updating for HF.
          KF_DO_COPY ->
    */
    public function Ensure(PathCompareStatusIface $oStat) {
        $oScrn = AppClass::Me()->Screen();
        $oScrn->ShowAction('VERIFYING B is A');

        $ofA = $this->GetFileA();
        $ofB = $this->GetFileB();
        $ofiA = $this->GetPathA()->Info();
        $ofiB = $this->GetPathB()->Info();
    //	if ($ofB->Exists() && !is_link($urlB)) {	// for now, not dealing with links
        $urlA = $ofiA->Raw();

        // first, make sure neither is a special file
        $isFileA = $ofiA->IsCopyable();
        $isFileB = $ofiB->IsCopyable();

        if ($isFileA) {
            $canCopy = FALSE;
            if ($isFileB) {
                // same-name file exists and is actual file, so now let's see if they're the same

                // obvious check: are they the same size?
                $nSizeA = $ofiA->GetSize();
                $nSizeB = $ofiB->GetSize();
                if ($nSizeA == $nSizeB) {
                    $oOpts = $this->Options();
                    if ($oOpts->DoCompFound()) {
                        #$this->ShowCompare($oStat,$nSizeA);
                        $this->ShowCompare($oStat);
                    }
                } elseif ($nSizeB == 0) {
                    // if target is empty, go ahead and assume it's ok to overwrite
                    $canCopy = TRUE;
                } else {
                    $sLong = "FILE $urlA DIFFERENT SIZE - A=$nSizeA , B=$nSizeB";
                    $sShort = "different sizes: A=$nSizeA , B=$nSizeB";
                    #csTTY::KeepLine($s);
                    // TODO: This was originally supposed to scroll, but that won't work now; need a new UX paradigm for that. Split-screen? Smol window?
                    AppClass::Me()->Screen()->ShowIssue($sLong);
                    $oLog = $this->LogFile();
                    $oLog->RecordConflict($this,$sShort);
                    if ($this->Options()->DoFinishPartial()) {
                        $oScrn->ShowAction('FINISH COPYING A to B');
                        #$this->ShowCompare($oStat,$nSizeA);
                        $this->ShowCompare($oStat);
                        $canCopy = TRUE;  // TODO 2024-05-02: resume copy at appropriate place
                    }
                }
            } else {
                $canCopy = TRUE;
            }

            if ($canCopy) {
                // NOTE: This can also happen if A is a file and B is a link or pipe
                //$oApp->ShowMessage("NO FILE: $urlB");
                $this->LogFile()->RecordMissing($this); // log that file is missing from B

                $doCopy = !$this->Options()->DoInfoOnly();
                if ($doCopy) {
                    if ($ofiA->IsPipe()) {
                        $s = "skipping pipe $urlA";
                        #$oApp->ShowMessage($s);
                        AppClass::Me()->Screen()->ShowIssue($s);
                    } else {
                        $ok = $this->CopyAToB($oStat);	// copy to B, verify
                    // TODO: original note indicated that this might delete from A, but actual fx() does not afaict.
                    // TODO: should we be doing something with $ok?
                    }
                }
            }
        } else {
            if ($isFileB) {
                $sDescr = 'local file is not';
            } else {
                $sDescr = 'neither local nor remote is';
            }
            $s = "FILE $urlA: ".$sDescr.' a copyable file';
            #$oApp->ShowMessage($s);
            AppClass::Me()->Screen()->ShowIssue($s);
            if (KF_DO_DEL_LINKS && $ofA->GetIsLink()) {
                $ofA->DeleteVisibly();
            }
        }
    }
    /*----
      ACTION: compare the contents of the two files, byte for byte
        TODO: 2024-05-02 If B is longer than A, optionally check that B is A plus more stuff? Wait until there's a need, though.
      RETURNS: TRUE if A and B are binary identical, or A includes all of B
      ASSUMES: Files are same length *OR* B is shorter than A
      HISTORY:
        2024-05-02 We *were* assuming that A and B are the same size,
          but now we want to support finishing incomplete copies --
          so this assumption no longer holds. (At least now we know
          what do do when they're different.)
    */
    #protected function ShowCompare(PathCompareStatusIface $oPathStat, $nSize) : bool {
    protected function ShowCompare(PathCompareStatusIface $oPathStat) : bool {
        $oLog = $this->LogFile();

        $ofA = $this->GetFileA();
        $ofB = $this->GetFileB();
        $onfA = $this->GetPathA()->Info();
        $onfB = $this->GetPathB()->Info();

        $nSizeA = $onfA->GetSize();
        $nSizeB = $onfB->GetSize();
        $urlA = $ofA->GetSpec();
        $urlB = $ofB->GetSpec();

        // only examine up to length of possibly-shorter destination file
        $nBytesLeft = $nSizeB;

        $nBlockBytes = ConfClass::Me()->BytesToReadPerBlock();
        $ok = TRUE;
        $osA = $ofA->OpenForRead();
        $osB = $ofB->OpenForRead();
        // ASSUME: if we got to here, files are readable
        $fpA = $osA->Handle();
        $fpB = $osB->Handle();

        while($ok && ($nBytesLeft > 0)) {
            // TODO: need to check for R/W errors here, log them, and move on
            $sFileA = fread($fpA,$nBlockBytes);
            $sFileB = fread($fpB,$nBlockBytes);
            if ($sFileA != $sFileB) {
                $sStat = "FILE [$urlA] CONTENTS ARE DIFFERENT";
                $oLog->WriteLn($sStat);
                $oLog->RecordConflict($this,$sStat);
                $ok = FALSE;
            } elseif($sFileA === FALSE) {
                $sStat = "FILE [$urlA] COULD NOT BE READ";
                $oLog->WriteLn($sStat);
                $oLog->RecordConflict($this,$sStat);
                $ok = FALSE;
            } else {
                $nBytesLeft -= strlen($sFileA);
                $sIdx = $this->GetIndexString();
                $oFileStat = new FileCompareStatusClass($nSizeB,$nBytesLeft);
                if ($nBytesLeft > 0) {
                    $this->UpdateFileRead($oPathStat,$oFileStat);
                } else {
                    /* 2022-10-27 This doesn't mean anything for now. If we go back to time-interval-based updates, then it may have a use.
                    if (($nBytesLeft > $nBlockBytes) || $this->IsLastOne()) {
                        $oApp->SetAlwaysShowNext();
                    }
                    */
                    $sMsg = "$sIdx : $nSizeB bytes ok";
                    $this->UpdateFileRead($oPathStat,$oFileStat);
                }
            }
        }

        // close files
        $ofA->Shut();
        $ofB->Shut();

        if ($ok) {	// if file contents are equal...
            // 2017-12-24 apparently filectime cannot be modified programmatically, so no point in checking it

            // if file content matches, check timestamps
            $nTimeA = filemtime($urlA);
            $nTimeB = filemtime($urlB);
            if ($nTimeA != $nTimeB) {
                #$this->LogFile()->RecordTimeMismatch($urlA,$nTimeA,$nTimeB,'mod');
                $oLog->RecordTimeMismatch($this,'mod');
                $ok = FALSE;
                $sTimeA = date(KS_FMT_TIMESTAMP,$nTimeA);
                $sTimeB = date(KS_FMT_TIMESTAMP,$nTimeB);
                $sMsg = "timestamp mismatch: A=$sTimeA B=$sTimeB";
                AppClass::Me()->Screen()->ShowIssue($sMsg);
                $ok = $this->Options()->IgnoreDateDiff();
            }
        }
        if ($ok && $this->Options()->DoDelete()) {
            // A and B are identical, and we're deleting identical files from A, so:
            $this->DeleteA();
        }
        return $ok;
    }
    private $nTries;
    protected function CopyAToB(PathCompareStatusIface $oPathStat) : bool {
        $oLog = $this->LogFile();
        $oScrn = AppClass::Me()->Screen();
        $oConf = ConfClass::Me();

        $ofA = $this->GetFileA();
        $ofB = $this->GetFileB();
        $ofiA = $this->GetPathA()->Info();
        $ofiB = $this->GetPathB()->Info();

        $nSize = $ofiA->GetSize();

        $urlA = $ofiA->Raw();
        $urlB = $ofiB->Raw();

        $nBytesLeft = $nSize;
        $ok = TRUE;
        //$fpA = fopen($urlA,'r');
        //$fpB = fopen($urlB,'w');	// B may exist as a pipe or link; this will overwrite

        $oLog->RecordCopyAttempt();
        $osA = $ofA->OpenForRead();
        $osB = $ofB->OpenForWrite();

        $ok = TRUE;

        if (!$osA->IsOpen()) {
            $ok = FALSE;
            $oLog->WriteLn('Could not open file A for reading.');
        }
        if (!$osB->IsOpen()) {
            $ok = FALSE;
            $oLog->WriteLn('Could not open file B for writing.');
        }

        if ($ok) {
            $nBlockBytes = ConfClass::Me()->BytesToReadPerBlock();
            $fpA = $osA->Handle();
            $fpB = $osB->Handle();
            $this->nTries = 0;  // reset the I/O-attempt counter
            while($ok && ($nBytesLeft > 0)) {
                $sTime = date(KS_FMT_TIMESTAMP);
                $sFileA = fread($fpA,$nBlockBytes);
                $nRead = strlen($sFileA);
                $sMsg = NULL;
                if (is_int($nRead)) {
                    // read without error
                    if ($nRead > 0) {
                        // read contained data, yay -- proceed!

                        $nWrit = fwrite($fpB,$sFileA);
                        if (is_int($nWrit)) {
                        // writ without error

                            if ($nWrit > 0) {
                                // actual data written
                                $nBytesLeft -= $nWrit;
                                $this->UpdateCopyStatus($oPathStat,$nBytesLeft);
                            } else {
                                $sMsg = "$sTime: FILE $urlB - STUCK writing at $nBytesLeft to go";
                                $oScrn->ShowIssue($sMsg);
                                #$oLog->UpdateCopyingError($oPathStat,$nBytesLeft);
                                #$oLog->RecordCopyFailure($urlA,$urlB);
                            }
                        } else {
                        // error writing
                            $ok = FALSE;
                            $sMsg = "FILE $urlB - ERROR writing at $nBytesLeft to go";
                        }
                    } else {
                        // read without error, but no bytes
                        $nTries = $this->nTries++;
                        if ($nTries > $oConf->MaxReadRetries()) {
                            $sMsg = "$sTime: FILE $urlA - STUCK reading at $nBytesLeft to go, retried $nTries times";
                            $ok = FALSE;
                            $this->nTries = 0;  // reset counter for next problem
                        }
                    }
                    if (is_string($sMsg)) {
                        $oLog->WriteLn($sMsg);
                    }
                } else {
                // error reading
                    $ok = FALSE;
                    $sMsg = "FILE $urlA - ERROR reading at $nBytesLeft to go";
                }
            }
        }

        // if there were no errors copying:
        if ($ok) {
            // set the timestamp on B:
            $nTime = filemtime($urlA);
            touch($urlB,$nTime);

            // verify same content, to make sure:
            $this->ShowCompare($oPathStat,$nSize);
        }
        return $ok;
    }
    protected function DeleteA() : bool {
        $ok = $this->GetFileA()->DeleteVisibly();
        return $ok;
    }
}
