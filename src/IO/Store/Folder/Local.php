<?php namespace woozalia\futil\IO\Store\Folder;

use woozalia\futil\IO\Store\caFolder as BaseClass;

/**
  INSTAR/RULE:
    URI is just the folder spec
  HISTORY:
    2024-04-30 converted to class-per-file
      renamed from .\cFolderLocal -> .\IO\Store\Folder\cLocal
*/
class cLocal extends BaseClass {
    public function __construct(string $uri) { $this->SetLocalSpec($uri); }

    // ++ LIFECYCLE ++ //

    public function Open()	{}	// no action needed
    public function Shut()	{}	// no action needed

    // -- LIFECYCLE -- //
    // ++ VALUE ACCESS ++ //

    private $fp;
    protected function SetLocalSpec(string $fp) { $this->fp = $fp; }
    // CEMENT
    public function GetLocalSpec() : string { return $this->fp; }

    // ++ VALUE ACCESS ++ //
}
