* PURPOSE: classes for managing folders
* HISTORY:
  *  2022-08-04 Created; moved cFolderator here from ff-lib.php and caMatch here from fileutil.php
    *    TODO: possibly caMatch should be renamed something like caFound
  *  2022-08-26 reworking a bunch of stuff having to do with relative paths
  *  2022-09-28 working again; attempting to sort files found in folders

