<?php namespace woozalia\futil\IO\Store\Folder\Remote;

use woozalia\futil\IO\Store\Folder\caRemote as BaseClass;

/*::::
  NOTE:
    sshfs [user@]host:[dir] mountpoint [options]
  HISTORY:
    2022-09-08 now defaults $fpqRemote to "/" so that a blank string
      takes us to "/", not "/<user>".
      We may want to change this behavior in future, because permissions,
      but wait for a need-case so we can figure out how it needs to work.
    2024-04-30 converted to class-per-file
      renamed from .\cFolderSFTP -> .\IO\Store\Folder\Remote\cSFTP
*/
class cSFTP extends BaseClass {
    protected function GetMountCommand() : string {
        $sConn = $this->GetConnSpec();
        $fpqRemote = $this->GetPath_esc();
        $fpMount = $this->GetMountFileSpec();
        if ($fpqRemote == '') { $fpqRemote = '/'; }
        $fpqMount = addslashes($fpMount);
        #echo "SCONN=[$sConn]\n";
        if ($this->CommandExists('sshfs')) {
            $sOut = "sshfs '$sConn:$fpqRemote' '$fpqMount'";
        } else {
            die("Command 'sshfs' not found; need to install it.\n");
        }
        return $sOut;
    }
}
