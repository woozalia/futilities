<?php namespace woozalia\futil\IO\Store\Folder;

use woozalia\futil\cConf as ConfigClass;
use woozalia\futil\IO\Store\caFolder as BaseClass;

/*::::
  PURPOSE: Folder on a remote server which gets mounted somewhere locally
  INSTAR/RULE:
    URI is the part after the "<protocol>://"
        Format: [<user>@]<host>/<path>
  HISTORY:
    2022-11-04 Added destructor that closes any open connection, in hopes of reducing hangs
      after interrupted sessions.
    2024-04-30 converted to class-per-file
      renamed from .\caFolderRemote -> .\IO\Store\Folder\caRemote
*/
abstract class caRemote extends BaseClass {

    // ++ SETUP ++ //

    public function __construct(string $uri) {
        $idxSlash = strpos($uri,'/');      // this might include a path
        if ($idxSlash == FALSE) {
            // This is just a connection spec.
            $sConn = $uri;
            $sPath = '';
        } else {
            // This spec includes a path
            $sConn = substr($uri,0,$idxSlash); // connection ([user]@host)
            $sPath = substr($uri,$idxSlash);	  // don't include first '/'
        }
        #echo "SCONN=[$sConn]\n";

        $idxAt = strpos($sConn,'@');            // check for optional user@
        if ($idxAt !== FALSE) {
            $sUser = substr($sConn,0,$idxAt);
            $sHost = substr($sConn,$idxAt+1);
        } else {
            $sUser = NULL;  // for diagnostic
            $sHost = $sConn;
        }
        $this->SetHost($sHost);
        $this->SetPath($sPath);
        $this->SetUser($sUser);
    }
    public function __destruct() { $this->Shut(); }

    // -- SETUP -- //
    // ++ VALUE ACCESS ++ //

    private $sUser=NULL;
    protected function SetUser(?string $s) { $this->sUser = $s; }
    protected function GetUser() : ?string { return $this->sUser; }

    private $sHost=NULL;
    protected function SetHost(string $s) { $this->sHost = $s; }
    protected function GetHost() : string { return $this->sHost; }

    private $sPath=NULL;
    protected function SetPath(string $s) { $this->sPath = $s; }
    protected function GetPath() : string { return $this->sPath; }

    // for making unique mountpoints
    private $sStamp = NULL;
    protected function GetStamp() : string {
        if (is_null($this->sStamp)) {
            $this->sStamp = date('Y-m-d.His');
        }
        return $this->sStamp;
    }

    // -- VALUE ACCESS -- //
    // ++ VALUE CALCULATIONS ++ //

    protected function GetMountFileSpec() : string {
        $fpHome = getenv('HOME'); // get current user's home folder
        $fpMounts = $fpHome.ConfigClass::Me()->RemoteMountBaseRel();

        $sHost = $this->GetHost();
        $sStamp = $this->GetStamp();
        $fsMount = "$fpMounts/$sHost-$sStamp";

        return $fsMount;
    }
    public function GetLocalSpec() : string { return $this->GetMountFileSpec(); } // ALIAS
    /*----
      RETURNS: spec for connection
        * [user@]host/path
        * no protocol
    */
    protected function GetConnSpec() : string {
        $sHost = $this->GetHost();
        $sUser = $this->GetUser();

        return (is_null($sUser)?'':"$sUser@") . $sHost;
    }
    protected function GetPath_esc() : string {
        return addslashes($this->GetPath());
    }
    abstract protected function GetMountCommand() : string;
    protected function GetUnmountCommand() {
        $fpLocal = $this->GetMountFileSpec();
        $fpqLocal = addslashes($fpLocal); // escape the spec string for CLI usage
        $sCmd = 'umount '.$fpqLocal;
        return $sCmd;
    }

    // -- VALUE CALCULATIONS -- //
    // ++ OPERATIONS ++ //

    protected function CommandExists(string $sCmd) : bool {
        $sFull = "which $sCmd";
        exec($sFull,$arOut,$nStatus);
        return (count($arOut) > 0); // I think...?
    }
    protected function CheckMountpoint() : bool {
        $fpLocal = $this->GetMountFileSpec();	// where the remote will appear locally
        if (file_exists($fpLocal)) {
            // make sure nothing is mounted (2021-07-15 I *think* this is to prevent collision with incomplete processes?)
            echo " - Error: Duplicate mount point [$fpLocal] found; making sure it is unmounted...";
            $sCmd = $this->GetUnmountCommand();
            exec($sCmd,$arOut,$nStatus);
            echo "done (exit code $nStatus)\n";
            $ok = FALSE;
        } else {
            echo " - Creating mount folder [$fpLocal]...";
            $doSudo = TRUE;
            if (!$this->GetUseSudo()) {
                // If not explicitly using sudo, try nonsudo first:
                $ok = @mkdir($fpLocal,0777,TRUE);	// create the root mount folder (recursively if needed)
                if ($ok) {
                    $doSudo = FALSE;
                } else {
                    echo "\nCouldn't create mount folder; trying sudo...\n";
                }
            }
            if ($doSudo) {
                $fpeLocal = escapeshellarg($fpLocal);
                $sCmd = "sudo mkdir -p -v --mode=777 $fpeLocal";
                $ok = passthru($sCmd);
            }
            if ($ok === FALSE) {
                echo "\nMount Failed: $sCmd\n";
                #die();
            } else {
                $ok = TRUE;
            }
            $sStatus = $ok?'ok':'ERROR';
            echo " $sStatus\n";
        }
        return $ok;
    }

    public function Open() {
        $sHost = $this->GetHost();
        $fpRemote = $this->GetPath();

        echo "Creating mountpoint:\n";

        if ($this->CheckMountpoint()) {
            $sCmd = $this->GetMountCommand();
            if ($this->GetUseSudo()) {
                $sCmd = 'sudo '.$sCmd;
            }
            echo " - Mounting $fpRemote (on $sHost):"
              ."\n -- command: [$sCmd]"
              ."\n -- "
              ;
            #exec($sCmd,$arOut,$nStatus);
            exec($sCmd,$arOut,$nStatus);
            if ($nStatus == 0) {
                echo "ok.\n";
            } else {
                echo "ERROR $nStatus; unmounting...";
                $sCmd = $this->GetUnmountCommand();
                exec($sCmd,$arOut,$nStatus);
                echo " unmount complete (exit code $nStatus)\n";
                die();
            }
        } else {
            die();
        }
    }
    public function Shut() {
        $fpLocal = $this->GetMountFileSpec();
        echo " - Closing (unmounting) [$fpLocal]...";
        $sCmd = $this->GetUnmountCommand();
        exec($sCmd,$arOut,$nStatus);
        echo "done (exit code $nStatus)\n";
    }

    // -- OPERATIONS -- //
}
