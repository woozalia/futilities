<?php namespace woozalia\futil\IO\Store\Node;

use woozalia\futil\IO\Store\Aux\Path\caPiece as PathPieceClass;
use woozalia\futil\IO\Store\Aux\Path\iPiece as PathPieceIface;

interface iPair {
    function GetPathA() : PathPieceIface;
    function GetPathB() : PathPieceIface;
}

/**
  HISTORY:
    2024-05-01 converted to class-per-file
      renamed .\cFSNodePair -> .\IO\Store\Node\cPair
*/
class cPair implements iPair {

    // ++ SETUP ++ //

    public function __construct(PathPieceIface $oPathA,PathPieceIface $oPathB) { $this->SetPaths($oPathA,$oPathB); }

    private $oPathA,$oPathB;
    protected function SetPaths(PathPieceIface $oPathA,PathPieceIface $oPathB) {
        $this->oPathA = $oPathA;
        $this->oPathB = $oPathB;
    }
    // PUBLIC so results can be displayed
    public function GetPathA() : PathPieceIface { return $this->oPathA; }
    public function GetPathB() : PathPieceIface { return $this->oPathB; }

    // -- SETUP -- //
}
