<?php namespace woozalia\futil\IO\Store\Node;

use woozalia\futil\IO\Store\cNode as BaseClass;
use woozalia\futil\IO\Store\iNode as BaseIface;

/**
 * HISTORY:
 *  2024-05-01 I actually cannot find the original code for this at the moment;
 *    I'll have to reconstruct what it was supposed to do from how it is being used.
 */
interface iFolder extends BaseIface {
}

class cFolder extends BaseClass implements iFolder {
}
