<?php namespace woozalia\futil\IO\Store\Node\Aux;

/**
 * NOTE:
 *  2024-05-03 As of v8.2, PHP doesn't recognize "resource" as being its own native "resource" type --
 *    so we can't have syntactical type-checking on that type, as far as I can tell.
 */
interface iOpener {
    function Handle();  // returns: resource
    function IsOpen() : bool;
    function Shut();
}

class cOpener implements iOpener {

    // ++ SETUP ++ //

    static public function FromSpec(string $fs, string $sMode) : self {
        $res = @fopen($fs,$sMode);
        $oThis = new static;
        $oThis->WithResult($res);
        return $oThis;
    }

    // ++ SETUP: dynamic ++ //

    private $res;
    protected function WithResult($res) { $this->res = $res; }
    protected function Result() { return $this->res; }

    // -- SETUP -- //
    // ++ SETDOWN ++ //

    // ASSUMES: is open
    public function Shut() {
        fclose($this->res);
        $this->res = NULL;
    }

    // -- SETDOWN -- //
    // ++ STATE ++ //

    public function Handle() { return $this->Result(); }
    public function IsOpen() : bool { return is_resource($this->Result()); }

    // -- STATE -- //
}
