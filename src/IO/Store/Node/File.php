<?php namespace woozalia\futil\IO\Store\Node;

use woozalia\futil\IO\Store\cNode as BaseClass;
use woozalia\futil\IO\Store\iNode as BaseIface;
use woozalia\futil\IO\Store\Node\Aux\cOpener as OpenerClass;
use woozalia\futil\IO\Store\Node\Aux\iOpener as OpenerIface;

interface iFile extends BaseIface {
    function OpenForRead() : OpenerIface;
    function OpenForWrite() : OpenerIface;
    function Shut();
}

/*::::
  HISTORY:
    2017-12-21 created to make it easier to sum up file sizes without trying to stat un-stat-able files
      need to encapsulate detection of whether something is a copyable file, at least
    2018-01-19 added GetIsCopyable()
    2022-09-10 copied from old FileFerret repo; tidying and updating
    2022-09-11 moving all functions which just use GetSpec() to the PathSeg class
*/
class cFile extends BaseClass implements iFile {

    // ++ STATUS ++ //

    private $oOpener=NULL;	// Opener object
    protected function SetOpener(OpenerIface $o) { $this->oOpener = $o; }
    protected function GetOpener() : OpenerIface { return $this->oOpener; }
    protected function GetNative() { return $this->oOpener->Handle(); }
    protected function IsOpen() : bool { return ($this->oOpener->IsOpen()); }
    protected function ClearNative() { $this->oOpener->Shut(); }

    // -- STATUS -- //
    // ++ OPERATIONS ++ //

    public function OpenForRead() : OpenerIface {
        $this->oOpener = OpenerClass::FromSpec($this->GetSpec(),'r');
        return $this->oOpener;
    }
    // ACTION: opens a file for writing; creates if not found; creates parent folder if not found
    public function OpenForWrite() : OpenerIface {
        $fs = $this->GetSpec();
        $fp = dirname($fs);
        if (!file_exists($fp)) {
            $ok = mkdir($fp,0777,TRUE);
            if (!$ok) {
          die("COULD NOT CREATE [$fp].\n");
            }
        }
        $this->oOpener = OpenerClass::FromSpec($fs,'w');
        return $this->oOpener;
    }
    public function Shut() {
        if ($this->IsOpen()) {
            $this->GetOpener()->Shut();
        }
    }

    // -- OPERATIONS -- //
    // ++ ACTIONS ++ //

    // CEMENT
    public function Delete() {
        $fs = $this->GetSpec();
        $ok = unlink($fs);
        return $ok;
    }

    // -- ACTIONS -- //

}
