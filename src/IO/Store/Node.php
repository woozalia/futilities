<?php namespace woozalia\futil\IO\Store;

use woozalia\futil\caApp as BaseAppClass;

interface iNode {
    function GetSpec() : string;
    function GetTimeInt() : int;
    function GetTimeStr() : string;
    function Delete();
    function DeleteVisibly() : bool;
}

/*::::
  HISTORY:
    2018-01-20 created as a common parent for cFile and cFolder
    2022-09-11 moving all functions which just use GetSpec() to the PathSeg class
    2024-05-01 converted to class-per-file
      renamed from .\cFSNode -> .\IO\Store\cNode
      NOTE: The existence of this class-tree is kinda sloppy; it needs to be reconciled with cFile.
        It also includes some UI stuff that probably doesn't belong here [anymore].
*/
abstract class cNode implements iNode {

    // ++ SETUP ++ //

    public function __construct(string $fs) { $this->SetSpec($fs); }

    private $fs;
    protected function SetSpec(string $fs) { $this->fs = $fs; }
    public function GetSpec() : string { return $this->fs; }

    public function GetTimeInt() : int {
        $fs = $this->GetSpec();
        $nTime = filemtime($fs);
        return $nTime;
    }
    public function GetTimeStr() : string {
        $nTime = $this->GetTimeInt();
        $sTime = date(KS_FMT_TIMESTAMP,$nTime);
        return $sTime;
    }

    // -- SETUP -- //
    // ++ CALCULATIONS ++ //


    // -- CALCULATIONS -- //
    // ++ ACTIONS ++ //

    public function DeleteVisibly() : bool {
        $oApp = BaseAppClass::Me();
        $oScrn = $oApp->Screen();

        $ok = $this->Delete();
        $fs = $this->GetSpec();
        if ($ok) {
            $oScrn->ShowAction( "DELETED [$fs]" );
        } else {
            $oScrn->ShowAction( "Could not delete [$fs]." );	// just leave it there, no need to die()
        }
        $oApp->Options()->LogFile()->RecordDeletion($fs);
        return $ok;
    }

    // -- ACTIONS -- //

}
