<?php namespace woozalia\futil\IO\Store\Aux\Path\Piece;

use woozalia\futil\IO\Store\Aux\Path\Aux\cStatus as StatusClass;
use woozalia\futil\IO\Store\Aux\Path\Aux\iStatus as StatusIface;
use woozalia\futil\IO\Store\Aux\Path\caPiece as BaseClass;
use woozalia\futil\IO\Store\Aux\Path\iPiece as BaseIface;

/*::::
  PURPOSE: the root segment of a path-chain; has no parent-folder
  HISTORY:
    2024-05-01 converted to class-per-file
      renamed .\cPathRoot -> .\IO\Store\Aux\Path\Piece\cRoot
*/
class cRoot extends BaseClass {
    public function __construct(string $fp) { $this->SetSpec($fp); }

    private $fpBase;
    protected function SetSpec(string $fp) { $this->fpBase = $fp; }
    /*----
      CEMENT caPathHandler
      HISTORY:
        2022-09-29 revised how cPathStatus (now Status*)works
    */
    public function GetPathStatus() : StatusIface {
        $oStat = new StatusClass;
        $oStat->SetRootPath($this->fpBase);
        return $oStat;
    }
}
