<?php namespace woozalia\futil\IO\Store\Aux\Path\Piece\Named;

use woozalia\futil\IO\Store\Aux\Path\Aux\iStatus as StatusIface;
use woozalia\futil\IO\Store\Aux\Path\iPiece as PieceIface;
use woozalia\futil\IO\Store\Aux\Path\Piece\caNamed as BaseClass;
use woozalia\futil\IO\Store\Aux\Path\Piece\iNamed as BaseIface;

interface iList extends BaseIface {
    public function DoReset();
    public function GoNext() : bool;
}

/*::::
  PURPOSE: a Path-segment for iterating through a directory
    I *think* this will essentially replace cPathBlank.
  HISTORY:
    2022-09-29 started
    2024-05-01 converted to class-per-file
      renamed .\cPathFileList -> .\IO\Store\Aux\Path\Piece\Named\cList
      created iList
*/
class cList extends BaseClass implements iList {
    public function __construct(PieceIface $oBase, array $arFiles) {
        $this->SetName('');
        $this->SetBase($oBase);
        $this->SetFiles($arFiles);
        if (count($arFiles) == 0) { throw new \exception('Something is wrong here.'); }
    }

    private $arFiles;
    protected function SetFiles(array $ar) {
        $this->arFiles = $ar;
        $this->SetCount(count($ar));
    }
    protected function GetFiles() : array { return  $this->arFiles; }

    private $nFiles;
    protected function SetCount(int $n) { $this->nFiles = $n; }
    protected function GetCount() : int { return $this->nFiles; }

    private $idxFile;
    public function DoReset() { $this->idxFile = 0; }
    public function GoNext() : bool {
        $ar = $this->GetFiles();
        $idx = $this->idxFile;
        $isMore = $idx < $this->GetCount();
        if ($isMore) {
            $this->SetName($ar[$idx]);
            $this->idxFile++;
        }
        return $isMore;
    }
    protected function SelfNumStatus() : string {
        $nThis = $this->idxFile;
        $nAll = $this->GetCount();
        return "$nThis:$nAll";
    }
    public function GetPathStatus() : StatusIface {
        $oStat = parent::GetPathStatus();
        $oStat->AddInfo('/'.$this->SelfNumStatus());
        return $oStat;
    }
}
