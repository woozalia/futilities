<?php namespace woozalia\futil\IO\Store\Aux\Path\Piece\Named;

use woozalia\futil\IO\Store\Aux\Path\iPiece as PieceIface;
use woozalia\futil\IO\Store\Aux\Path\Piece\caNamed as BaseClass;

/*::::
  PURPOSE: segment of a Path, but also provides an API to the entire parent-path
    from which it dangles; it's the "cap" to a path
  NOTE: "base" in this context means "parent folder", not the root-base of the whole path.
  HISTORY:
    2024-05-01 converted to class-per-file
      renamed .\cPathCap -> .\IO\Store\Aux\Path\Piece\Named\cCap
*/
class cCap extends BaseClass {
    public function __construct(string $sName, PieceIface $oBase) {
        $this->SetName($sName);
        $this->SetBase($oBase);
    }
}
