<?php namespace woozalia\futil\IO\Store\Aux\Path\Piece;

use woozalia\futil\IO\Store\Aux\Path\caPiece as BaseClass;
use woozalia\futil\IO\Store\Aux\Path\iPiece as BaseIface;
use woozalia\futil\IO\Store\Aux\Path\Aux\iStatus as StatusIface;

interface iNamed extends BaseIface {
    function SetName(string $fn);
    function GetName() : string;
    function GetBasePath() : string;
}

/**
  PURPOSE: implements the Named stuff
  HISTORY:
    2024-04-30 converted to class-per-file
      renamed:
        from .\iPathNamed -> .\IO\Store\Aux\Path\Piece\iNamed
          but then merged up with .
        from .\caPathNamed -> .\IO\Store\Aux\Path\Piece\caNamed
  */
abstract class caNamed extends BaseClass implements iNamed {

    // ++ VALUE ++ //

    private $fn;
    public function SetName(string $fn) { $this->fn = $fn; }
    // PUBLIC so we can display just the latest addition to the path
    public function GetName() : string { return $this->fn; }

    // -- VALUE -- //
    // ++ PATH ++ //

    private $oBase;
    protected function SetBase(BaseIface $oBase=NULL) { $this->oBase = $oBase; }
    public function GetBasePath() : string {
        if (is_null($this->oBase)) {
            return '';
        } else {
            return $this->oBase->GetPath();
        }
    }

    /*----
      CEMENT caPathHandler
      INSTAR: non-base piece, no numbering
      RETURNS: Status
      HISTORY:
        2022-09-29 revised how cPathStatus (now StatusIface) works
    */
    public function GetPathStatus() : StatusIface {
        $oStat = $this->oBase->GetPathStatus();
        #$oStat->nSegs++;
        $sName = $this->GetName();
        $oStat->AddRelPath($sName);
        return $oStat;
    }

    // -- PATH -- //
}
