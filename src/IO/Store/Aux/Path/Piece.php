<?php namespace woozalia\futil\IO\Store\Aux\Path;

use woozalia\futil\IO\Store\Aux\Path\Aux\iStatus as StatusIface;
use woozalia\futil\IO\Store\Aux\Path\Piece\iNamed as NamedIface;
use woozalia\futil\IO\Store\Aux\Path\Piece\Named\cCap as CapClass;
use woozalia\futil\IO\Store\Node\cFile as FileClass;
use woozalia\futil\IO\Store\Node\iFile as FileIface;
use woozalia\futil\IO\Store\Node\cFolder as FolderClass;
use woozalia\futil\IO\Store\Node\iFolder as FolderIface;
use woozalia\futil\IO\Store\Spec\cInfo as SpecInfoClass;
use woozalia\futil\IO\Store\Spec\iInfo as SpecInfoIface;

/**
 * HISTORY:
 *  2024-05-01 merged .\IO\Store\Aux\Path\Piece\iNamed into woozalia\futil\IO\Store\Aux\Path\iPiece,
 *    because iNamed is the root trait and it just didn't make sense the way it was...
 *    ...and then re-extracted the stuff that needs to *not* be in Piece\Root.
 */
interface iPiece {
    // ++ ACTION ++ //

    function AddSegment(string $fn) : NamedIface;
    // ACTION: Parse the given relative path and return a new PathPiece for the last segment
    function ParsePath(string $fpr) : NamedIface;

    // -- ACTION -- //
    // ++ FIGURING ++ //

    // RETURNS entire filespec, including base
    function GetSpec() : string;
    /*----
      RETURNS relative filespec from base
      PUBLIC because sometimes we need that information
    */
    function GetPath() : string;

    // -- FIGURING -- //
    // ++ FILESYS: NAMING ++ //
    // This was originally in iNamed.

    function GetPathStatus() : StatusIface;

    // ++ FILESYS: OBJECTS ++ //

    function GetFileNode() : FileIface;
    function GetFolderNode() : FolderIface;

    // ++ FILESYS: I/O ++ //

    function Info() : SpecInfoIface;

    // -- FILESYS -- //

}

/**
  PURPOSE: generic base class for path-handling classes
  HISTORY:
    2024-04-30 converted to class-per-file
      renamed from .\caPathPiece -> .\IO\Store\Aux\Path\caPiece
  */
abstract class caPiece implements iPiece {

    // ++ ACTION ++ //

    public function AddSegment(string $fn) : NamedIface { return new CapClass($fn,$this); }
    // ACTION: Parse the given relative path and return a new PathPiece for the last segment
    public function ParsePath(string $fpr) : NamedIface {
        $fpClean = trim($fpr,'/');  // make sure there are no extra slashes
        $arPath = explode('/',$fpClean);
        $oSeg = $this;
        foreach ($arPath as $fnSeg) {
            $oSeg = $oSeg->AddSegment($fnSeg);
        }
        return $oSeg;
    }

    // -- ACTION -- //
    // ++ FIGURING ++ //

    // RETURNS entire filespec, including base
    public function GetSpec() : string { return $this->GetPathStatus()->GetAbsPath(); }
    /*----
      RETURNS relative filespec from base
      PUBLIC because sometimes we need that information
    */
    public function GetPath() : string { return $this->GetPathStatus()->GetRelPath(); }

    // -- FIGURING -- //
    // ++ FILESYS: OBJECTS ++ //

    public function GetFileNode() : FileIface {
        $fs = $this->GetSpec();
        return new FileClass($fs);
    }
    public function GetFolderNode() : FolderIface {
        $fs = $this->GetSpec();
        return new FolderClass($fs);
    }

    // ++ FILESYS: I/O ++ //

    public function Info() : SpecInfoIface { return new SpecInfoClass($this->GetSpec()); }

    // -- FILESYS -- //

}
