*  PURPOSE: classes for handling file-paths in a structured way
*  HISTORY:
  *   2019-03-31 started, so we can show paths as trees
    *    Originally written for ffmove, I think; was pathSeg.php.
  *   2022-09-05 copied to The Futilities and adapted/updated
    *    ...though it turns out maybe I don't need it [yet].
  *   2022-09-11 need it now! Some class changes:
    *      caPathPiece: new base class
    *      cPathSeg renamed to cPathCap
    *      cPathBase renamed to cPathRoot, descends from cPathHandler instead of cPath[Seg]
    *      merged in work from 9/10 accidentally done on separate file
  *   2022-09-12 added cnPathInfo

