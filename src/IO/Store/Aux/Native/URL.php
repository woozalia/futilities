<?php namespace woozalia\futil\IO\Store\Aux\Native;

/**
  HISTORY:
    2024-04-30 converted to class-per-file
      renamed from .\cnURLInfo -> .\IO\Store\Aux\Native\cnURL
  */
class cnURL {
    static public function FromSpec(string $fs) : self { return new self(parse_url($fs)); }

    private $ar;

    public function __construct(array $ar) { $this->ar = $ar; }

    protected function GetItNz(string $sName, $def=NULL, string $sPfx=NULL, string $sSfx=NULL) {
        if (array_key_exists($sName,$this->ar)) {
            return $sPfx . $this->ar[$sName] . $sSfx;
        } else {
            return $def;
        }
    }
    // 2022-10-25 Just using this where immediately needed; will probably need to use in more of the below methods.

    public function Scheme()    : ?string { return $this->ar['scheme']; } // proto:
    public function Host()      : string { return $this->ar['host']; }    // domain/address
    public function Port()      : int    { return $this->ar['port']; }    // port number (if specified)
    public function User()      : string { return $this->ar['user']; }
    public function Pass()      : string { return $this->ar['pass']; }
    public function Path()      : string { return $this->ar['path']; }
    public function PathInfo()  : cnPathInfo { return cnPathInfo::FromSpec($this->Path()); }
    public function Query()     : ?string { return $this->GetItNz('query'); }  // after the question mark ?
    public function Fragment()  : string { return $this->ar['fragment']; }    // after the "#"

    // ++ FIGURED ++ //

    // *Mk() functions include the necessary URL markup *if* the part is set

    public function SchemeMk()    : string    { return $this->GetItNz('scheme','','','://'); }
    public function PortMk()      : string    { return $this->GetItNz('port','',':'); }
    public function PassMk()      : string    { return $this->GetItNz('pass','',':'); }
    public function UserMk()      : string    { return $this->GetItNz('user','','','@'); }

    // 2022-12-25 maybe we need a version of some of these which includes demarcation (e.g. "://") *only if* the piece isn't blank.
    public function BaseURL()   : string {
        return
          $this->SchemeMk()
          .$this->UserMk()
          .$this->Host()
          .$this->PortMk()
          .$this->PassMk()
          .$this->Path()
          ;
    }

    // -- FIGURED -- //
}
