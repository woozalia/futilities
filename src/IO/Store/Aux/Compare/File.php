<?php namespace woozalia\futil\IO\Store\Aux\Compare;

interface iFile {
    function GetBytesLeft() : int;
    function GetByteSize() : int;
}

/**
  HISTORY:
    2024-05-02 converted to class-per-file
      renamed .\cFileCompareStatus -> .\IO\Store\Aux\Compare\cFile
*/
class cFile implements iFile {
    #private $n,$dx;	// total size, bytes of progress

    public function __construct(private int $n,private int $dx) {
        #$this->n = $n;
        #$this->dx = $dx;
    }
    public function GetByteSize()   : int { return $this->n; }
    public function GetBytesLeft()  : int { return $this->dx; }
}
