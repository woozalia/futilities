<?php namespace woozalia\futil\IO\Store\Aux\Compare;

use woozalia\futil\IO\Store\Aux\Path\iPiece as PathPieceIface;

interface iPath {
    function GetProgressText() : string;
    function GetPathObject() : PathPieceIface;
}

/**
  HISTORY:
    2019-04-05 created
    2022-10-19 updating for FUtils
    2024-05-02 converted to class-per-file
      renamed .\cPathCompareStatus -> .\IO\Store\Aux\Compare\cPath
      NOTE: This *could* inherit from cFile, but nothing actually needs it to.
*/
class cPath implements iPath {
    private $n,$dx,$oPath;

    public function __construct(PathPieceIface $oPath, $n,$dx) {
        $this->oPath = $oPath;
        $this->n = $n;
        $this->dx = $dx;
    }
    public function GetProgressText() : string { return $this->dx.'/'.$this->n;  }
    public function GetPathObject() : PathPieceIface { return $this->oPath; }
}
