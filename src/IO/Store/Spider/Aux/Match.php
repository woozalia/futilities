<?php namespace woozalia\futil\IO\Store\Spider\Aux;

use woozalia\futil\IO\Store\Aux\Path\iPiece as PieceIface;
use woozalia\futil\iOpt as OptsIface;

interface iMatch {
    // ++ ACTIONS ++ //

    function DoStart();
    function DoFinish();

     // handle a folder known to be wanted
    function HandleFolder(PieceIface $of);
    // handle a file known to be wanted
    function HandleFile(PieceIface $of);

    // -- ACTIONS -- //
    // ++ STATUS ++ //

    function IsFolderWanted(PieceIface $of) : bool;
    function IsFileWanted(PieceIface $of) : bool;

    // -- STATUS -- //
    // ++ OBJECT ++ //

    function GetOptions() : OptsIface;

    // -- OBJECT -- //
}

/*::::
  PURPOSE: abstract wrapper for determining what constitutes a "match" and what do do when one happens
  HISTORY:
    2022-07-29 created
    2022-08-05 Moved abstract HandleFile() here from caMatchFF,
      because every Match class should be able to describe what it is doing.
*/
abstract class caMatch implements iMatch {

    // ++ ACTIONS ++ //

    public function DoStart() {}   // STUB
    public function DoFinish() {}  // STUB

     // handle a folder known to be wanted
    public function HandleFolder(PieceIface $of) {
        $sClass = get_called_class();
        $sMethod = __METHOD__;
        die("CODING PROMPT: Class $sClass needs to implement $sMethod.\n");
    }

    // -- ACTIONS -- //
    // ++ SETTINGS ++ //

    private $oOpts;
    protected function SetOptions(OptsIface $o) { $this->oOpts = $o; }
    public function GetOptions() : OptsIface { return $this->oOpts; }

    // -- SETTINGS -- //
}
