<?php namespace woozalia\futil\IO\Store\Spider\Aux\Match;

use woozalia\futil\cConf as RootConfClass;
use woozalia\futil\IO\Store\Aux\Path\iPiece as PieceIface;
use woozalia\futil\IO\Store\Spider\Aux\caMatch as BaseClass;
use woozalia\futil\IO\Store\Spider\Aux\Match\Aux\cResult as ResultClass;
use woozalia\futil\IO\Store\Spider\Aux\Match\Aux\iResult as ResultIface;

/*::::
  PURPOSE: Matcher class with some common defaults
  HISTORY:
    2022-09-30 moved to here from FTM: the version of DescribeSelf()
      which just passes the buck to Options. This seems like a good default.
      Maybe it should even go in caMatch.
    2022-11-04 Removed DescribeSelf() -- doesn't seem to be used.
      If we do need it, let's call it DescribeMatch(), to avoid confusion.
    2024-05-01 converted to class-per-file
      renamed from .\caMatchStandard -> .\IO\Store\Spider\Aux\Match\caStandard
*/
abstract class caStandard extends BaseClass {

    // ++ CONDITIONS: API ++ //

    // RULE: all folders searched unless in ignore list
    public function IsFolderWanted(PieceIface $of) : bool {
        $fnThis = $of->GetName();
        $doUse = !$this->IsIgnorable($fnThis);
        #echo " || NAME=[$fnThis] - ".($doUse ? 'ok' : 'IGNORE')."\n";
        #if (!$doUse) { echo " IGNORING FOLDER $fnThis\n"; }
        return $doUse;
    }
    // RULE: files searched if mask matches
    public function IsFileWanted(PieceIface $of) : bool {
        $fnThis = $of->GetName();
        if ($this->IsIgnorable($fnThis)) {
            $bWant = FALSE;
            #echo " IGNORING FILE $fnThis\n";
        } else {
            $omRes = $this->NameMatch($of);
            $this->SetTextMatch($omRes);
            $bWant = $omRes->GetIsMatch();
        }
        return $bWant;
    }

    // ++ CONDITIONS: SUB ++ //

    /*----
      HISTORY:
        2022-09-28 renamed from FileMatch() to NameMatch(), because that's more accurately descriptive
    */
    protected function NameMatch(PieceIface $of) : ResultIface {
        $fnThis = $of->GetName();

        $oOpts = $this->GetOptions();
        if ($oOpts->HasFileMask()) {
            $fnMask = $oOpts->GetFileMask();
            $nFlags = ($oOpts->CaseSensitive() ? 0 : FNM_CASEFOLD);
            $bOk = fnmatch($fnMask,$fnThis,$nFlags);
        } else {
            $bOk = TRUE;
        }

        $oRes = new ResultClass($bOk,$fnThis);
        return $oRes;
    }
    protected function IsIgnorable(string $fn) : bool {
        // check against ignore list
        $arIgnore = RootConfClass::Me()->MasksToIgnore();
        $doIgnore = FALSE;
        #echo "\nCHECKING [$fn]...\n";
        foreach ($arIgnore as $fnMask) {

            /*
            // DEBUGGING
            if (strpos($fnMask,'cache') !== FALSE) {
                $okGlob = fnmatch($fnMask,$fn);
                $dxCache = strpos($fn,'cache');
                if ($dxCache !== FALSE) {
                    echo "\n - [$fn] matches [$fnMask]: [$okGlob] @[$dxCache]\n";
                }
            }
            */

            if (fnmatch($fnMask,$fn)) {
                $doIgnore = TRUE;
                break;  // no need to check further
            }
        }
        #echo ($doIgnore ? 'IGNORE' : 'ok')."\n";
        return $doIgnore;
    }

    // -- CONDITIONS -- //

    private $omRes;
    protected function SetTextMatch(ResultIface $o) { $this->omRes = $o; }
    protected function GetTextMatch() : ResultIface { return $this->omRes; }
}
