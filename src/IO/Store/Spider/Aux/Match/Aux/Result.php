<?php namespace woozalia\futil\IO\Store\Spider\Aux\Match\Aux;

interface iResult {
    public function GetIsMatch() : bool;
    public function GetString() : string;
}

/*::::
  PURPOSE: wraps the result of a string-matching operation
  HISTORY:
    2024-05-01 converted to class-per-file
      renamed .\cMatchResult -> .\IO\Store\Spider\Aux\Match\Aux\cResult
      added iResult
*/
class cResult implements iResult {
    // sWhat = the string examined - might be just part of the original inquiry
    public function __construct(bool $isMatch, string $sWhat) {
        $this->SetIsMatch($isMatch);
        $this->SetString($sWhat);
    }

    private $isMatch;
    protected function SetIsMatch(bool $b) { $this->isMatch = $b; }
    public function GetIsMatch() : bool { return $this->isMatch; }

    private $sWhat;
    protected function SetString(string $s) { $this->sWhat = $s; }
    public function GetString() : string { return $this->sWhat; }
}
