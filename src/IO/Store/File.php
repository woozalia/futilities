<?php namespace woozalia\futil\IO\Store;

interface iFile {
    function Restart();
}

/*
  PURPOSE: classes for file I/O
  NOTE:
    2022-10-07 need to document the relationship between this and cFile in file.php
  HISTORY:
    2022-09-03 split off from fileutil.php
*/
abstract class caFile implements iFile {

    // ++ SETUP ++ //

    public function __construct(string $fs) { $this->Open($fs); }
    public function __destruct() { $this->Shut(); }

    // -- SETUP -- //
    // ++ CONFIG ++ //

    abstract protected function FileMode() : string;

    // -- CONFIG -- //
    // ++ ACTION ++ //

    protected $r;
    protected function Open(string $fs) {
        $sMode = $this->FileMode();
        $r = fopen($fs,$sMode);
        if ($r === FALSE) {
            echo "ERROR: Could not open file '$fs'.\n";
            die();
        }
        $this->r = $r;
    }
    protected function Shut() { fclose($this->r); }

    public function Restart() { fseek($this->r,0); }

    // -- ACTION -- //

}
