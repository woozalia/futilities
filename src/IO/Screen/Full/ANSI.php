<?php namespace woozalia\futil\IO\Screen\Full;

use woozalia\futil\IO\Screen\caFull as BaseClass;

/*::::
  PURPOSE: experimental af
  CODES (for easy reference; see https://en.wikipedia.org/wiki/ANSI_escape_code#Description)
    <ESC>[<n>A - cursor up <n> lines
    <ESC>[<n>B - cursor down <n> lines
    <ESC>[<n>C - cursor forward (right) <n> spaces
    <ESC>[<n>D - cursor back (left) <n> spaces
    <ESC>[<n>;<m>H - position cursor at row <n>, column <m>
    <ESC>[<n>J - erase part/all of display (3 possible modes)
    <ESC>[<n>K - erase part/all of line (3 possible modes)
  HISTORY:
    2022-10-29 started
    2024-04-29 converted to class-per-file
      renamed from .\cScreenANSI -> .\IO\Screen\Full\cANSI
*/
class cANSI extends BaseClass {

    const ESC = "\e";     // for production
    #const ESC = "[ESC]";  // for debugging

    protected function CtrlSeq(string $sCode) : string { return self::ESC . '[' . $sCode; }
    protected function EraseScreenPart(int $sCode) : string { return $this->CtrlSeq($sCode.'J'); }
    protected function EraseLinePart(int $sCode) : string { return $this->CtrlSeq($sCode.'K'); }

    public function EraseScreenAll() { echo $this->EraseScreenPart(2); }
    public function EraseScreenToTop() { echo $this->EraseScreenPart(1); }
    public function EraseScreenToEnd() { echo $this->EraseScreenPart(0); }

    public function EraseLineAll()   { echo $this->EraseLinePart(2); }
    public function EraseLineLeft() { echo $this->EraseLinePart(1); }
    public function EraseLineRight() { echo $this->EraseLinePart(0); }

    public function SetCursor(int $colX, int $rowY) { echo $this->CtrlSeq($rowY.';'.$colX.'H'); }
    public function MoveCursorLeft(int $n=1)  { echo $this->CtrlSeq($n.'D'); }
    public function MoveCursorRight(int $n=1) { echo $this->CtrlSeq($n.'C'); }
    public function MoveCursorUp(int $n=1)    { echo $this->CtrlSeq($n.'A'); }
    public function MoveCursorDown(int $n=1)  { echo $this->CtrlSeq($n.'B'); }

}

