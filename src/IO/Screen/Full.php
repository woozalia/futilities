<?php namespace woozalia\futil\IO\Screen;

interface iFull {
    function EraseScreenAll();
    function EraseScreenToTop();
    function EraseScreenToEnd();

    function EraseLineAll();
    function EraseLineLeft();
    function EraseLineRight();

    function SetCursor(int $colX, int $rowY);
    function MoveCursorLeft(int $n=1);
    function MoveCursorRight(int $n=1);
    function MoveCursorUp(int $n=1);
    function MoveCursorDown(int $n=1);

    function SetCursorLeft();
    // ACTION: make $n lines of vertical space for relative cursor ops at the bottom of the screen
    function AllocateLines(int $n);
    function ShowOnRelLine(int $n, string $s);

}

/**::::
  PURPOSE: abstract wrapper for TTY with whole-screen control (typically via ESC codes)
  HISTORY:
    2022-10-29 started
    2024-04-29 converted to class-per-file
*/
abstract class caFull implements iFull {

    // ++ FIGURING: RELATIVE ++ //

    private $rx=1;  // horizontal position relative to start of line (start=1)
    private $ry=0;  // vertical position relative to starting location (start=0)

    const CR = "\r";    // production
    #const CR = '[CR]';  // debugging

    public function SetCursorLeft() {
        echo self::CR;
        $this->rx = 1;
    }

    // ACTION: make $n lines of vertical space for relative cursor ops at the bottom of the screen
    public function AllocateLines(int $n) {
        echo str_repeat("\n",$n);
        $this->rx = 1;
        $this->ry = $n;
    }
    protected function MoveToRelLine(int $n) {
        $nDiff = $n - $this->ry;  // # of lines the cursor needs to move
        if ($nDiff < 0) {
            // move up
          #echo "\n===\nMOVING UP - diff=$nDiff\n===\n";
            $this->MoveCursorUp(-$nDiff);
        } elseif ($nDiff > 0) {
          #echo "\n===\nMOVING DOWN - diff=$nDiff\n===\n";
            $this->MoveCursorDown($nDiff);
        }
        $this->ry = $n;
    }
    public function ShowOnRelLine(int $n, string $s) {
        $this->MoveToRelLine($n);
        $this->SetCursorLeft();
        echo $s;
        $this->EraseLineRight();
    }
}
