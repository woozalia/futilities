<?php
$fErrLevel = E_ALL | E_STRICT;
error_reporting($fErrLevel);
if (!ini_get('display_errors')) {
    ini_set('display_errors', 'TRUE');
}

$sVer = phpversion();
echo "PHP version: <b>$sVer</b><br>";
$idUser = getmyuid();
$idGroup = getmygid();
$arUser = posix_getpwuid($idUser);
$arGroup = posix_getgrgid($idGroup);
$sUser = $arUser['name'];
$sGroup = $arGroup['name'];
echo "Process owner (u:g): <b>$sUser</b>:<b>$sGroup</b><br>";

$fs = 'userid.tmp';
$ok = touch($fs);

if ($ok) {

    $idUser = fileowner($fs);
    $idGroup = filegroup($fs);
    
    $arUser = posix_getpwuid($idUser);
    $arGroup = posix_getgrgid($idGroup);
    
    $sUser = $arUser['name'];
    $sGroup = $arGroup['name'];
    
    echo "Running as user '<b>$sUser</b>' ($idUser), group '<b>$sGroup</b>' ($idGroup).\n";
} else {
    echo "Could not create test file '$fs'!<br>";
}
